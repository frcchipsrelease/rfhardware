# rfHardware

Robot Framework Hardware - classes that are used to interact with specific hardware. The intent is to have a layer of a generic interface with an implementation that can be changed out for different hardware that performs the same function