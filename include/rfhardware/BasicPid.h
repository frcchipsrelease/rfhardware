/*******************************************************************************
 *
 * File: BasicPid.h
 * 
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

/*******************************************************************************
 *
 * This class is used for closed loop control of high speed motors that 
 * will see rapid changes in loading
 * 
 ******************************************************************************/
class BasicPid
{
	public:
        BasicPid();
        ~BasicPid();

        void setGains(double kf, double kp, double ki, double kd, double kz=100000000.0f);

        void setKp(double kp);
        void setKi(double ki);
        void setKd(double kd);
        void setKf(double kf);
        void setKz(double kz);

        double getKp(void);
        double getKi(void);
        double getKd(void);
        double getKf(void);
        double getKz(void);

        double getErrorKd();
        double getErrorKp();
        double getErrorKi();

        void setCommandLimits(double lower, double upper);
        void setIsAngle(bool is_angle);

        double getLowerCommandLimit(void);
        double getUpperCommandLimit(void);

        double calculateCommand(double target, double actual);

	private:
		double m_kp; // proportional gain
		double m_ki; // integral gain
        double m_kd; // differentil gain
        double m_kf; // feed forward
        double m_kz; // integral zone

        bool m_is_angle;  // if the PID is an angle, how error is treated differently (error between -179 and 179 is 2, not -358)

        double m_error;               // kept for logging
        double m_error_prev;          // sum of all prior error, subject to decay
        double m_error_delta;         // difference between current and prior m_error (can be low passed)

		double m_filter_coeff;
        double m_max_command;
        double m_min_command;
};

