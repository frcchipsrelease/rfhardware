/*******************************************************************************
 *
 * File: Gyro_Analog.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Gyro.h"

#include "frc/AnalogGyro.h"

namespace rfh
{

/*******************************************************************************
 *
 * This class is a wrapper around the WPI Gyro class that adds some features.
 * 
 * The isReady method can be used to help insure the Gyro is not
 * used when it is unplugged or damaged.
 * 
 ******************************************************************************/
class Gyro_Analog : public Gyro
{
	public:
        Gyro_Analog(uint32_t channel);
        virtual ~Gyro_Analog(void);

        double getAngle(void) const;
        double getRate(void) const;

        void setSensitivity(double value);
        
        void reset(void);
        void calibrate();

	private:
		float drift_rate;
		float offset;

		float cur_raw_angle;
		float prev_raw_angle;

		bool is_ready;

        frc::AnalogGyro *m_analog_gyro;
};

} // end namespace
