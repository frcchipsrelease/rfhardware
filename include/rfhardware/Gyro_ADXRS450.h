/*******************************************************************************
 *
 * File: Gyro_ADXRS450.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *  This file is based on the WPILib ADXRS450 Gyro class with minor
 *  modifications to allow for more configuration and use of the ADXRS453 gyro
 *  on the Spartan board.
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <frc/ADXRS450_Gyro.h>

#include "rfhardware/Gyro.h"
#include "frc/Notifier.h"
#include "frc/SPI.h"
#include "wpi/priority_mutex.h"

#include <memory>

namespace rfh
{

/*******************************************************************************
 *
 * Use a rate gyro to return the robots heading relative to a starting position.
 * The Gyro class tracks the robots heading based on the starting position. As
 * the robot rotates the new heading is computed by integrating the rate of
 * rotation returned by the sensor. When the class is instantiated, it does a
 * short calibration routine where it samples the gyro while at rest to
 * determine the default offset. This is subtracted from each sample to
 * determine the heading.
 *
 * This class is for the digital ADXRS450 gyro sensor that connects via SPI.
 *
 ******************************************************************************/
class Gyro_ADXRS450 : public Gyro
{
    public:
        explicit Gyro_ADXRS450(
            frc::SPI::Port port=frc::SPI::kOnboardCS0,
			bool invert = false);

        virtual ~Gyro_ADXRS450() = default;

        double getAngle() const;
        double getRate() const;
        void reset();
        void calibrate();
        bool isReady() const;
        int getPort() const;

    private:
        frc::ADXRS450_Gyro m_gyro;
        float m_invert;
        bool m_is_ready;

        uint16_t readRegister(uint8_t reg);
};

} // end namespace