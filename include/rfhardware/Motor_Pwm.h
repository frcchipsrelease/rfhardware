/*******************************************************************************
 *
 * File: Motor_Pwm.h
 *
 * Written by:
 *  FRC Team 324, Chips
 * 	Clear Creek Independent School District FIRST Robotics
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Motor.h"

#include "rfhardware/ScaledAnalogInput.h"
#include "rfhardware/LimitSwitch.h"
#include "rfhardware/BasicPid.h"

#include "frc/motorcontrol/PWMMotorController.h"
#include "frc/Encoder.h"

#include <stdint.h>
#include <string>

/*******************************************************************************
 *
 * This class is for controlling motors through a PWM interface, there are
 * brands of speed controllers that use this interface.
 * 
 ******************************************************************************/
class Motor_Pwm : public Motor
{
    public:
        Motor_Pwm(frc::PWMMotorController* controller);
        virtual ~Motor_Pwm(void);

        void setPercent(double val);
        double getPercent(void);

        void setPosition(double position);
        double getPosition();
        double getRawPosition(void);

        void setVelocity(double velocity);
        double getVelocity(void);
        double getRawVelocity(void);

        // When using a generic motor with closed loop control,
        // the doUpdate method must be called at regular intervals.
        // This is to allow logic to be implemented and executed
        // for simple motor controllers. For motor controllers that
        // have an embedded processor, this should be left as a
        // no-opt implementation.
        void doUpdate(void);

        void setMotorInvert(bool invert);
        bool getMotorInvert(void);

        ControlModeType getControlMode();

        void setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak);

        bool isUpperLimitPressed(void);
        bool isLowerLimitPressed(void);

        int getDeviceID();

        void setAsFollower(Motor* leader);

        void setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position=false, double position_value=0.0);
        void setLowerLimitSwitch(int8_t port, bool normally_open, bool reset_position=false, double position_value=0.0);

        void setUpperLimitSwitch(LimitSwitch* forward_limit_switch);
        void setLowerLimitSwitch(LimitSwitch* reverse_limit_switch);

        void setAnalogInput(ScaledAnalogInput* analog_input);
        void setEncoder(frc::Encoder* encoder);
        void setPid(BasicPid* pid);

        // @TODO: could maybe have a setCurrentSensor, then make a CurrentSensor class with
        // an implementation that uses the PDP and fuse to get the current

        void setKp(double p);
        void setKi(double i);
        void setKd(double d);
        void setKf(double f);
        void setKz(double z);

        double getKp(void);
        double getKi(void);
        double getKd(void);
        double getKf(void);
        double getKz(void);

    private:
        frc::MotorController* m_controller;

        ControlModeType m_control_mode;
        FeedbackDeviceType m_sensor_type;

        ScaledAnalogInput* m_analog_input;
        frc::Encoder* m_encoder_input;
        LimitSwitch* m_forward_limit_switch;
        LimitSwitch* m_reverse_limit_switch;
        BasicPid* m_pid;
        Motor* m_leader;

        double m_motor_invert_factor;

        double m_target_percent;
        double m_command_percent;

        double m_target_position;
        double m_actual_position;
        double m_actual_position_raw;

        double m_target_velocity;
        double m_actual_velocity;
        double m_actual_velocity_raw;

        double m_max_command_delta;

        double m_max_command_percent;
        double m_min_command_percent;

        bool m_forward_limit_pressed;
        bool m_reverse_limit_pressed;
};
