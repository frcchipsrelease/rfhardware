/*******************************************************************************
 *
 * File: Gyro_Pigeon.h
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 * 
 * References:
 * CTRE Pigeon 2.0:
 * - Communication via CAN.            
 * - Docs https://v5.docs.ctr-electronics.com/en/stable/ch11a_BringUpPigeon2.html.
 * - API https://api.ctr-electronics.com/phoenix/release/cpp/classctre_1_1phoenix_1_1sensors_1_1_w_p_i___pigeon2.html
 * 
 * - https://github.com/CrossTheRoadElec/Phoenix6-Examples
 * 
 ******************************************************************************/
#pragma once

#include "rfhardware/Gyro.h"

#ifndef PHOENIX_VERSION
#define PHOENIX_VERSION 6
#endif

#if (PHOENIX_VERSION == 6)
#include <ctre/phoenix6/Pigeon2.hpp>
#else
#include "ctre/Phoenix.h"
#endif

#include "frc/geometry/Rotation2d.h"

#include <memory>

namespace rfh
{

/*******************************************************************************
 *
 * Use a rate gyro to return the robots heading relative to a starting position.
 * The Gyro class tracks the robots heading based on the starting position. As
 * the robot rotates the new heading is computed by integrating the rate of
 * rotation returned by the sensor. When the class is instantiated, it does a
 * short calibration routine where it samples the gyro while at rest to
 * determine the default offset. This is subtracted from each sample to
 * determine the heading.
 *
 * This class is for the digital ADXRS450 gyro sensor that connects via SPI.
 *
 ******************************************************************************/
class Gyro_Pigeon : public Gyro
{
    public:
        explicit Gyro_Pigeon(int device_id, std::string canbus);
        virtual ~Gyro_Pigeon() = default;

        double getAngle() const;
        double getRate() const;
        frc::Rotation2d getRotation2d() const;
        void reset();
        void calibrate();
        bool isReady() const;
        double getPitch() const override;
        double getRoll() const override;
        double getYaw() const override;
        double getCompassHeading() const override;
        bool isMoving() const override;
        bool isRotating() const override;
        bool isConnected() const override;

    private:

#if (PHOENIX_VERSION == 6)
        ctre::phoenix6::hardware::Pigeon2 *pigeon;
#else
        ctre::phoenix::sensors::WPI_Pigeon2 *pigeon;
#endif
        bool m_is_ready;
};

} // end namespace