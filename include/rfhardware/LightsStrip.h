/*******************************************************************************
 *
 * File: LightsStrip.h
 *
 * Written by:
 *	Clear Creek Independent School District FIRST Robotics
 *	FRC Team 324, Chips
 *	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *	Copywrite and License information can be found in the LICENSE.md file 
 *	distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "frc/AddressableLED.h"
#include <vector>

class LightsStrip
{
	public:
		enum Patterns
		{
			NONE = 0,		SOLID,		GRADIENT,		DASHED,		GRADIENT_DASHED,
			THEATER_CHASE,	LOADING,	REVERSE_LOADING,	SHOOTING_STAR
		};

		LightsStrip(int length, int port);
		~LightsStrip(void);

		void start(void);
		void setData(void);
		int getLength(void);

		void setStaticPattern(int pattern, int start_idx, int end_idx, int color_1[3], int color_2[3]);
		void setMovingPattern(int pattern, int start_idx, int end_idx, int color_1[3], int color_2[3], int count);


	private:	
		frc::AddressableLED *led;

		std::vector <frc::AddressableLED::LEDData> buffer;
		int strip_length;
};