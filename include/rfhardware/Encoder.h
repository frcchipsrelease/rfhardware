/*******************************************************************************
 *
 * File: Encoder.h
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <string>

namespace rfh
{

/*******************************************************************************
 *
 * @class Encoder
 * 
 * This class is a generic implementation of any sensor that measures values
 * based on rotations of a shaft. This is intended to allow other software to 
 * focus more on function than hardware implementation.
 *
 ******************************************************************************/
class Encoder
{
    public:

        enum Capability : uint32_t
        {
            ABSOLUTE     = 0x0001, // 2^0
            WRAPS        = 0x0002, // 2^1
            POSITION     = 0x0004, // 2^2
            RATES        = 0x0008  // 2^3
        };

        virtual ~Encoder(void) = default;

        bool isCapableOf(Encoder::Capability capability);

        void setScale(float scale);
        float getScale(void);

        void setOffset(float offset);
        float getOffset(void);

        void setOffsetToPosition(float value);

        void setCountsPerRotation(float counts_per_rotation);
        float getCountsPerRotation(void);
 
        virtual void setInvertDirection(bool invert);
        virtual bool getInvertDirection(void);

        // positions
        virtual float getRawPosition(void);
        float getRotationPosition(void);
        float getPosition(void);

        virtual float getAbsoluteRawPosition(void);
        float getAbsoluteRotationPosition(void);
        float getAbsolutePosition(void);
        float getAbsoluteRotationCenteredPosition(void);
        float getAbsoluteCenteredPosition(void);

        // rates
        virtual float getRawRate(void);
        float getRotationRate(void);
        float getRate(void);

    protected:
        explicit Encoder(uint32_t capabilities);  // protected so only subclasses can be create

        float scale;
        float offset;
        float counts_per_rotation;
        bool invert_direction;

        const uint32_t capabilities; // bitwise-or list of capabilities
};

} // end namespace
