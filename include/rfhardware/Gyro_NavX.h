/*******************************************************************************
 *
 * File: Gyro_Spi.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	FRC Team 118, The Robonauts
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 ***********************************************************************
 * navX-MXP:
 * - Communication via RoboRIO MXP (SPI, I2C) and USB.            
 * - See http://navx-mxp.kauailabs.com/guidance/selecting-an-interface.
 * 
 * navX-Micro:
 * - Communication via I2C (RoboRIO MXP or Onboard) and USB.
 * - See http://navx-micro.kauailabs.com/guidance/selecting-an-interface.
 * 
 * VMX-pi:
 * - Communication via USB.
 * - See https://vmx-pi.kauailabs.com/installation/roborio-installation/
 * 
 * Multiple navX-model devices on a single robot are supported.
 ************************************************************************
 ******************************************************************************/
#pragma once

#include "rfhardware/Gyro.h"
#include "AHRS.h"
#include "frc/geometry/Rotation2d.h"


#include <memory>

namespace rfh
{

/*******************************************************************************
 *
 * Use a rate gyro to return the robots heading relative to a starting position.
 * The Gyro class tracks the robots heading based on the starting position. As
 * the robot rotates the new heading is computed by integrating the rate of
 * rotation returned by the sensor. When the class is instantiated, it does a
 * short calibration routine where it samples the gyro while at rest to
 * determine the default offset. This is subtracted from each sample to
 * determine the heading.
 *
 * This class is for the digital ADXRS450 gyro sensor that connects via SPI.
 *
 ******************************************************************************/
class Gyro_NavX : public Gyro
{
    public:
        explicit Gyro_NavX(
            frc::I2C::Port port = frc::I2C::kOnboard,
            bool is_micro = true); // navx micro
        
        explicit Gyro_NavX(
            frc::SPI::Port port = frc::SPI::kMXP); // navx regular (mxp)

        virtual ~Gyro_NavX() = default;

        double getAngle() const;
        double getRate() const;
        frc::Rotation2d getRotation2d() const;
        void reset();
        void calibrate();
        bool isReady() const;
        double getPitch() const override;
        double getRoll() const override;
        double getYaw() const override;
        double getCompassHeading() const override;
        double getWorldLinearAccelX() const override;
        double getWorldLinearAccelY() const override;
        double getWorldLinearAccelZ() const override;
        bool isMoving() const override;
        bool isRotating() const override;
        bool isConnected() const override;

    private:
        AHRS *navXGyro;
        bool m_is_ready;
};

} // end namespace