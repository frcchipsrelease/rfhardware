/*******************************************************************************
 *
 * File: RpmCounter.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "frc/Counter.h"

#define SECONDS_TO_MINUTES 60.0

/*******************************************************************************
 *
 * This class provides a wrapper for the Counter class to convert counts into
 * RPM
 *  
 ******************************************************************************/
class RpmCounter : public frc::Counter
{
	public:
        RpmCounter(uint32_t channel, double filterCoeff = 0.9);

        void update();
        double getSpeedRpm();
        double getSpeedFiltered();
        double getSpeedCountsPerSec();
        double getSpeedRevsPerSec();
        double getSpeedRpmFiltered();
        int32_t getCounts();
        void setCountPerRev(int cpr);
        void setFilterCoeff(double coeff);
		void reset();

	private:
		int32_t counter_last_count;
		double counter_last_clock;
		double counter_counts_per_rev;

		double counter_speed_rpm_raw;
		double counter_speed_rps_raw;
		double counter_speed_cps_raw;
		double counter_speed_rpm_filter;
		double counter_low_pass_coeff;
		double counter_last_filter_data;
		bool counter_first;
};
