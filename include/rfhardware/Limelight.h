/*******************************************************************************
 *
 * File: Limelight.h 
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "networktables/NetworkTable.h"
#include "networktables/NetworkTableInstance.h"
//#include "networktables/NetworkTableEntry.h"
//#include "networktables/NetworkTableValue.h"
#include <span>

// never put a using in include file, prefix needed items with frc::
// using namespace frc;


/**
 * 
 */



class Limelight
{
 public:

  Limelight();
  ~Limelight();

  void getData(double &target_visible,
            double &targetOffsetAngle_Horizontal, 
            double &targetOffsetAngle_Vertical,
            double &targetArea,
            double &targetSkew);

    void setPipeline(int pipeline);

 private:
 std::shared_ptr<nt::NetworkTable> table = nt::NetworkTableInstance::GetDefault().GetTable("limelight");
 
double m_target_visible;
double m_targetOffsetAngle_Horizontal;
double m_targetOffsetAngle_Vertical;
double m_targetArea;
double m_targetSkew; 

};
