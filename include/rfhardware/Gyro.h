/*******************************************************************************
 *
 * File: Gyro.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "frc/geometry/Pose2d.h"

namespace rfh
{

/*******************************************************************************
 *
 * This class is a generic Gyro interface
 * 
 ******************************************************************************/
class Gyro
{
	public:
        Gyro(void);
        virtual ~Gyro(void);

        bool isReady(void) const;

        virtual double getAngle(void) const;
        virtual double getRate(void) const;
        virtual double getPitch(void) const;
        virtual double getRoll(void) const;
        virtual frc::Rotation2d getRotation2d(void) const;
        virtual double getYaw() const;
        virtual double getCompassHeading() const;
        virtual double getWorldLinearAccelX() const;
        virtual double getWorldLinearAccelY() const;
        virtual double getWorldLinearAccelZ() const;
        virtual bool isMoving() const;
        virtual bool isRotating() const;
        virtual bool isConnected() const;

        virtual void reset(void);
        virtual void calibrate();

    protected:
        bool is_ready;
};

} // end namespace