/*******************************************************************************
 *
 * File: AbsPosSensor.h -- Absolute Position Sensor
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "frc/AnalogInput.h"

// never put a using in include file, prefix needed items with frc::
// using namespace frc;

/*******************************************************************************
 *
 * @TODO: This class should extend ScaledAnalogInput
 *
 * This class provides an interface for getting calibrated values from an
 * Absolute Position Sensor.
 *
 *  Periodically call getRaw(), getPosition(), or getPositionWithWraps(). The
 *  period should be such that the actuation being measured cannot turn the
 *  APS more than a quarter turn during the period. This allows this class
 *  to monitor for sensor wraps.
 *
 *  AbsPosSensor *my_aps = new AbsPosSensor(my_chan);
 *  my_aps->setScale(my_scale);
 *  my_aps->setOffset(my_offset);
 *  
 *  ...
 *  
 *	my_pos = my_aps->getPositionWithWraps());
 * 
 ******************************************************************************/
class AbsPosSensor : public frc::AnalogInput
{
	public:
        AbsPosSensor(uint32_t channel);
        ~AbsPosSensor(void);

		float getRaw(void);
		float getPosition(void);
		float getPositionWithWraps(void);
		int getWrapCount(void);
		void moveWrapCountToRange(float min, float max); // this is so the user can put into a range

		void setScale(float scale);
		float getScale(void);

		void setOffset(float offset);
		float getOffset(void);

		void setRawRange(float arg);
		float getRawRange(void);

	private:
		float m_offset;
		float m_scale;
		int   m_wrap_count;
		float m_previous_raw;
		float m_raw_range;
};
