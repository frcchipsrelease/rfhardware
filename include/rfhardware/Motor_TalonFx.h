/*******************************************************************************
 *
 * File: Motor_TalonFx.h
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Motor.h"

#include "ctre/Phoenix.h"

#include <stdint.h>
#include <string>

/*******************************************************************************
 *
 * This class is for controlling motors through a PWM interface, there are
 * brands of speed controllers that use this interface.
 * 
 ******************************************************************************/
class Motor_TalonFx : public Motor
{
    public:
        Motor_TalonFx(int device_id, std::string canbus, int status_period_general_ms = 10, int status_period_feedback_ms = 20);
        virtual ~Motor_TalonFx(void);

        void setPercent(double val);
        double getPercent(void);

        void setPosition(double position);
        double getPosition();
        double getRawPosition(void);
        virtual void resetPosition(double position = 0.0);

        void setVelocity(double velocity);
        double getVelocity(void);
        double getRawVelocity(void);

        void doUpdate(void);

        void setMotorInvert(bool invert);
        bool getMotorInvert(void);

        void setSensorInvert(bool invert);
//        bool getSensorInvert(void);
        void setSensorScale(double scale, double offset=0);

        void setPid(BasicPid* pid);

        void setBrakeMode(bool brake);

        ControlModeType getControlMode();

        void setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak);
        void  setCurrentLimit(bool enabled, double amps=10.0, double peak=12.5, double duration=0.5 );
        void  setStatorCurrentLimit(bool enabled, double amps=10, double peak=12.5, double duration=0.5 );

        bool isUpperLimitPressed(void);
        bool isLowerLimitPressed(void);

        int getDeviceID();

        void setAsFollower(Motor_TalonFx& leader);
        void setFeedbackDevice(FeedbackDeviceType sensor_type, bool wrap);

        void setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position = false, double position_value = 0.0);
        void setLowerLimitSwitch(int8_t port, bool normally_open, bool reset_position = false, double position_value = 0.0);

        void setKp(double p);
        void setKi(double i);
        void setKd(double d);
        void setKf(double f);
        void setKz(double z);

        double getKp(void);
        double getKi(void);
        double getKd(void);
        double getKf(void);
        double getKz(void);

        ctre::phoenix::motorcontrol::can::BaseMotorController& getController(void);

        static const double m_volt_max;

    private:
        // wpi version has setvoltage() and other needed commands
        ctre::phoenix::motorcontrol::can::WPI_TalonFX m_controller;
        // ctre::phoenix::motorcontrol::SensorCollection& m_sensor_data;

        ControlModeType m_control_mode;

        bool m_forward_limit_pressed;
        bool m_reverse_limit_pressed;

        double m_output_scale;
        double m_output_offset;
        /**
         * Time scale to convert velocity to x / 100ms
         * By default, TalonFX considers velocity a change in position over 100ms 
         * per CTRE docs.
         */
        double m_velocity_timescale; 

};
