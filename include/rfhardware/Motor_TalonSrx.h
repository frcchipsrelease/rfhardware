/*******************************************************************************
 *
 * File: Motor_TalonSrx.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Motor.h"

#include "ctre/phoenix/motorcontrol/can/TalonSRX.h"

#include <stdint.h>
#include <string>

/*******************************************************************************
 *
 * This class is for controlling motors through a PWM interface, there are
 * brands of speed controllers that use this interface.
 * 
 ******************************************************************************/
class Motor_TalonSrx : public Motor
{
    public:
        Motor_TalonSrx(int device_id, int control_period_ms = 10);
        virtual ~Motor_TalonSrx(void);

        void setPercent(double val);
        double getPercent(void);

        void setPosition(double position);
        double getPosition();
        double getRawPosition(void);

        void setVelocity(double velocity);
        double getVelocity(void);
        double getRawVelocity(void);

        void doUpdate(void);

        void setMotorInvert(bool invert);
        bool getMotorInvert(void);

        void setSensorInvert(bool invert);
//        bool getSensorInvert(void);
        void setSensorScale(double scale, double offset=0);

        void setPid(BasicPid* pid);

        void setBrakeMode(bool brake);

        ControlModeType getControlMode();

        void setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak);

        bool isUpperLimitPressed(void);
        bool isLowerLimitPressed(void);

        int getDeviceID();

        void setAsFollower(Motor_TalonSrx& leader);
        void setFeedbackDevice(FeedbackDeviceType sensor_type, bool wrap);

        void setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position = false, double position_value = 0.0);
        void setLowerLimitSwitch(int8_t port, bool normally_open, bool reset_position = false, double position_value = 0.0);

        void setKp(double p);
        void setKi(double i);
        void setKd(double d);
        void setKf(double f);
        void setKz(double z);

        double getKp(void);
        double getKi(void);
        double getKd(void);
        double getKf(void);
        double getKz(void);

        ctre::phoenix::motorcontrol::can::TalonSRX& getController(void);

    private:
        ctre::phoenix::motorcontrol::can::TalonSRX m_controller;
 //       ctre::phoenix::motorcontrol::SensorCollection& m_sensor_data;

        ControlModeType m_control_mode;

        bool m_forward_limit_pressed;
        bool m_reverse_limit_pressed;

        double m_output_scale;
        double m_output_offset;

};
