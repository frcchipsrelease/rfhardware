/*******************************************************************************
 *
 * File: CDoubleSolenoid.h - Double solenoid
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "frc/Solenoid.h"

/*******************************************************************************
 *
 * This class provides an interface for getting switch inputs, it works for
 * switches that are wired normally open and normally closed. When the class
 * is configured for the appropriate type of switch, the Get method will
 * return true when the switch is pressed.
 * 
 ******************************************************************************/
class CDoubleSolenoid
{
	public:
        CDoubleSolenoid(uint32_t module, frc::PneumaticsModuleType pm_type, uint32_t channel_a, uint32_t channel_b);
        CDoubleSolenoid(uint32_t channel_a, uint32_t channel_b);
        virtual ~CDoubleSolenoid(void);

		virtual void set(bool val);

	private:
        frc::Solenoid* solenoid_a;
        frc::Solenoid* solenoid_b;
};
