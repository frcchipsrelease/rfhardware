/*******************************************************************************
 *
 * File: AnalogIrSensor.h -- Analog IR Sensor
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "frc/AnalogInput.h"

// never put a using in include file, prefix needed items with frc::
// using namespace frc;

#define DEFAULT_LATCH_CYLES 4
#define DEFAULT_THRESHOLD_VALUE 3900
#define DEFAULT_IR_VALUE 4100
/**
 * 
 */
class AnalogIrSensor : public frc::AnalogInput
{
 public:

  enum ThresholdType { kGreaterThan, kLessThan };

  AnalogIrSensor(int channel);
  ~AnalogIrSensor();

  void setThreshold(int threshold);
  void setLatchCycles(int cycles);
  void setThresholdType(ThresholdType type);
  bool onLine();
  int update();

 private:
  int m_threshold;
  ThresholdType m_threshold_type;
  int m_default_value;
  int m_latch_cycles;
  int m_latch_count;
  int m_value;

};
