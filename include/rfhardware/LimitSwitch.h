/*******************************************************************************
 *
 * File: LimitSwitch.h -- Digital Input for Simple Switches
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "frc/DigitalInput.h"
#include <stdint.h>

/*******************************************************************************
 *
 * This class provides an interface for getting switch inputs, it works for
 * switches that are wired normally open and normally closed. When the class
 * is configured for the appropriate type of switch, the Get method will
 * return true when the switch is pressed.
 * 
 ******************************************************************************/
class LimitSwitch : public frc::DigitalInput
{
	public:
		enum InputMode
		{
			INPUT_NORMALLY_OPEN = 0,
			INPUT_NORMALLY_CLOSED
		};

        LimitSwitch(uint16_t port, InputMode mode = INPUT_NORMALLY_OPEN,
            bool reset = false, double reset_value = 0.0);
        ~LimitSwitch(void);

        virtual bool isPressed(void) const;

		void setMode(InputMode mode);
		InputMode getMode(void) const;

        void setReset(bool reset);
        bool isReset(void);

        void setResetValue(double value);
        double getResetValue(void);

	private:
        InputMode m_mode;

        bool m_reset;
        double m_reset_value;
};
