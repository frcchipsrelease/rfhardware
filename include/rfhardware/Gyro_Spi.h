/*******************************************************************************
 *
 * File: Gyro_Spi.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *  This file is based on the WPILib ADXRS450 Gyro class with minor
 *  modifications to allow for more configuration and use of the ADXRS453 gyro
 *  on the Spartan board.
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Gyro.h"
#include "frc/Notifier.h"
#include "frc/SPI.h"
#include "wpi/priority_mutex.h"

#include <memory>

namespace rfh
{

/*******************************************************************************
 *
 * Use a rate gyro to return the robots heading relative to a starting position.
 * The Gyro class tracks the robots heading based on the starting position. As
 * the robot rotates the new heading is computed by integrating the rate of
 * rotation returned by the sensor. When the class is instantiated, it does a
 * short calibration routine where it samples the gyro while at rest to
 * determine the default offset. This is subtracted from each sample to
 * determine the heading.
 *
 * This class is for the digital ADXRS450 gyro sensor that connects via SPI.
 *
 ******************************************************************************/
class Gyro_Spi : public Gyro
{
    public:
        explicit Gyro_Spi(
            frc::SPI::Port port=frc::SPI::kMXP,
            uint32_t spi_cmd = 0x20000000u,
            uint8_t xfer_size = 4,
            uint32_t valid_mask = 0x0c00000eu, // ADXRS450 is 0x0c000000u
            uint32_t valid_value = 0x04000000u,
            uint8_t data_shift = 10u,
            uint8_t data_size = 16u,
            bool is_signed = true,
            bool big_endian = true,
			bool invert = false);

        virtual ~Gyro_Spi() = default;

        double getAngle() const;
        double getRate() const;
        void reset();
        void calibrate();
        bool isReady() const;

    private:
        frc::SPI m_spi;
        float m_invert;
        bool m_is_ready;

        uint16_t readRegister(uint8_t reg);
};

} // end namespace