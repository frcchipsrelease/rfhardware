/*******************************************************************************
 *
 * File: Motor.h
 *
 * Written by:
 *  FRC Team 324, Chips
 * 	Clear Creek Independent School District FIRST Robotics
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <stdint.h>
#include "rfhardware/BasicPid.h"

/*******************************************************************************
 *
 * This class provides the definition of an interface for motor/speed controllers,
 * providing a common interface for both PWM motor controller and CAN motor
 * controllers.
 * 
 ******************************************************************************/
class Motor
{
	public:
		enum FeedbackDeviceType
		{
            NONE=0,
            EXTERNAL_ENCODER,
			INTERNAL_ENCODER,
			EXTERNAL_POTENTIOMETER,
			INTERNAL_POTENTIOMETER,
            INTEGRATED_SENSOR
		};

		enum ControlModeType
		{
            PERCENT=0,
			POSITION,
			VELOCITY,
            FOLLOWER,
			UNKNOWN_TYPE
		};

        enum BrakeMode
        {
            COAST = 0,
            BRAKE
        };

        Motor(void);
        virtual ~Motor(void);

        // The calling application should use one of setPercent, setPosition, or setSpeed
        // at a time, switching between these could result is some delays as the underlying
        // controller switches modes

        virtual void setPercent(double val) = 0;
        virtual double getPercent(void) = 0;

        virtual void setPosition(double pos);
        virtual double getPosition();
        virtual double getRawPosition(void);
        virtual void resetPosition(double position = 0.0);


        virtual void setVelocity(double velocity);
        virtual double getVelocity(void);
        virtual double getRawVelocity(void);

        // When using a generic motor with closed loop control,
        // the doUpdate method must be called at regular intervals.
        // This is to allow logic to be implemented and executed
        // for simple motor controllers. For motor controllers that
        // have an embedded processor, this should be left as a
        // no-opt implementation.
        virtual void doUpdate(void);

        virtual void setBrakeMode(BrakeMode brake_mode);
        virtual BrakeMode getBrakeMode(void);

        virtual void setMotorInvert(bool invert);
        virtual bool getMotorInvert(void);

//        virtual void setSensorDeviceType(FeedbackDeviceType sensor_type, int port_a=0, int port_b=0);

        virtual void setSensorInvert(bool invert);
        virtual bool getSensorInvert(void);
        virtual void setSensorScale(double scale, double offset=0);

        virtual void setPid(BasicPid* pid);
        virtual void setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak);

        virtual void setControlMode(ControlModeType type, int deviceID=-1);
        virtual ControlModeType getControlMode();

        virtual void setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position = false, double position_value = 0.0);
        virtual void setLowerLimitSwitch(int8_t port, bool normally_open, bool reset_position = false, double position_value = 0.0);

        virtual bool isUpperLimitPressed(void);
        virtual bool isLowerLimitPressed(void);

        virtual int getDeviceID();

        virtual void setCurrentLimit(uint32_t amps, uint32_t peak=0, uint32_t duration=0 );
        virtual void setCurrentLimitEnabled(bool enabled);

        virtual void setKp(double p);
        virtual void setKi(double i);
        virtual void setKd(double d);
        virtual void setKf(double f);
        virtual void setKz(double z);

        virtual double getKp(void);
        virtual double getKi(void);
        virtual double getKd(void);
        virtual double getKf(void);
        virtual double getKz(void);
};
