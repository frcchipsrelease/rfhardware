/*******************************************************************************
 *
 * File: Motor_SparkMax.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Motor.h"

#include "rev/CANSparkMax.h"

#include <stdint.h>
#include <string>

/*******************************************************************************
 *
 * This class is for controlling motors through a PWM interface, there are
 * brands of speed controllers that use this interface.
 * 
 ******************************************************************************/
class Motor_SparkMax : public Motor
{
    public:
        Motor_SparkMax(int device_id);
        virtual ~Motor_SparkMax(void);

        void setPercent(double val);
        double getPercent(void);

        void setPosition(double position);
        double getPosition();
        double getRawPosition(void);
        void resetPosition(double position = 0.0);

        void setVelocity(double velocity);
        double getVelocity(void);
        double getRawVelocity(void);

        double getOutputCurrent();

        void doUpdate(void);

        void setMotorInvert(bool invert);
        bool getMotorInvert(void);

        void burnFlash(void);

        void setSensorInvert(bool invert);
 //       bool getSensorInvert(void);
        void setSensorScale(double scale, double offset=0);

        void setPid(BasicPid* pid);

        void setBrakeMode(bool brake);

        ControlModeType getControlMode();

        void setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak);
        // Current Limits are set to not burn up the motor
        // From Rev document 2/2024 for NEO 550
        // https://www.revrobotics.com/neo-550-brushless-motor-locked-rotor-testing/
        // Limit            Motor destruction
        // 20 A             N/A no failure before 227 seconds
        // 40 A             Fail after ~27 seconds
        // 60 A             Fail after ~5.5 seconds
        // 80 A             Fail after ~2.0 seconds
        void  setCurrentLimit(unsigned int amps=20);

        bool isUpperLimitPressed(void);
        bool isLowerLimitPressed(void);

        int getDeviceID();

        void setAsFollower(Motor_SparkMax& leader);
        void setFeedbackDevice(FeedbackDeviceType sensor_type, bool wrap);

        void setUpperLimitSwitch(bool normally_open, bool enabled, bool zero_position);
        void setLowerLimitSwitch(bool normally_open, bool enabled, bool zero_position);

        void setKp(double p);
        void setKi(double i);
        void setKd(double d);
        void setKf(double f);
        void setKz(double z);

        double getKp(void);
        double getKi(void);
        double getKd(void);
        double getKf(void);
        double getKz(void);

        static const double m_volt_max;

    private:
        int m_device_id;
        rev::CANSparkMax m_controller;
        rev::SparkPIDController m_pidController;
        rev::SparkRelativeEncoder m_encoder;
        rev::SparkLimitSwitch *m_forward_limit;
        rev::SparkLimitSwitch *m_reverse_limit;

        ControlModeType m_control_mode;
        FeedbackDeviceType m_sensor_type;

        bool m_forward_limit_pressed;
        bool m_reverse_limit_pressed;

        double m_output_scale;
        double m_output_offset;
        
        /**
         * Time scale to convert velocity to x / 100ms
         * By default, TalonFX considers velocity a change in position over 100ms 
         * per CTRE docs.
         */
        double m_velocity_timescale; 

};
