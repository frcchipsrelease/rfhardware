/*******************************************************************************
 *
 * File: Cancoder.h
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Encoder.h"

#include <ctre/phoenix6/CANcoder.hpp>

#include <string>

namespace rfh
{

/*******************************************************************************
 *
 * TODO write a readme
 *
 ******************************************************************************/
class Cancoder : public Encoder
{
    public:
        explicit Cancoder(int can_id, std::string can_bus);
        virtual ~Cancoder() = default;

        void setInvertDirection(bool invert);
        bool getInvertDirection(void);

        float getRawPosition(void);
        float getRawRate(void);

        float getAbsoluteRawPosition();

        float getAbsoluteAngle() {return getAbsolutePosition();}

    private:
        ctre::phoenix6::hardware::CANcoder m_cancoder;
};

} // end namespace
