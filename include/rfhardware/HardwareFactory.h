/*******************************************************************************
 *
 * File: HardwareFactory.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "frc/motorcontrol/MotorController.h"
#include "frc/Solenoid.h"
#include "frc/Encoder.h"
#include "frc/DoubleSolenoid.h"
#include "frc/Relay.h"
#include "frc/Servo.h"
#include "frc/DigitalOutput.h"
#include "frc/DigitalInput.h"
#include "frc/motorcontrol/Jaguar.h"
#include "frc/motorcontrol/Victor.h"
#include "frc/motorcontrol/VictorSP.h"
#include "frc/motorcontrol/Talon.h"

#include "gsinterfaces/Thread.h"
#include "gsutilities/tinyxml2.h"

#include "rfutilities/SimpleTrapCntl.h"

#include "rfhardware/AnalogIrSensor.h"
#include "rfhardware/AbsPosSensor.h"
#include "rfhardware/BasicPid.h"
#include "rfhardware/Encoder.h"
#include "rfhardware/Cancoder.h"
#include "rfhardware/LightsStrip.h"
#include "rfhardware/Limelight.h"
#include "rfhardware/LimitSwitch.h"
#include "rfhardware/Motor.h"
#include "rfhardware/Motor_Pwm.h"
#include "rfhardware/Motor_TalonSrx.h"
#include "rfhardware/Motor_TalonFx.h"
#include "rfhardware/Motor_TalonFxV6.h"
#include "rfhardware/Motor_SparkMax.h"
#include "rfhardware/ScaledAnalogInput.h"
#include "rfhardware/RpmCounter.h"
#include "rfhardware/Gyro_Analog.h"
#include "rfhardware/Gyro_Spi.h"
#include "rfhardware/Gyro_ADXRS450.h"
#include "rfhardware/Gyro_NavX.h"
#include "rfhardware/Gyro_Pigeon.h"
#include "rfhardware/CDoubleSolenoid.h"

/*******************************************************************************
 *
 * This class contains methods for parsing XML elements to create objects
 * for use by the robot.  This is intended to help keep the XML interface
 * consistent across many classes and to reduce the amount of duplicate 
 * code.
 * 
 ******************************************************************************/
class HardwareFactory
{
	public:
        static frc::Solenoid *createSolenoid(tinyxml2::XMLElement* xml);

        static rfh::Encoder* createEncoder(tinyxml2::XMLElement *xml);
        static frc::Encoder *createOpticalEncoder(tinyxml2::XMLElement *xml);
        static rfh::Cancoder *createCancoder(tinyxml2::XMLElement *xml);

        static frc::Relay *createRelay(tinyxml2::XMLElement* xml);
        static frc::Servo *createServo(tinyxml2::XMLElement* xml);
        static frc::DigitalInput *createDigitalInput(tinyxml2::XMLElement* xml);
        static frc::DigitalOutput *createDigitalOutput(tinyxml2::XMLElement* xml);

        static Motor* createMotor(tinyxml2::XMLElement* xml);

        static rfh::Gyro *createGyro(tinyxml2::XMLElement* xml);
        static rfh::Gyro_Analog *createGyroAnalog(tinyxml2::XMLElement* xml);
        static rfh::Gyro_Spi *createGyroSpi(tinyxml2::XMLElement* xml);
        static rfh::Gyro_ADXRS450 *createGyroADXRS450(tinyxml2::XMLElement* xml);
        static rfh::Gyro_NavX *createGyroNavX(tinyxml2::XMLElement* xml);
        static rfh::Gyro_Pigeon *createGyroPigeon(tinyxml2::XMLElement* xml);


        static AbsPosSensor *createAbsPosSensor(tinyxml2::XMLElement* xml);
        static AnalogIrSensor *createAnalogIrSensor(tinyxml2::XMLElement* xml);
        static frc::DoubleSolenoid *createDoubleSolenoid(tinyxml2::XMLElement* xml);
        static LimitSwitch *createLimitSwitch(tinyxml2::XMLElement* xml);
        static RpmCounter *createRpmCounter(tinyxml2::XMLElement* xml);
        static ScaledAnalogInput *createScaledAnalogInput(tinyxml2::XMLElement* xml);
        static Limelight *createLimelight(tinyxml2::XMLElement* xml);
        static LightsStrip *createLightsStrip(tinyxml2::XMLElement* xml);

        // These are not really hardware
        //
        static BasicPid *createPid(tinyxml2::XMLElement* xml);

		static SimpleTrapCntl *createTrapCntl(tinyxml2::XMLElement* xml);
//		static TrapezoidalProfile *createTrapezoid(tinyxml2::XMLElement* xml);
//		static WashoutCommand *createWashout(tinyxml2::XMLElement* xml);

    private:
        static void parseMotorCommon(Motor *motor, tinyxml2::XMLElement* xml);
        static void parseMotorPwm(Motor_Pwm *motor, tinyxml2::XMLElement* xml);
        static void parseMotorTalonSrx(Motor_TalonSrx *motor, tinyxml2::XMLElement* xml);
        static void parseMotorTalonFx(Motor_TalonFx *motor, tinyxml2::XMLElement* xml);
        static void parseMotorTalonFxV6(Motor_TalonFxV6 *motor, tinyxml2::XMLElement* xml);
        static void parseMotorSparkMax(Motor_SparkMax *motor, tinyxml2::XMLElement* xml);

        static void parseLimitSwitch(tinyxml2::XMLElement* xml, int16_t& port, LimitSwitch::InputMode& mode, bool& reset, double& reset_value);
        static void parseScaledAnalogInput(tinyxml2::XMLElement* xml, double& scale, double& offset, double& min_raw, double& max_raw);
};
