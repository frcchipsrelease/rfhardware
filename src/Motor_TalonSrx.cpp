/*******************************************************************************
 *
 * File: Motor_TalonSrx.cpp
 * 
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Motor_TalonSrx.h"

#include "gsutilities/Advisory.h"
#include "rfutilities/RobotUtil.h"

#include "ctre/phoenix/motorcontrol/SensorCollection.h"
#include "ctre/phoenix/motorcontrol/IMotorController.h"

using namespace ctre;
using namespace ctre::phoenix;
using namespace ctre::phoenix::motion;
using namespace ctre::phoenix::motorcontrol;
using namespace ctre::phoenix::motorcontrol::can;
using namespace ctre::phoenix::sensors;


//using namespace gsu;
#define CAN_TIMEOUT 0
//#define CAN_TIMEOUT kTimeout

/******************************************************************************
 *
 * Create an instance of a motor controller for the provided PWM Speed Controller
 *
 * WARNING: This class will take ownership of provided controller -- this class
 *          will delete the controller when finished using it if it is not
 *          a nullptr.
 *
 ******************************************************************************/
Motor_TalonSrx::Motor_TalonSrx(int device_id, int control_period_ms)
    : Motor()
    , m_controller(device_id)
{
    if (control_period_ms > 0)
    {
//        m_controller.setControlPeriod(control_period_ms);
    }

    m_forward_limit_pressed = false;
    m_reverse_limit_pressed = false;

    m_output_scale = 1.0;
    m_output_offset = 0.0;
}

/******************************************************************************
 *
 ******************************************************************************/
Motor_TalonSrx::~Motor_TalonSrx(void)
{
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setPercent(double percent)
{
    m_control_mode = ControlModeType::PERCENT;
    m_controller.Set(ControlMode::PercentOutput, percent);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getPercent()
{
    return m_controller.GetMotorOutputPercent();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setPosition(double position)
{
    m_control_mode = ControlModeType::POSITION;
    m_controller.Set(ControlMode::Position, (position-m_output_offset)/m_output_scale);

    //
    // should make sure sensors were set
//    if (((m_analog_input != nullptr) || (m_encoder_input != nullptr)) &&
//        (m_pid != nullptr))
//    {
//        m_control_mode = ControlModeType::POSITION;
//        m_target_position = position;
//    }
//    else
//    {
//        m_control_mode = ControlModeType::UNKNOWN_TYPE;
//    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getPosition()
{
    return ( ((getRawPosition() * m_output_scale) + m_output_offset) );
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getRawPosition(void)
{
    return m_controller.GetSelectedSensorPosition(0);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setVelocity(double velocity)
{
    m_control_mode = ControlModeType::VELOCITY;
    m_controller.Set(ControlMode::Velocity, (velocity / m_output_scale) * 0.1 ); // @TODO: fix hard coded value

    //
    // should make sure sensors were set
//    if ((m_encoder_input != nullptr) && (m_pid != nullptr))
//    {
//        m_control_mode = ControlModeType::VELOCITY;
//        m_target_velocity = velocity;
//    }
//    else
//    {
//        m_control_mode = ControlModeType::UNKNOWN_TYPE;
//    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getVelocity(void)
{
    return (getRawVelocity() * m_output_scale * 10.0);  // @TODO: fix hard coded value
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getRawVelocity(void)
{
    return m_controller.GetSelectedSensorVelocity(0);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setMotorInvert(bool invert)
{
    m_controller.SetInverted(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_TalonSrx::getMotorInvert(void)
{
    return m_controller.GetInverted();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setSensorInvert(bool invert)
{
    m_controller.SetSensorPhase(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
// bool Motor_TalonSrx::getSensorInvert(void)
// {
//     return m_controller.GetSensorPhase();
// }

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setSensorScale(double scale, double offset)
{
    m_output_scale = scale;
    m_output_offset = offset;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setBrakeMode(bool brake)
{
    if (brake)
    {
        m_controller.SetNeutralMode(NeutralMode::Brake);
    }
    else
    {
        m_controller.SetNeutralMode(NeutralMode::Coast);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::doUpdate(void)
{
    phoenix::motorcontrol::SensorCollection& sensor_data = m_controller.GetSensorCollection();
    m_forward_limit_pressed = (sensor_data.IsFwdLimitSwitchClosed() > 0);
    m_reverse_limit_pressed = (sensor_data.IsRevLimitSwitchClosed() > 0);
}

/******************************************************************************
 *
 ******************************************************************************/
Motor::ControlModeType Motor_TalonSrx::getControlMode()
{
    return m_control_mode;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setFeedbackDevice(FeedbackDeviceType sensor_type, bool wrap)
{
    switch(sensor_type)
    {
        case INTERNAL_ENCODER:
            m_controller.ConfigSelectedFeedbackSensor(FeedbackDevice::QuadEncoder, 0, CAN_TIMEOUT);
            break;

        case INTERNAL_POTENTIOMETER:
            m_controller.ConfigSelectedFeedbackSensor(FeedbackDevice::Analog, 0, CAN_TIMEOUT);
            m_controller.ConfigSetParameter(ParamEnum::eFeedbackNotContinuous, wrap, 0x00, 0x00, 0x00);
            break;

        case EXTERNAL_ENCODER:
        case EXTERNAL_POTENTIOMETER:
        default:
        {
        } break;
    }
}

/******************************************************************************
 *
 * This may be different then other motors.
 *
 * If this method is not called, the default values will be 1.0 and -1.0.
 *
 * @param fwd_nom   the maximum command to which the output will be set,
 *                  this will normally be positive -- between 0.0 and 1.0
 *
 * @param rev_nom   the minimum command to which the output will be set
 *                  this will normally be negative -- between -1.0 and 0.0
 *
 ******************************************************************************/
void Motor_TalonSrx::setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak)
{
    m_controller.ConfigPeakOutputForward(fwd_peak, CAN_TIMEOUT);
    m_controller.ConfigPeakOutputReverse(rev_peak, CAN_TIMEOUT);
    m_controller.ConfigNominalOutputForward(fwd_nom, CAN_TIMEOUT);
    m_controller.ConfigNominalOutputReverse(rev_nom, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_TalonSrx::isUpperLimitPressed(void)
{
    return m_forward_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_TalonSrx::isLowerLimitPressed(void)
{
    return m_reverse_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
int Motor_TalonSrx::getDeviceID()
{
    return m_controller.GetDeviceID();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setAsFollower(Motor_TalonSrx& leader)
{
    m_control_mode = ControlModeType::FOLLOWER;

    Advisory::pwarning("Not Implemented -- Motor_TalonSrx::setAsFollower");
//    m_controller.Follow(&leader);
//    m_controller.Set(ControlMode::Follower, leader);
}

/******************************************************************************
 *
 ******************************************************************************/
ctre::phoenix::motorcontrol::can::TalonSRX& Motor_TalonSrx::getController(void)
{
    return m_controller;
}

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) it will the talon will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 							the limit switch
 * 							NOTE: this could be updated to also allow for
 * 							the Talon to use external limit switches.
 *
 ******************************************************************************/
void Motor_TalonSrx::setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position, double position_value)
{
    Advisory::pinfo("        Motor_TalonSrx::setUpperLimitSwitch(%d,%d,%d,%f)", port, normally_open,reset_position, position_value);
    if (port >= 0)
    {
        m_controller.ConfigForwardLimitSwitchSource(
                LimitSwitchSource_FeedbackConnector,
                normally_open?LimitSwitchNormal_NormallyOpen:LimitSwitchNormal_NormallyClosed,
                CAN_TIMEOUT);

        m_controller.ConfigClearPositionOnLimitF(reset_position);
    }
    else
    {
        m_controller.ConfigForwardLimitSwitchSource(
                LimitSwitchSource_Deactivated,
                LimitSwitchNormal_Disabled,
                CAN_TIMEOUT);
    }
}

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) it will the talon will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 							the limit switch
 * 							NOTE: this could be updated to also allow for
 * 							the Talon to use external limit switches.
 *
 ******************************************************************************/
void Motor_TalonSrx::setLowerLimitSwitch  (int8_t port, bool normally_open, bool reset_position, double position_value)
{
    Advisory::pinfo("        Motor_TalonSrx::setLowerLimitSwitch(%d,%d,%d,%f)", port, normally_open,reset_position, position_value);
    if (port >= 0)
    {
        m_controller.ConfigReverseLimitSwitchSource(
                LimitSwitchSource_FeedbackConnector,
                normally_open?LimitSwitchNormal_NormallyOpen:LimitSwitchNormal_NormallyClosed,
                CAN_TIMEOUT);

        m_controller.ConfigClearPositionOnLimitR(reset_position);
    }
    else
    {
        m_controller.ConfigReverseLimitSwitchSource(
                LimitSwitchSource_Deactivated,
                LimitSwitchNormal_Disabled,
                CAN_TIMEOUT);
    }
}

/******************************************************************************
 *
 * NOTICE: This method takes ownership of the PID, this class will delete the
 *         PID when it is no longer needed.
 *
 ******************************************************************************/
void Motor_TalonSrx::setPid(BasicPid* pid)
{
    if (pid != nullptr)
    {
        m_controller.Config_kF(0, pid->getKf(), CAN_TIMEOUT);
        m_controller.Config_kP(0, pid->getKp(), CAN_TIMEOUT);
        m_controller.Config_kI(0, pid->getKi(), CAN_TIMEOUT);
        m_controller.Config_kD(0, pid->getKd(), CAN_TIMEOUT);
        m_controller.Config_IntegralZone(0, (int)pid->getKz(), CAN_TIMEOUT);

        m_controller.ConfigPeakOutputReverse(-1.0, CAN_TIMEOUT);
        m_controller.ConfigPeakOutputForward(1.0, CAN_TIMEOUT);

        m_controller.ConfigNominalOutputReverse(pid->getLowerCommandLimit(), CAN_TIMEOUT);
        m_controller.ConfigNominalOutputForward(pid->getUpperCommandLimit(), CAN_TIMEOUT);

        delete pid;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setKp(double p)
{
    m_controller.Config_kP(0, p, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setKi(double i)
{
    m_controller.Config_kI(0, i, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setKd(double d)
{
    m_controller.Config_kD(0, d, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setKf(double f)
{
    m_controller.Config_kF(0, f, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonSrx::setKz(double z)
{
    m_controller.Config_IntegralZone(0, (int)z, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getKp(void)
{
    return m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_P, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getKi(void)
{
    return  m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_I, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getKd(void)
{
    return  m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_D, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getKf(void)
{
    return  m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_F, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonSrx::getKz(void)
{
    return  m_controller.ConfigGetParameter(ctre::phoenix::ParamEnum::eProfileParamSlot_IZone, 0, CAN_TIMEOUT);
}
