/*******************************************************************************
 *
 * File: LightsStrip.cpp
 *
 * Written by:
 *  Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *  NASA, Johnson Space Center
 *
 * Acknowledgements:
 *  some light patterns based on 3847 spectrum's photon library <3
 *  https://github.com/Spectrum3847/Spectrum-Photon
 *
 * Copywrite and License:
 *  Copywrite and License information can be found in the LICENSE.md file 
 *  distributed with this software.
 *
 ******************************************************************************/

#include "rfhardware/LightsStrip.h"
#include "gsutilities/Advisory.h"

using namespace frc;

/**
 * creates a new addressable led strip with the provided length and pwm port
 * @param length total number of leds in the strip
 * @param port pwm port on the roborio that the leds' data pin is connected to
*/
LightsStrip::LightsStrip(int length, int port)
{
	led = new AddressableLED(port);
	strip_length = length;
	led->SetLength(strip_length);
	buffer.resize(strip_length);
}

LightsStrip::~LightsStrip()
{
}

/**
 * 
*/
void LightsStrip::start(void)
{
	for (int i = 0; i < strip_length; i++)
	{
		// dim orange; if you see this, something went wrong when setting the pattern
		buffer[i].SetRGB(24, 6, 0);
	}
	led->SetData(buffer);
	led->Start();
}

/**
 * 
*/
void LightsStrip::setData(void)
{
	led->SetData(buffer);
}

/**
 * 
*/
int LightsStrip::getLength(void)
{
    return strip_length;
}

/**
 * todo
 * 
 * @param pattern
 * @param start_idx
 * @param end_idx
 * @param color_1 (r, g, b) [0, 255]
 * @param color_2 (r, g, b) [0, 255]
 * 
 */
void LightsStrip::setStaticPattern(int pattern, int start_idx, int end_idx, int color_1[3], int color_2[3])
{
    int length = end_idx - start_idx; // end_idx is exclusive
    switch (pattern)
    {
        case NONE:
        {
            for (int i = 0; i < length; i++)
            {
                buffer[start_idx + i].SetRGB(0, 0, 0);
            }
        } break;

        case SOLID:
        {
            for (int i = 0; i < length; i++)
            {
                buffer[start_idx + i].SetRGB(color_1[0], color_1[1], color_1[2]);
            }
        } break;

        case GRADIENT:
        {
            for (int i = 0; i < length; i++)
            {
                float pct = (float)i / (length-1);
                int r = color_1[0]*(1-pct) + color_2[0]*(pct);
                int g = color_1[1]*(1-pct) + color_2[1]*(pct);
                int b = color_1[2]*(1-pct) + color_2[2]*(pct);
                buffer[start_idx + i].SetRGB(r, g, b);
            }
        } break;

        case DASHED: 
        {
            for (int i = 0; i < length; i++)
            {
                if (i % 8 < 4) // if (i % (2*dash length) < dash length)
                {
                    buffer[start_idx + i].SetRGB(color_1[0], color_1[1], color_1[2]);
                }
                else
                {
                    buffer[start_idx + i].SetRGB(color_2[0], color_2[1], color_2[2]);
                }
            }
        } break;

        case GRADIENT_DASHED:
        {
            for (int i = 0; i < length; i++)
            {
                if (i % 2 < 1) // if (i % (2*dash length) < dash length)
                {
                    float pct = (float)i / (length-1);
                    int r = color_1[0]*(1-pct) + color_2[0]*(pct);
                    int g = color_1[1]*(1-pct) + color_2[1]*(pct);
                    int b = color_1[2]*(1-pct) + color_2[2]*(pct);
                    buffer[start_idx + i].SetRGB(r, g, b);
                }
                else
                {
                    buffer[start_idx + i].SetRGB(0, 0, 0);
                }
            }
        } break;

        case THEATER_CHASE: // 3847
        {
            for (int i = 0; i < length; i++)
            {
                if (i % 3 == 0)
                {
                    buffer[start_idx + i].SetRGB(color_1[0], color_1[1], color_1[2]);
                }
                else
                {
                    buffer[start_idx + i].SetRGB(0, 0, 0);
                }
            }
        } break;

        default:
        {
        } break;
    }
}


/**
 * todo
 * 
 * @param pattern
 * @param start_idx
 * @param end_idx
 * @param color_1 (R, G, B) [0, 255]
 * @param color_2 (R, G, B) [0, 255]
 * @param count
 * 
 */
void LightsStrip::setMovingPattern(int pattern, int start_idx, int end_idx, int color_1[3], int color_2[3], int count)
{
    int length = end_idx - start_idx; // end_idx is exclusive
    switch (pattern)
    {
        case DASHED: 
        {
            for (int i = 0; i < length; i++)
            {
                if (i % 8 < 4) // each dash is 4 lights long
                {
                    buffer[start_idx + (i+count)%(length)].SetRGB(color_1[0], color_1[1], color_1[2]);
                }
                else
                {
                    buffer[start_idx + (i+count)%(length)].SetRGB(color_2[0], color_2[1], color_2[2]);
                }
            }
        } break;

        case THEATER_CHASE: 
        {
            for (int i = 0; i < length; i++)
            {
                if (i % 3 == 0) // every third light
                {
                    buffer[start_idx + (i+count)%(length)].SetRGB(color_1[0], color_1[1], color_1[2]);
                }
                else
                {
                    buffer[start_idx + (i+count)%(length)].SetRGB(0, 0, 0);
                }
            }
        } break;

        case LOADING: 
        {
            for (int i = 0; i < length; i++)
            {
                if (i <= count)
                {
                    buffer[start_idx + i].SetRGB(color_1[0], color_1[1], color_1[2]);
                }
                else
                {
                    buffer[start_idx + i].SetRGB(0, 0, 0);
                }
            }
        } break;

        case REVERSE_LOADING: 
        {
            for (int i = 0; i < length*-1; i++)
            {
                if (i <= count)
                {
                    buffer[end_idx - 1 - i].SetRGB(color_1[0], color_1[1], color_1[2]);
                }
                else
                {
                    buffer[end_idx - 1 - i].SetRGB(0, 0, 0);
                }
            }
        } break;

        case SHOOTING_STAR:
        {
            for (int i = 0; i < length; i++)
            {
                buffer[start_idx + i].SetRGB(0, 0, 0);
            }
            for (int i = 1; i <= 16; i++) // 16 = length of trail
            {
                int r = color_1[0]*i*i / 256; // 256 = length^2 = 16^2
                int g = color_1[1]*i*i / 256;
                int b = color_1[2]*i*i / 256;
                buffer[start_idx + (i+count)%length].SetRGB(r, g, b);
            }
        } break;
        
        case GRADIENT_DASHED:
        {
            for (int i = 0; i < length; i++)
            {
                if (i % 32 < 16) // if (i % (2*dash length) < dash length)
                {
                    int pct = i%16;
                    int r = color_1[0]*(pct)*(pct) / 256;
                    int g = color_1[1]*(pct)*(pct) / 256;
                    int b = color_1[2]*(pct)*(pct) / 256;
                    buffer[start_idx + (i+count)%length].SetRGB(r, g, b);
                }
                else
                {
                    buffer[start_idx + (i+count)%length].SetRGB(0, 0, 0);
                }
            }
        } break;
        
        default:
        {
        } break;
    }
}
