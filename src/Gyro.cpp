/*******************************************************************************
 *
 * File: Gyro.cpp
 *
 * WARNING: This class expects either the Calibrate method or the GetAngle 
 * 			method to be called every period.  Calibrate should be called when
 * 			the robot is not moving (disabled)
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Gyro.h"

namespace rfh
{

/*******************************************************************************
 *
 * Create an instance of the Gyro class.
 * 
 ******************************************************************************/
Gyro::Gyro(void)
{
    is_ready = false;
}

/*******************************************************************************
 *
 * Release any resources allocated by this class.
 *
 ******************************************************************************/
Gyro::~Gyro(void)
{

}

/*******************************************************************************
 *
 * Return true if this sensor is ready for use
 *
 ******************************************************************************/
bool Gyro::isReady(void) const
{
    return is_ready;
}

/*******************************************************************************
 *
 * Return the current angle
 *
 ******************************************************************************/
double Gyro::getAngle(void) const
{
    return 0.0;
}

/*******************************************************************************
 *
 * Return the current rate
 *
 ******************************************************************************/
double Gyro::getRate(void) const
{
    return 0.0;
}

/*******************************************************************************
 *
 * Return the current pitch
 *
 ******************************************************************************/
double Gyro::getPitch(void) const
{
    return 0.0;
}

/*******************************************************************************
 *
 * Return the current roll
 *
 ******************************************************************************/
double Gyro::getRoll(void) const
{
    return 0.0;
}


/*******************************************************************************
 *
 * Return the current heading as a Rotation2d object
 *
 ******************************************************************************/
frc::Rotation2d Gyro::getRotation2d(void) const
{
    return frc::Rotation2d(1.0, 0.0);
}


/*******************************************************************************
 *
 * Reset the Gyro
 *
 ******************************************************************************/
void Gyro::reset(void)
{

}

/*******************************************************************************
 *
 * Calibrate the gyro
 *
 ******************************************************************************/
void Gyro::calibrate()
{

}

/*******************************************************************************
 *
 * Return the yaw
 *
 ******************************************************************************/
double Gyro::getYaw() const
{
    return 0.0;
}

/*******************************************************************************
 *
 * Return the current compass heading
 *
 ******************************************************************************/
double Gyro::getCompassHeading() const
{
    return 0.0;
}

/*******************************************************************************
 *
 * Return the current acceleration along the x-axis
 *
 ******************************************************************************/
double Gyro::getWorldLinearAccelX() const
{
    return 0.0;
}

/*******************************************************************************
 *
 * Return the current acceleration along the y-axis
 *
 ******************************************************************************/
double Gyro::getWorldLinearAccelY() const
{
    return 0.0;
}

/*******************************************************************************
 *
 * Return the current acceleration along the z-axis
 *
 ******************************************************************************/
double Gyro::getWorldLinearAccelZ() const
{
    return 0.0;
}

/*******************************************************************************
 *
 * Return true if moving, false if not moving
 *
 ******************************************************************************/
bool Gyro::isMoving() const
{ 
    return false;
}

/*******************************************************************************
 *
 * Return true if rotating, false if not rotating
 *
 ******************************************************************************/
bool Gyro::isRotating() const
{
    return false;
}

/*******************************************************************************
 *
 * Return true if connected, false if not connected
 *
 ******************************************************************************/
bool Gyro::isConnected() const
{
    return false;
}

} // end namespace