/*******************************************************************************
 *
 * File: LimitSwitch.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/LimitSwitch.h"

/******************************************************************************
 * 
 * Create an interface to a limit switch on the specified port.
 * 
 * @param	port        the channel or port that the switch is on
 *
 * @param   mode        indicates the type of connected limit switch
 *
 * @param   reset       this value is just held by instances of this class it is
 *                      retrievable so it can be as related code needs it
 *
 * @param   reset_value this value is just held by instances of this class it is
 *                      retrievable so it can be as related code needs it
 * 
 ******************************************************************************/
LimitSwitch::LimitSwitch(uint16_t port, InputMode mode, bool reset, double reset_value)
    : frc::DigitalInput(port)
{
    m_mode = mode;
    m_reset = reset;
    m_reset_value = reset_value;
}

/******************************************************************************
 *
 ******************************************************************************/
LimitSwitch::~LimitSwitch(void)
{
}

/******************************************************************************
 *
 * Getters and Setters
 *
 ******************************************************************************/
void LimitSwitch::setMode(InputMode mode) { m_mode = mode; }
LimitSwitch::InputMode LimitSwitch::getMode(void) const { return m_mode; }

void LimitSwitch::setReset(bool reset) { m_reset = reset; }
bool LimitSwitch::isReset(void) { return m_reset; }

void LimitSwitch::setResetValue(double value) {m_reset_value = value; }
double LimitSwitch::getResetValue(void) { return m_reset_value; }

/******************************************************************************
 * 
 * @return  true if the switch is pressed
 * 
 ******************************************************************************/
bool LimitSwitch::isPressed(void) const
{
    return(frc::DigitalInput::Get() == (m_mode == INPUT_NORMALLY_CLOSED));
}
