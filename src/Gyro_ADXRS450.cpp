/*******************************************************************************
 *
 * File: Gyro_ADXRS450.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *  This file is based on the WPILib ADXRS450 Gyro class with minor
 *  modifications to allow for more configuration and use of the ADXRS453 gyro
 *  on the Spartan board.
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Gyro_ADXRS450.h"

#include "frc/DriverStation.h"
#include "frc/livewindow/LiveWindow.h"
#include "gsutilities/Advisory.h"
#include "frc/Timer.h"

static constexpr double kSamplePeriod = 0.001;
#ifdef WIN32
static constexpr double kCalibrationSampleTime = 5.0;
#else
static units::second_t kCalibrationSampleTime = 5.0_s;
#endif

static constexpr double kDegreePerSecondPerLSB = 0.0125;

static constexpr uint8_t kRateRegister = 0x00;
static constexpr uint8_t kTemRegister = 0x02;
static constexpr uint8_t kLoCSTRegister = 0x04;
static constexpr uint8_t kHiCSTRegister = 0x06;
static constexpr uint8_t kQuadRegister = 0x08;
static constexpr uint8_t kFaultRegister = 0x0A;
static constexpr uint8_t kPIDRegister = 0x0C;
static constexpr uint8_t kSNHighRegister = 0x0E;
static constexpr uint8_t kSNLowRegister = 0x10;

using namespace frc;

namespace rfh
{

/*******************************************************************************
 *
 * Gyro constructor on the specified SPI port.
 *
 * @param port The SPI port the gyro is attached to.
 *
 ******************************************************************************/
Gyro_ADXRS450::Gyro_ADXRS450(SPI::Port port, bool invert)
    : Gyro()
    , m_gyro(port)
    , m_invert(1.0)
    , m_is_ready(false)
{
    if(invert)
    {
        m_invert = -1.0;
    }

    m_is_ready = true;
}


/*******************************************************************************
 *
 ******************************************************************************/
bool Gyro_ADXRS450::isReady() const
{
    return m_is_ready;
}

/*******************************************************************************
 *
 * Initialize the gyro.
 * Calibrate the gyro by running for a number of samples and computing the
 * center value.
 * Then use the center value as the Accumulator center value for subsequent
 * measurements.
 * It's important to make sure that the robot is not moving while the centering
 * calculations are in progress, this is typically done when the robot is first
 * turned on while it's sitting at rest before the competition starts.
 *
 ******************************************************************************/
void Gyro_ADXRS450::calibrate()
{
 #ifdef WIN32
    Wait(0.1);
#else
    Wait(0.1_s);
#endif

    m_gyro.Calibrate();
}

/*******************************************************************************
 *
 ******************************************************************************/
static inline uint32_t BytesToIntBE(uint8_t* buf)
{
    uint32_t result = ((uint32_t) buf[0]) << 24;
    result |= ((uint32_t) buf[1]) << 16;
    result |= ((uint32_t) buf[2]) << 8;
    result |= (uint32_t) buf[3];
    return result;
}

/*******************************************************************************
 * TODO
 ******************************************************************************/
uint16_t Gyro_ADXRS450::readRegister(uint8_t reg)
{
    // m_gyro.ReadRegister(reg);
    return 0;
}

/*******************************************************************************
 *
 * Reset the gyro.
 * Resets the gyro to a heading of zero. This can be used if there is
 * significant
 * drift in the gyro and it needs to be recalibrated after it has been running.
 *
 ******************************************************************************/
void Gyro_ADXRS450::reset()
{
    m_gyro.Reset();
}

/*******************************************************************************
 *
 * Return the actual angle in degrees that the robot is currently facing.
 *
 * The angle is based on the current accumulator value corrected by the
 * oversampling rate, the
 * gyro type and the A/D calibration values.
 * The angle is continuous, that is it will continue from 360->361 degrees. This
 * allows algorithms that wouldn't
 * want to see a discontinuity in the gyro output as it sweeps from 360 to 0 on
 * the second time around.
 *
 * @return the current heading of the robot in degrees. This heading is based on
 * integration
 * of the returned rate from the gyro.
 *
 ******************************************************************************/
double Gyro_ADXRS450::getAngle() const
{
    return m_gyro.GetAngle();
}

/*******************************************************************************
 *
 * Return the rate of rotation of the gyro
 *
 * The rate is based on the most recent reading of the gyro analog value
 *
 * @return the current rate in degrees per second
 *
 ******************************************************************************/
double Gyro_ADXRS450::getRate() const
{
    return m_gyro.GetRate();
}

/*******************************************************************************
 * TODO
 ******************************************************************************/
int Gyro_ADXRS450::getPort() const
{
    return m_gyro.GetPort();
}

} // end namespace
