/*******************************************************************************
 *
 * File: Gyro_Analog.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Gyro_Analog.h"

#include <math.h>
#include <chrono>
#include <thread>

namespace rfh
{

/*******************************************************************************
 *
 * Create an instance of the RGyro class on the specified channel and default 
 * module.  Note: because of CRIO hardware, the Gyro must be connected to
 * module 1, channel 1,  but Gyro provides this constructor so it's passed on.
 * 
 * @param	channel	the channel to which the Gyro is connected
 * 
 ******************************************************************************/
Gyro_Analog::Gyro_Analog(uint32_t channel)
    : Gyro()
{
    is_ready = false;
    m_analog_gyro = new frc::AnalogGyro(channel);
    reset();
    calibrate();
}

/*******************************************************************************
 *
 * Release any resources allocated by this class.
 * 
 ******************************************************************************/
Gyro_Analog::~Gyro_Analog(void)
{
    is_ready = false;

    if (m_analog_gyro != nullptr)
    {
        delete m_analog_gyro;
        m_analog_gyro = nullptr;
    }
}

/*******************************************************************************
 *
 * @return the current gyro angle
 *
 ******************************************************************************/
double Gyro_Analog::getAngle() const
{
    if (is_ready)
    {
        return m_analog_gyro->GetAngle();
    }

    return 0.0;
}

/*******************************************************************************
 *
 ******************************************************************************/
void Gyro_Analog::setSensitivity(double value)
{
    m_analog_gyro->SetSensitivity(value);
}

/*******************************************************************************
 *
 * @return the current gyro rate
 *
 ******************************************************************************/
double Gyro_Analog::getRate() const
{
    if (is_ready)
    {
        return m_analog_gyro->GetRate();
    }

    return 0.0;
}

/*******************************************************************************
 *
 * Reset the gyro and the calibrated offset.  Also does a check to make sure
 * the gyro has not been unplugged or broken.
 * 
 ******************************************************************************/
void Gyro_Analog::reset()
{
    //
    // reset the gyro,
    // give it some time to finish the reset,
    // check to make sure the reset worked
    //
    is_ready = false;

    if (m_analog_gyro != nullptr)
    {
        m_analog_gyro->Reset();
        offset = 0.0;

        // Wait for a little while to make sure the gyro gets reset
        // reference "Einstein 2012, 118 doesn't move" for more info
        int count = 0;
        while ((fabs(m_analog_gyro->GetAngle()) > 1.0) && count < 20)
        {
            count++;
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        if (count < 20)
        {
            is_ready = true;
        }
    }
}

/*******************************************************************************
 *
 * Reset the gyro and the calibrated offset.  Also does a check to make sure
 * the gyro has not been unplugged or broken.
 *
 ******************************************************************************/
void Gyro_Analog::calibrate()
{
    if (m_analog_gyro != nullptr)
    {
        // Calibrate in AnalogGyro has a built in 5 second wait time;
        m_analog_gyro->Calibrate();
    }
}

} // end namespace
