/*******************************************************************************
 *
 * File: CDoubleSolenoid.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/CDoubleSolenoid.h"

/******************************************************************************
 *
 * Create an interface to an APS plugged into the specified channel.
 *
 * @param	channel	the channel or port that the pot is in
 *
 ******************************************************************************/
CDoubleSolenoid::CDoubleSolenoid(uint32_t module, frc::PneumaticsModuleType pm_type, uint32_t channel_a, uint32_t channel_b)
{
     // 2022: starting to differentiate between REV and CTRE PCMs
    solenoid_a = new frc::Solenoid(module, pm_type, channel_a);
    solenoid_b = new frc::Solenoid(module, pm_type, channel_b);
}

/******************************************************************************
 *
 ******************************************************************************/
CDoubleSolenoid::~CDoubleSolenoid(void)
{
	if(nullptr != solenoid_a)
	{
		delete solenoid_a;
        solenoid_a = nullptr;
	}

	if(nullptr != solenoid_b)
	{
		delete solenoid_b;
        solenoid_b = nullptr;
	}
}

/******************************************************************************
 * 
 ******************************************************************************/
void CDoubleSolenoid::set(bool val)
{
	solenoid_a->Set(val);
	solenoid_b->Set(!val);
}
