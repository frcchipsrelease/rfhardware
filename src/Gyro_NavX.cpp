/*******************************************************************************
 *
 * File: Gyro_NavX.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	FRC Team 118, The Robonauts
 * 	NASA, Johnson Space Center
 *
 ***********************************************************************
 * navX-MXP:
 * - Communication via RoboRIO MXP (SPI, I2C) and USB.            
 * - See http://navx-mxp.kauailabs.com/guidance/selecting-an-interface.
 * 
 * navX-Micro:
 * - Communication via I2C (RoboRIO MXP or Onboard) and USB.
 * - See http://navx-micro.kauailabs.com/guidance/selecting-an-interface.
 * 
 * VMX-pi:
 * - Communication via USB.
 * - See https://vmx-pi.kauailabs.com/installation/roborio-installation/
 * 
 * Multiple navX-model devices on a single robot are supported.
 *************************************************************************
 ******************************************************************************/
#include "rfhardware/Gyro_NavX.h"

#include "frc/DriverStation.h"
#include "frc/livewindow/LiveWindow.h"
#include "gsutilities/Advisory.h"
#include "frc/Timer.h"

static constexpr double kSamplePeriod = 0.001;
#ifdef WIN32
static constexpr double kCalibrationSampleTime = 5.0;
#else
static units::second_t kCalibrationSampleTime = 5.0_s;
#endif

static constexpr double kDegreePerSecondPerLSB = 0.0125;

static constexpr uint8_t kRateRegister = 0x00;
static constexpr uint8_t kTemRegister = 0x02;
static constexpr uint8_t kLoCSTRegister = 0x04;
static constexpr uint8_t kHiCSTRegister = 0x06;
static constexpr uint8_t kQuadRegister = 0x08;
static constexpr uint8_t kFaultRegister = 0x0A;
static constexpr uint8_t kPIDRegister = 0x0C;
static constexpr uint8_t kSNHighRegister = 0x0E;
static constexpr uint8_t kSNLowRegister = 0x10;

using namespace frc;

namespace rfh
{

/*******************************************************************************
 *
 * NavX Regular constructor on the specified SPI port
 *
 * @param port The SPI port the NavX is attached to.
 *
 ******************************************************************************/
Gyro_NavX::Gyro_NavX(SPI::Port port)
    : Gyro()
{
    navXGyro = new AHRS(port);
    m_is_ready = true;
}


/*******************************************************************************
 *
 * NavX Micro constructor on the specified I2C port
 *
 * @param port The I2C port the NavX is attached to.
 * @param is_micro should always be true, this param just exists to differentiate the micro and regular constructors
 *
 ******************************************************************************/
Gyro_NavX::Gyro_NavX(I2C::Port port, bool is_micro)
    : Gyro()
{
    navXGyro = new AHRS(port);
    m_is_ready = true;
}


/*******************************************************************************
 *
 ******************************************************************************/
bool Gyro_NavX::isReady() const
{
    return( m_is_ready);
}

/*******************************************************************************
 *
 * Initialize the gyro.
 * Calibrate the gyro by running for a number of samples and computing the
 * center value.
 * Then use the center value as the Accumulator center value for subsequent
 * measurements.
 * It's important to make sure that the robot is not moving while the centering
 * calculations are in progress, this is typically done when the robot is first
 * turned on while it's sitting at rest before the competition starts.
 *
 ******************************************************************************/
void Gyro_NavX::calibrate()
{

}

/*******************************************************************************
 *
 ******************************************************************************/
// static bool CalcParity(uint32_t v)
// {
//     bool parity = false;

//     return parity;
// }


/*******************************************************************************
 *
 * Reset the gyro.
 * Resets the gyro to a heading of zero. This can be used if there is
 * significant
 * drift in the gyro and it needs to be recalibrated after it has been running.
 *
 ******************************************************************************/
void Gyro_NavX::reset()
{
       navXGyro->ZeroYaw();
}

/*******************************************************************************
 *
 * Return the actual angle in degrees that the robot is currently facing.
 *
 * The angle is based on the current accumulator value corrected by the
 * oversampling rate, the
 * gyro type and the A/D calibration values.
 * The angle is continuous, that is it will continue from 360->361 degrees. This
 * allows algorithms that wouldn't
 * want to see a discontinuity in the gyro output as it sweeps from 360 to 0 on
 * the second time around.
 *
 * @return the current heading of the robot in degrees. This heading is based on
 * integration
 * of the returned rate from the gyro.
 *
 ******************************************************************************/
double Gyro_NavX::getAngle() const
{
    return navXGyro->GetAngle(); // cw +
}

/*******************************************************************************
 *
 * Return the actual angle in degrees that the robot is currently facing as a
 * Rotation2d object.
 *
 * The angle is based on the current accumulator value corrected by the
 * oversampling rate, the
 * gyro type and the A/D calibration values.
 * The angle is continuous, that is it will continue from 360->361 degrees. This
 * allows algorithms that wouldn't
 * want to see a discontinuity in the gyro output as it sweeps from 360 to 0 on
 * the second time around.
 *
 * @return the current heading of the robot in degrees as a Rotation2d object.
 * This heading is based on integration
 * of the returned rate from the gyro.
 *
 ******************************************************************************/
frc::Rotation2d Gyro_NavX::getRotation2d() const
{
    double angle = navXGyro->GetAngle();
    return frc::Rotation2d(-1 * angle*1_deg); // invert angle to convert to wpilib coordinate system (ccw +)
}

/*******************************************************************************
 *
 * Return the rate of rotation of the gyro
 *
 * The rate is based on the most recent reading of the gyro analog value
 *
 * @return the current rate in degrees per second
 *
 ******************************************************************************/
double Gyro_NavX::getRate() const
{
    // jly HUH does this not work?
    // jr this should work I think, haven't tested it tho
    return navXGyro->GetRate();
}

double Gyro_NavX::getPitch() const
{
    return static_cast<double>(navXGyro->GetPitch());
}

double Gyro_NavX::getRoll() const
{
    return static_cast<double>(navXGyro->GetRoll());
}

double Gyro_NavX::getYaw() const
{
    return static_cast<double>(navXGyro->GetYaw());
}

double Gyro_NavX::getCompassHeading() const
{
    return static_cast<double>(navXGyro->GetCompassHeading());
}

double Gyro_NavX::getWorldLinearAccelX() const
{
    return static_cast<double>(navXGyro->GetWorldLinearAccelX());
}

double Gyro_NavX::getWorldLinearAccelY() const
{
    return static_cast<double>(navXGyro->GetWorldLinearAccelY());
}
 
double Gyro_NavX::getWorldLinearAccelZ() const
{
    return static_cast<double>(navXGyro->GetWorldLinearAccelZ());
}
 
bool Gyro_NavX::isMoving() const
{
    return navXGyro->IsMoving();
}
 
bool Gyro_NavX::isRotating() const
{
    return navXGyro->IsRotating();
}

bool Gyro_NavX::isConnected() const
{
    return navXGyro->IsConnected();
}

} // end namespace
