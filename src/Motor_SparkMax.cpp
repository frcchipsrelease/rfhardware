/*******************************************************************************
 *
 * File: Motor_SparkMax.cpp
 * 
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Motor_SparkMax.h"

#include "gsutilities/Advisory.h"
#include "rfutilities/RobotUtil.h"


//using namespace gsu;
#define CAN_TIMEOUT 0
//#define CAN_TIMEOUT kTimeout

const double Motor_SparkMax::m_volt_max = 12.0;

/******************************************************************************
 *
 * Create an instance of a motor controller for the SparkMax Motor Controller
 *
 * WARNING: This class will take ownership of provided motor controller -- this class
 *          will delete the controller when finished using it if it is not
 *          a nullptr.
 *
 ******************************************************************************/
Motor_SparkMax::Motor_SparkMax(int device_id)
    : Motor()
    , m_controller(device_id, rev::CANSparkMax::MotorType::kBrushless)
    , m_pidController(m_controller.GetPIDController())
    , m_encoder(m_controller.GetEncoder(rev::SparkRelativeEncoder::Type::kHallSensor))
{
//        m_hall_encoder = new rev::SparkMaxRelativeEncoder(m_controller->GetEncoder(rev::SparkMaxRelativeEncoder::Type::kHallSensor, ticks_per_rev));  // 42 is ticks_per_rev on neos


    m_forward_limit = nullptr;
    m_reverse_limit = nullptr;
    m_device_id = device_id;
    m_sensor_type = FeedbackDeviceType::INTERNAL_ENCODER;
/*
        rev::CANSparkMax m_controller{deviceID, rev::CANSparkMax::MotorType::kBrushless};
    if (control_period_ms > 0)
    {
        m_controller.SetControlFramePeriod(ControlFrameEnhanced::Control_1_General, control_period_ms);
    }
    else
    {
        m_controller.SetControlFramePeriod(ControlFrameEnhanced::Control_1_General, 10);
    }
*/
/*
    if (status_period_general_ms > 0)
    {
        m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_1_General, status_period_general_ms, CAN_TIMEOUT);
    }
    else
    {
        m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_1_General, 10, CAN_TIMEOUT);
    }

    if (status_period_feedback_ms > 0)
    {
        m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_2_Feedback0, status_period_feedback_ms, CAN_TIMEOUT);
    }
    else
    {
        m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_2_Feedback0, 20, CAN_TIMEOUT);
    }
*/    
    m_forward_limit_pressed = false;
    m_reverse_limit_pressed = false;

    m_output_scale = 1.0;
    m_output_offset = 0.0;
    m_velocity_timescale = 10.0; // 1 s = 10 * (100ms) where 100ms is TalonFx unitary time

//        SetFeedbackDevice(RSpeedController::INTERNAL_ENCODER, 42);  // 42 is default for NEOs 
 //   setFeedbackDevice(INTEGRATED_SENSOR, true);
 //   m_controller.SetSelectedSensorPosition(0);   // set the Integrated Encoder to a raw position of 0
}

/******************************************************************************
 *
 ******************************************************************************/
Motor_SparkMax::~Motor_SparkMax(void)
{
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setPercent(double percent)
{
    m_pidController.SetReference(percent, rev::CANSparkMax::ControlType::kDutyCycle);
    m_controller.Set(percent);
    //if (fabs(percent) >0.05) {
            //Advisory::pwarning(" -------- SetPercent: %.2f", percent);
    //}
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getPercent()
{
    return m_controller.Get();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setPosition(double position)
{
    m_control_mode = ControlModeType::POSITION;
    float val = (position - m_output_offset)/m_output_scale;
 //   Advisory::pinfo("     SparkMaxCan val: %f)", val);
    m_pidController.SetReference(val, rev::CANSparkLowLevel::ControlType::kPosition);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getPosition()
{
    return ( ((getRawPosition() * m_output_scale) + m_output_offset) );
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getRawPosition(void)
{
    float raw_pos = 0.0;

    if (m_sensor_type == FeedbackDeviceType::INTERNAL_ENCODER) // ||
    {
        raw_pos = (float)m_encoder.GetPosition();
    }
    // else if (m_sensor_type == FeedbackDeviceType::INTERNAL_POTENTIOMETER)
    // {
 //       raw_pos = (float)m_potentiometer->GetPosition();
    // }
    else
    {
        raw_pos = 0.0;
    }
    return (raw_pos);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::resetPosition(double position)
{
    if (m_sensor_type == FeedbackDeviceType::INTERNAL_ENCODER)
    {
        m_encoder.SetPosition(position);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setVelocity(double velocity)
{
    //m_control_mode = ControlModeType::VELOCITY;
    //m_controller.Set(ControlMode::Velocity, (velocity / m_output_scale / m_velocity_timescale) );

    //
    // should make sure sensors were set
//    if ((m_encoder_input != nullptr) && (m_pid != nullptr))
//    {
//        m_control_mode = ControlModeType::VELOCITY;
//        m_target_velocity = velocity;
//    }
//    else
//    {
//        m_control_mode = ControlModeType::UNKNOWN_TYPE;
//    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getVelocity(void)
{
    return (getRawVelocity() * m_output_scale * m_velocity_timescale);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getRawVelocity(void)
{
    double raw_vel;
    if (m_sensor_type == FeedbackDeviceType::INTERNAL_ENCODER)
        // ||m_sensor_type == FeedbackDeviceType::SPARK_MAX_ALTERNATE_ENCODER)
    {
        raw_vel = m_encoder.GetVelocity();
    }
    else
    {
        raw_vel = 0.0;
    }

    return (raw_vel);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getOutputCurrent()
{
    return m_controller.GetOutputCurrent();
}
/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setMotorInvert(bool invert)
{
    m_controller.SetInverted(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_SparkMax::getMotorInvert(void)
{
    return m_controller.GetInverted();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setSensorInvert(bool invert)
{
    //m_controller.SetSensorPhase(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
// bool Motor_SparkMax::getSensorInvert(void)
// {
//     return m_controller.GetSensorPhase();
// }

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setSensorScale(double scale, double offset)
{
    m_output_scale = scale;
    m_output_offset = offset;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setBrakeMode(bool brake)
{
    if (brake)
    {
      //  Advisory::pinfo("   set Brake Mode to Brake");
      //  m_controller.SetNeutralMode(NeutralMode::Brake);
    }
    else
    {
      //  Advisory::pinfo("   set Brake Mode to Coast");
      //  m_controller.SetNeutralMode(NeutralMode::Coast);
    }
}

/******************************************************************************
 *
 * Configure the current limits for the motor
 * 
 * @param   amps        The desired current limit.
 * 
 *   Current Limits are set to not burn up the motor
 *   From Rev document 2/2024
 *   https://www.revrobotics.com/neo-550-brushless-motor-locked-rotor-testing/
 *   Limit            Motor destruction
 *   20 A             N/A no failure before 227 seconds
 *   40 A             Fail after ~27 seconds
 *   60 A             Fail after ~5.5 seconds
 *   80 A             Fail after ~2.0 seconds
 ******************************************************************************/
void  Motor_SparkMax::setCurrentLimit(unsigned int amps)
{
    m_controller.SetSmartCurrentLimit(amps);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::doUpdate(void)
{
//    phoenix::motorcontrol::TalonFXSensorCollection & sensor_data = m_controller.GetSensorCollection();
//    m_forward_limit_pressed = (sensor_data.IsFwdLimitSwitchClosed() > 0);
//    m_reverse_limit_pressed = (sensor_data.IsRevLimitSwitchClosed() > 0);
}

/******************************************************************************
 *
 ******************************************************************************/
Motor::ControlModeType Motor_SparkMax::getControlMode()
{
    return m_control_mode;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setFeedbackDevice(FeedbackDeviceType sensor_type, bool wrap)
{/*
    switch(sensor_type)
    {
        case INTEGRATED_SENSOR:
        {
            m_controller.ConfigSelectedFeedbackSensor(FeedbackDevice::IntegratedSensor, 0, CAN_TIMEOUT);
        } break;

        default:
        {
            Advisory::pwarning("    unable to set sensor to specified type -- TalonFx only support INTEGRATED_SENSOR");
        } break;
    }*/
}

/******************************************************************************
 *
 * This may be different then other motors.
 *
 * If this method is not called, the default values will be 1.0 and -1.0.
 *
 * @param fwd_nom   the maximum command to which the output will be set,
 *                  this will normally be positive -- between 0.0 and 1.0
 *
 * @param rev_nom   the minimum command to which the output will be set
 *                  this will normally be negative -- between -1.0 and 0.0
 *
 ******************************************************************************/
void Motor_SparkMax::setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak)
{
//    m_controller.ConfigPeakOutputForward(fwd_peak, CAN_TIMEOUT);
//    m_controller.ConfigPeakOutputReverse(rev_peak, CAN_TIMEOUT);
//    m_controller.ConfigNominalOutputForward(fwd_nom, CAN_TIMEOUT);
//    m_controller.ConfigNominalOutputReverse(rev_nom, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_SparkMax::isUpperLimitPressed(void)
{
    return m_forward_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_SparkMax::isLowerLimitPressed(void)
{
    return m_reverse_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
int Motor_SparkMax::getDeviceID()
{
    return m_device_id;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setAsFollower(Motor_SparkMax& leader)
{
//    m_control_mode = ControlModeType::FOLLOWER;

 //   Advisory::pwarning("Not Implemented -- Motor_SparkMax::setAsFollower");
//    m_controller.Follow(&leader);
//    m_controller.Set(ControlMode::Follower, leader);
}

/******************************************************************************
 *
 ******************************************************************************/
/*ctre::phoenix::motorcontrol::can::TalonFX& Motor_SparkMax::getController(void)
{
    return m_controller;
}*/

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) the spark will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 	
 * 
 ******************************************************************************/
void Motor_SparkMax::setUpperLimitSwitch(bool normally_open, bool enabled, bool zero_position)
{
    Advisory::pinfo("        SparkMaxCan::setUpperLimitSwitch(no=%d,en=%d,zero=%d)", normally_open, enabled, zero_position);
    rev::SparkLimitSwitch::Type polarity;
    // limit switches is that the can't have the polarity switched through API calls

    if (nullptr != m_forward_limit)
    {
        delete m_forward_limit;
    }
    if (normally_open == true)
    {
        polarity = rev::SparkLimitSwitch::Type::kNormallyOpen;
    }
    else
    {
        polarity = rev::SparkLimitSwitch::Type::kNormallyClosed;
    }
    m_forward_limit = new rev::SparkLimitSwitch(m_controller.GetForwardLimitSwitch(polarity));
    m_forward_limit->EnableLimitSwitch(enabled);
}

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) the spark will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 							the limit switch
 ******************************************************************************/
void Motor_SparkMax::setLowerLimitSwitch(bool normally_open, bool enabled, bool zero_position)
{
    /*
    Advisory::pinfo("        Motor_SparkMax::setLowerLimitSwitch(%d,%d,%d,%f)", port, normally_open,reset_position, position_value);
    if (port >= 0)
    {
        m_controller.ConfigReverseLimitSwitchSource(
                LimitSwitchSource_FeedbackConnector,
                normally_open?LimitSwitchNormal_NormallyOpen:LimitSwitchNormal_NormallyClosed,
                CAN_TIMEOUT);

        m_controller.ConfigClearPositionOnLimitR(reset_position);
    }
    else
    {
        m_controller.ConfigReverseLimitSwitchSource(
                LimitSwitchSource_Deactivated,
                LimitSwitchNormal_Disabled,
                CAN_TIMEOUT);
    }*/
}

void Motor_SparkMax::burnFlash(void)
{
    m_controller.BurnFlash();
 
}


/******************************************************************************
 *
 * NOTICE: This method takes ownership of the PID, this class will delete the
 *         PID when it is no longer needed.
 *
 ******************************************************************************/
void Motor_SparkMax::setPid(BasicPid* pid)
{
    if (pid != nullptr)
    {
    m_pidController.SetP(pid->getKp());
    m_pidController.SetI(pid->getKi());
    m_pidController.SetD(pid->getKd());
    m_pidController.SetFF(pid->getKf());
    m_pidController.SetIZone(pid->getKz());
/*      
        setClosedLoopOutputLimits(pid->getLowerCommandLimit(), pid->getUpperCommandLimit(), 1.0, -1.0); */
        delete pid;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setKp(double p)
{
    //m_controller.Config_kP(0, p, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setKi(double i)
{
   // m_controller.Config_kI(0, i, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setKd(double d)
{
   // m_controller.Config_kD(0, d, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setKf(double f)
{
   // m_controller.Config_kF(0, f, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_SparkMax::setKz(double z)
{
  //  m_controller.Config_IntegralZone(0, (int)z, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getKp(void)
{
    return 0.0;//m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_P, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getKi(void)
{
    return 0.0;// m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_I, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getKd(void)
{
    return 0.0;// m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_D, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getKf(void)
{
    return 0.0;// m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_F, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_SparkMax::getKz(void)
{
    return 0.0;// m_controller.ConfigGetParameter(ctre::phoenix::ParamEnum::eProfileParamSlot_IZone, 0, CAN_TIMEOUT);
}
