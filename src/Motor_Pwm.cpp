/*******************************************************************************
 *
 * File: Motor_Pwm.cpp
 * 
 * Written by:
 *  FRC Team 324, Chips
 * 	Clear Creek Independent School District FIRST Robotics
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Motor_Pwm.h"

#include "gsutilities/Advisory.h"
#include "rfutilities/RobotUtil.h"

//using namespace gsu;

/******************************************************************************
 *
 * Create an instance of a motor controller for the provided PWM Speed Controller
 *
 * WARNING: This class will take ownership of provided controller -- this class
 *          will delete the controller when finished using it if it is not
 *          a nullptr.
 *
 ******************************************************************************/
Motor_Pwm::Motor_Pwm(frc::PWMMotorController * controller)
    : Motor()
{
    m_controller = controller;
    if (m_controller == nullptr)
    {
        Advisory::pinfo("Motor_Pwm cannot be created with a null controller");
    }

    m_analog_input = nullptr;
    m_encoder_input = nullptr;
    m_forward_limit_switch = nullptr;
    m_reverse_limit_switch = nullptr;
    m_leader = nullptr;
    m_pid = nullptr;

    m_control_mode = ControlModeType::PERCENT;
    m_sensor_type = FeedbackDeviceType::NONE;

    m_motor_invert_factor = 1.0;

    m_target_percent = 0.0;
    m_command_percent = 0.0;

    m_target_position = 0.0;
    m_actual_position = 0.0;
    m_actual_position_raw = 0.0;

    m_target_velocity = 0.0;
    m_actual_velocity = 0.0;
    m_actual_velocity_raw = 0.0;

    m_max_command_delta = 0.25;

    m_max_command_percent = 1.0;
    m_min_command_percent = -1.0;

    m_forward_limit_pressed = false;
    m_reverse_limit_pressed = false;
}

/******************************************************************************
 *
 ******************************************************************************/
Motor_Pwm::~Motor_Pwm(void)
{
    if (m_controller != nullptr)
    {
        m_controller->Set(0.0); // make sure the motor is commanded to stop

        delete m_controller;
        m_controller = nullptr;
    }

    // TODO: delete any pointers that this object takes ownership of,
    //       that is at least the sensors and the PID
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setPercent(double percent)
{
    m_control_mode = ControlModeType::PERCENT;
    m_target_percent = percent;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getPercent()
{
    return m_command_percent;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setPosition(double position)
{
    if (((m_analog_input != nullptr) || (m_encoder_input != nullptr)) &&
        (m_pid != nullptr))
    {
        m_control_mode = ControlModeType::POSITION;
        m_target_position = position;
    }
    else
    {
        m_control_mode = ControlModeType::UNKNOWN_TYPE;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getPosition()
{
    return m_actual_position;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getRawPosition(void)
{
    return m_actual_position_raw;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setVelocity(double velocity)
{
    if ((m_encoder_input != nullptr) && (m_pid != nullptr))
    {
        m_control_mode = ControlModeType::VELOCITY;
        m_target_velocity = velocity;
    }
    else
    {
        m_control_mode = ControlModeType::UNKNOWN_TYPE;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getVelocity(void)
{
    return m_actual_velocity;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getRawVelocity(void)
{
    return m_actual_velocity_raw;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::doUpdate(void)
{
    double motor_command = 0.0;

    // Get inputs from sensors
    if (m_forward_limit_switch != nullptr)
    {
        m_forward_limit_pressed = m_forward_limit_switch->isPressed();
    }

    if (m_reverse_limit_switch != nullptr)
    {
        m_reverse_limit_pressed = m_reverse_limit_switch->isPressed();
    }

    if (m_analog_input != nullptr)
    {
        m_actual_position = m_analog_input->getPosition();
        m_actual_position_raw = m_analog_input->getRaw();
    }

    if (m_encoder_input != nullptr)
    {
        m_actual_velocity = m_encoder_input->GetRate();
        m_actual_velocity_raw = m_encoder_input->GetRaw();

        if (m_analog_input == nullptr)
        {
            m_actual_position = m_encoder_input->GetDistance();
            m_actual_position_raw = m_encoder_input->GetRaw();
        }
    }

    // Process Values
    switch(m_control_mode)
    {
        case PERCENT:
        {
            motor_command = m_target_percent;

            // Ramps the motor command to reduce jerk
            RobotUtil::limit(m_command_percent - m_max_command_delta,
                m_command_percent + m_max_command_delta, motor_command);
        } break;

        case POSITION:
        {
            motor_command = m_pid->calculateCommand(m_target_position, m_actual_position);
        } break;

        case VELOCITY:
        {
            motor_command = m_pid->calculateCommand(m_target_velocity, m_actual_velocity);
        } break;

        case FOLLOWER:
        {
            motor_command = m_leader->getPercent();
        } break;

        default:
        {
            motor_command = 0.0;
        } break;
    }

    if (m_forward_limit_pressed && m_reverse_limit_pressed)
    {
        motor_command = 0;
    }
    else if (m_forward_limit_pressed)
    {
        RobotUtil::limit(m_min_command_percent, 0.0, motor_command);
    }
    else if (m_reverse_limit_pressed)
    {
        RobotUtil::limit(0.0, m_max_command_percent, motor_command);
    }
    else
    {
        RobotUtil::limit(m_min_command_percent, m_max_command_percent, motor_command);
    }

    // Set the Outputs
    m_command_percent = motor_command;
    m_controller->Set(m_command_percent * m_motor_invert_factor);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setMotorInvert(bool invert)
{
    if (invert)
    {
        m_motor_invert_factor = -1.0;
    }
    else
    {
        m_motor_invert_factor = 1.0;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_Pwm::getMotorInvert(void)
{
    return (m_motor_invert_factor < 0.0);
}

/******************************************************************************
 *
 ******************************************************************************/
Motor::ControlModeType Motor_Pwm::getControlMode()
{
    return m_control_mode;
}

/******************************************************************************
 *
 * This may be different then other motors.
 *
 * If this method is not called, the default values will be 1.0 and -1.0.
 *
 * @param fwd_nom   the maximum command to which the output will be set,
 *                  this will normally be positive -- between 0.0 and 1.0
 *
 * @param rev_nom   the minimum command to which the output will be set
 *                  this will normally be negative -- between -1.0 and 0.0
 *
 ******************************************************************************/
void Motor_Pwm::setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak)
{
    m_max_command_percent = fwd_nom;
    m_min_command_percent = rev_nom;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position, double position_value)
{
    if (port >= 0)
    {
        LimitSwitch* sw = new LimitSwitch(port);
        sw->setMode(normally_open?LimitSwitch::InputMode::INPUT_NORMALLY_OPEN:LimitSwitch::InputMode::INPUT_NORMALLY_CLOSED);
        setUpperLimitSwitch(sw);
    }
    else
    {
        setUpperLimitSwitch(nullptr);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setLowerLimitSwitch(int8_t port, bool normally_open, bool reset_position, double position_value)
{
    if (port >= 0)
    {
        LimitSwitch* sw = new LimitSwitch(port);
        sw->setMode(normally_open?LimitSwitch::InputMode::INPUT_NORMALLY_OPEN:LimitSwitch::InputMode::INPUT_NORMALLY_CLOSED);
        setLowerLimitSwitch(sw);
    }
    else
    {
        setLowerLimitSwitch(nullptr);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_Pwm::isUpperLimitPressed(void)
{
    return m_forward_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_Pwm::isLowerLimitPressed(void)
{
    return m_reverse_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
int Motor_Pwm::getDeviceID()
{
//    return m_controller->GetDeviceIdOrPortOrSomething();
    return -1;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setAsFollower(Motor* leader)
{
    m_leader = leader;
    if (m_leader != nullptr)
    {
        m_control_mode = ControlModeType::FOLLOWER;
    }
    else
    {
        m_control_mode = ControlModeType::UNKNOWN_TYPE;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setUpperLimitSwitch(LimitSwitch* forward_limit_switch)
{
    m_forward_limit_switch = forward_limit_switch;
    if(m_forward_limit_switch == nullptr)
    {
        m_forward_limit_pressed = false;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setLowerLimitSwitch(LimitSwitch* reverse_limit_switch)
{
    m_reverse_limit_switch = reverse_limit_switch;
    if(m_reverse_limit_switch == nullptr)
    {
        m_reverse_limit_pressed = false;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setAnalogInput(ScaledAnalogInput* analog_input)
{
    m_analog_input = analog_input;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setEncoder(frc::Encoder* encoder)
{
    m_encoder_input = encoder;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setPid(BasicPid* pid)
{
    m_pid = pid;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setKp(double p)
{
    if (m_pid != nullptr)
    {
        m_pid->setKp(p);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setKi(double i)
{
    if (m_pid != nullptr)
    {
        m_pid->setKi(i);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setKd(double d)
{
    if (m_pid != nullptr)
    {
        m_pid->setKd(d);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setKf(double f)
{
    if (m_pid != nullptr)
    {
        m_pid->setKf(f);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_Pwm::setKz(double z)
{
    if (m_pid != nullptr)
    {
        m_pid->setKz(z);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getKp(void)
{
    if (m_pid != nullptr)
    {
        m_pid->getKp();
    }
    return 0.0;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getKi(void)
{
    if (m_pid != nullptr)
    {
        m_pid->getKi();
    }
    return 0.0;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getKd(void)
{
    if (m_pid != nullptr)
    {
        m_pid->getKd();
    }
    return 0.0;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getKf(void)
{
    if (m_pid != nullptr)
    {
        m_pid->getKf();
    }
    return 0.0;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_Pwm::getKz(void)
{
    if (m_pid != nullptr)
    {
        m_pid->getKz();
    }
    return 0.0;
}
