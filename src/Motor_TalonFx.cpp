/*******************************************************************************
 *
 * File: Motor_TalonFx.cpp
 * 
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Motor_TalonFx.h"

#include "gsutilities/Advisory.h"
#include "rfutilities/RobotUtil.h"

#include "ctre/phoenix/motorcontrol/SensorCollection.h"

//using namespace gsu;
#define CAN_TIMEOUT 0
//#define CAN_TIMEOUT kTimeout

const double Motor_TalonFx::m_volt_max = 12.0;

/******************************************************************************
 *
 * Create an instance of a motor controller for the provided PWM Speed Controller
 *
 * WARNING: This class will take ownership of provided controller -- this class
 *          will delete the controller when finished using it if it is not
 *          a nullptr.
 *
 ******************************************************************************/
Motor_TalonFx::Motor_TalonFx(int device_id, std::string canbus, int status_period_general_ms, int status_period_feedback_ms)
    : Motor()
    , m_controller(device_id, canbus)
{
    Advisory::pinfo("Motor_TalonFx::Motor_TalonFx");
/*
    if (control_period_ms > 0)
    {
        m_controller.SetControlFramePeriod(ControlFrameEnhanced::Control_1_General, control_period_ms);
    }
    else
    {
        m_controller.SetControlFramePeriod(ControlFrameEnhanced::Control_1_General, 10);
    }
*/
    if (status_period_general_ms > 0)
    {
        m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_1_General, status_period_general_ms, CAN_TIMEOUT);
    }
    else
    {
        m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_1_General, 10, CAN_TIMEOUT);
    }

    if (status_period_feedback_ms > 0)
    {
        m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_2_Feedback0, status_period_feedback_ms, CAN_TIMEOUT);
    }
    else
    {
        m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_2_Feedback0, 20, CAN_TIMEOUT);
    }
    
    m_forward_limit_pressed = false;
    m_reverse_limit_pressed = false;

    m_output_scale = 1.0;
    m_output_offset = 0.0;
    m_velocity_timescale = 10.0; // 1 s = 10 * (100ms) where 100ms is TalonFx unitary time

    setFeedbackDevice(INTEGRATED_SENSOR, true);
    m_controller.SetSelectedSensorPosition(0);   // set the Integrated Encoder to a raw position of 0
}

/******************************************************************************
 *
 ******************************************************************************/
Motor_TalonFx::~Motor_TalonFx(void)
{
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setPercent(double percent)
{
    m_control_mode = ControlModeType::PERCENT;
    m_controller.Set(ControlMode::PercentOutput, percent);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getPercent()
{
    return m_controller.GetMotorOutputPercent();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setPosition(double position)
{
    m_control_mode = ControlModeType::POSITION;
    float val = (position - m_output_offset)/m_output_scale;
    m_controller.Set(ControlMode::Position, val);

    //
    // should make sure sensors were set
//    if (((m_analog_input != nullptr) || (m_encoder_input != nullptr)) &&
//        (m_pid != nullptr))
//    {
//        m_control_mode = ControlModeType::POSITION;
//        m_target_position = position;
//    }
//    else
//    {
//        m_control_mode = ControlModeType::UNKNOWN_TYPE;
//    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getPosition()
{
    return ( ((getRawPosition() * m_output_scale) + m_output_offset) );
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getRawPosition(void)
{
    return m_controller.GetSelectedSensorPosition(0);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::resetPosition(double value)
{
    m_controller.SetSelectedSensorPosition(value);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setVelocity(double velocity)
{
    m_control_mode = ControlModeType::VELOCITY;
    m_controller.Set(ControlMode::Velocity, (velocity / m_output_scale / m_velocity_timescale) );

    //
    // should make sure sensors were set
//    if ((m_encoder_input != nullptr) && (m_pid != nullptr))
//    {
//        m_control_mode = ControlModeType::VELOCITY;
//        m_target_velocity = velocity;
//    }
//    else
//    {
//        m_control_mode = ControlModeType::UNKNOWN_TYPE;
//    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getVelocity(void)
{
    return (getRawVelocity() * m_output_scale * m_velocity_timescale);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getRawVelocity(void)
{
    return m_controller.GetSelectedSensorVelocity(0);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setMotorInvert(bool invert)
{
    m_controller.SetInverted(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_TalonFx::getMotorInvert(void)
{
    return m_controller.GetInverted();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setSensorInvert(bool invert)
{
    m_controller.SetSensorPhase(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
// bool Motor_TalonFx::getSensorInvert(void)
// {
//     return m_controller.GetSensorPhase();
// }

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setSensorScale(double scale, double offset)
{
    m_output_scale = scale;
    m_output_offset = offset;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setBrakeMode(bool brake)
{
    if (brake)
    {
        Advisory::pinfo("   set Brake Mode to Brake");
        m_controller.SetNeutralMode(NeutralMode::Brake);
    }
    else
    {
        Advisory::pinfo("   set Brake Mode to Coast");
        m_controller.SetNeutralMode(NeutralMode::Coast);
    }
}

/******************************************************************************
 *
 * Configure the supply current limits for the motor
 * 
 * @param   enabled     If true current limiting will be enabled
 * 
 * @param   amps        The limit of the current, if the peak is exceeded, the
 *                      current will hold at this value, default 10.0 amps.
 * 
 * @param   peak        Current must exceed this peak value before limiting 
 *                      occurs. If this value is less than amps, then amps
 *                      is used as the threshold, default 12.5 amps.
 * 
 * @param   duration    The number of seconds for which the current must 
 *                      exceed the peak value before limiting occures,
 *                      default = 0.5
 * 
 ******************************************************************************/
void  Motor_TalonFx::setCurrentLimit(bool enabled, double amps, double peak, double duration )
{
    Advisory::pinfo("   Motor_TalonFx::setCurrentLimit enabled=%d, amps=%f, peak=%f, duration=%f", enabled, amps, peak, duration);
    ctre::phoenix::motorcontrol::SupplyCurrentLimitConfiguration sclc;
    m_controller.ConfigGetSupplyCurrentLimit(sclc);
    sclc.enable = enabled;
    sclc.currentLimit = amps;
	sclc.triggerThresholdCurrent = peak;
	sclc.triggerThresholdTime = duration;
    m_controller.ConfigSupplyCurrentLimit(sclc);
}

/******************************************************************************
 *
 * Configure the stator current limits for the motor
 * 
 * @param   enabled     If true current limiting will be enabled
 * 
 * @param   amps        The limit of the current, if the peak is exceeded, the
 *                      current will hold at this value, default 10.0 amps.
 * 
 * @param   peak        Current must exceed this peak value before limiting 
 *                      occurs. If this value is less than amps, then amps
 *                      is used as the threshold, default 12.5 amps.
 * 
 * @param   duration    The number of seconds for which the current must 
 *                      exceed the peak value before limiting occures,
 *                      default = 0.5
 * 
 ******************************************************************************/
void  Motor_TalonFx::setStatorCurrentLimit(bool enabled, double amps, double peak, double duration )
{
    Advisory::pinfo("   Motor_TalonFx::setStatorCurrentLimit enabled=%d, amps=%f, peak=%f, duration=%f", enabled, amps, peak, duration);
    ctre::phoenix::motorcontrol::StatorCurrentLimitConfiguration sclc;
    m_controller.ConfigGetStatorCurrentLimit(sclc);
    sclc.enable = enabled;
    sclc.currentLimit = amps;
	sclc.triggerThresholdCurrent = peak;
	sclc.triggerThresholdTime = duration;
    m_controller.ConfigStatorCurrentLimit(sclc);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::doUpdate(void)
{
    phoenix::motorcontrol::TalonFXSensorCollection & sensor_data = m_controller.GetSensorCollection();
    m_forward_limit_pressed = (sensor_data.IsFwdLimitSwitchClosed() > 0);
    m_reverse_limit_pressed = (sensor_data.IsRevLimitSwitchClosed() > 0);

}

/******************************************************************************
 *
 ******************************************************************************/
Motor::ControlModeType Motor_TalonFx::getControlMode()
{
    return m_control_mode;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setFeedbackDevice(FeedbackDeviceType sensor_type, bool wrap)
{
    switch(sensor_type)
    {
        case INTEGRATED_SENSOR:
        {
            m_controller.ConfigSelectedFeedbackSensor(FeedbackDevice::IntegratedSensor, 0, CAN_TIMEOUT);
        } break;

        default:
        {
            Advisory::pwarning("    unable to set sensor to specified type -- TalonFx only support INTEGRATED_SENSOR");
        } break;
    }
}

/******************************************************************************
 *
 * This may be different then other motors.
 *
 * If this method is not called, the default values will be 1.0 and -1.0.
 *
 * @param fwd_nom   the maximum command to which the output will be set,
 *                  this will normally be positive -- between 0.0 and 1.0
 *
 * @param rev_nom   the minimum command to which the output will be set
 *                  this will normally be negative -- between -1.0 and 0.0
 *
 ******************************************************************************/
void Motor_TalonFx::setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak)
{
    m_controller.ConfigPeakOutputForward(fwd_peak, CAN_TIMEOUT);
    m_controller.ConfigPeakOutputReverse(rev_peak, CAN_TIMEOUT);
    m_controller.ConfigNominalOutputForward(fwd_nom, CAN_TIMEOUT);
    m_controller.ConfigNominalOutputReverse(rev_nom, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_TalonFx::isUpperLimitPressed(void)
{
    return m_forward_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_TalonFx::isLowerLimitPressed(void)
{
    return m_reverse_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
int Motor_TalonFx::getDeviceID()
{
    return m_controller.GetDeviceID();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setAsFollower(Motor_TalonFx& leader)
{
    m_control_mode = ControlModeType::FOLLOWER;

    Advisory::pwarning("Not Implemented -- Motor_TalonFx::setAsFollower");
//    m_controller.Follow(&leader);
//    m_controller.Set(ControlMode::Follower, leader);
}

/******************************************************************************
 *
 ******************************************************************************/
ctre::phoenix::motorcontrol::can::BaseMotorController& Motor_TalonFx::getController(void)
{
    return m_controller;
}

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) it will the talon will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 							the limit switch
 * 							NOTE: this could be updated to also allow for
 * 							the Talon to use external limit switches.
 *
 ******************************************************************************/
void Motor_TalonFx::setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position, double position_value)
{
    Advisory::pinfo("        Motor_TalonFx::setUpperLimitSwitch(%d,%d,%d,%f)", port, normally_open,reset_position, position_value);
    if (port >= 0)
    {
        m_controller.ConfigForwardLimitSwitchSource(
                LimitSwitchSource_FeedbackConnector,
                normally_open?LimitSwitchNormal_NormallyOpen:LimitSwitchNormal_NormallyClosed,
                CAN_TIMEOUT);

        m_controller.ConfigClearPositionOnLimitF(reset_position);
    }
    else
    {
        m_controller.ConfigForwardLimitSwitchSource(
                LimitSwitchSource_Deactivated,
                LimitSwitchNormal_Disabled,
                CAN_TIMEOUT);
    }
}

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) it will the talon will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 							the limit switch
 * 							NOTE: this could be updated to also allow for
 * 							the Talon to use external limit switches.
 *
 ******************************************************************************/
void Motor_TalonFx::setLowerLimitSwitch  (int8_t port, bool normally_open, bool reset_position, double position_value)
{
    Advisory::pinfo("        Motor_TalonFx::setLowerLimitSwitch(%d,%d,%d,%f)", port, normally_open,reset_position, position_value);
    if (port >= 0)
    {
        m_controller.ConfigReverseLimitSwitchSource(
                LimitSwitchSource_FeedbackConnector,
                normally_open?LimitSwitchNormal_NormallyOpen:LimitSwitchNormal_NormallyClosed,
                CAN_TIMEOUT);

        m_controller.ConfigClearPositionOnLimitR(reset_position);
    }
    else
    {
        m_controller.ConfigReverseLimitSwitchSource(
                LimitSwitchSource_Deactivated,
                LimitSwitchNormal_Disabled,
                CAN_TIMEOUT);
    }
}

/******************************************************************************
 *
 * NOTICE: This method takes ownership of the PID, this class will delete the
 *         PID when it is no longer needed.
 *
 ******************************************************************************/
void Motor_TalonFx::setPid(BasicPid* pid)
{
    if (pid != nullptr)
    {
        setKf(pid->getKf());
        setKp(pid->getKp());
        setKi(pid->getKi());
        setKd(pid->getKd());
        setKz(pid->getKz());

        setClosedLoopOutputLimits(pid->getLowerCommandLimit(), pid->getUpperCommandLimit(), 1.0, -1.0);
        delete pid;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setKp(double p)
{
    m_controller.Config_kP(0, p, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setKi(double i)
{
    m_controller.Config_kI(0, i, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setKd(double d)
{
    m_controller.Config_kD(0, d, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setKf(double f)
{
    m_controller.Config_kF(0, f, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFx::setKz(double z)
{
    m_controller.Config_IntegralZone(0, (int)z, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getKp(void)
{
    return m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_P, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getKi(void)
{
    return  m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_I, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getKd(void)
{
    return  m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_D, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getKf(void)
{
    return  m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_F, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFx::getKz(void)
{
    return  m_controller.ConfigGetParameter(ctre::phoenix::ParamEnum::eProfileParamSlot_IZone, 0, CAN_TIMEOUT);
}
