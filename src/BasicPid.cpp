/*******************************************************************************
 *
 * File: BasicPid.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/BasicPid.h"

#include "rfutilities/RobotUtil.h"

#include <math.h>

/******************************************************************************
 *
 * Create a default instance of a Basic PID control
 *
 ******************************************************************************/
BasicPid::BasicPid()
{
    m_kf = 1.0;
    m_kp = 0.0;
    m_ki = 0.0;
    m_kd = 0.0;
    m_kz = 100000000.0;

    m_error = 0.0;
    m_error_prev = 0.0;
    m_error_delta = 0.0;

    m_max_command = 1.0;
    m_min_command = -1.0;

    m_is_angle = false;
}

/******************************************************************************
 *
 * Clean up any resources used by this instance
 *
 ******************************************************************************/
BasicPid::~BasicPid()
{

}

/******************************************************************************
 *
 *  Simple Getters/Setters
 *
 ******************************************************************************/
void BasicPid::setKp(double kp) { m_kp = kp; }
void BasicPid::setKi(double ki) { m_ki = ki; }
void BasicPid::setKd(double kd) { m_kd = kd; }
void BasicPid::setKf(double kf) { m_kf = kf; }
void BasicPid::setKz(double kz) { m_kz = kz; }

double BasicPid::getKp() { return m_kp;}
double BasicPid::getKi() { return m_ki;}
double BasicPid::getKd() { return m_kd;}
double BasicPid::getKf() { return m_kf;}
double BasicPid::getKz() { return m_kz;}

double BasicPid::getErrorKp() { return m_error; }
double BasicPid::getErrorKi() { return m_error_prev; }
double BasicPid::getErrorKd() { return m_error_delta; }

double BasicPid::getLowerCommandLimit(void) { return m_min_command; }
double BasicPid::getUpperCommandLimit(void) { return m_max_command; }

/******************************************************************************
 *
 * 	Set all of the gains at the same time
 *
 * @param kf    The feed forward constant. For position loops this should be
 *              set to 0.0. For velocity control loops this should be set such
 *              that  percent of target results in about percent output velocity
 *              for most of the commandible range.
 *
 * @param kp    The proportianal error gain. Leave this zero until the
 *              feed forward has been set. Then start small and doulbe this
 *              until you start to get oscilations about the target. Then reduce
 *              reduce the value until the oscilations stop. In many cases
 *              you will get acceptible results once kp is set, the kd and ki
 *              terms can often be left at 0.0.
 *
 * @param kd    The derivative error gain. If you want to get to the target
 *              faster, After the initial setting of kp, kd can be increased
 *              probably getting to something close to kp. As kp is increased,
 *              try increasing kd more, the point of oscilation should increase
 *              some.
 *
 * @param ki    The integral error gain. After tuning the kf, kp, and kd values,
 *              increasing ki to a value that is normally between 1 and 10 % of kd
 *              can help get closer to the actual target value.
 *
 * @param kz    The integration threashold or integratino zone indicates how much
 *              error can be adjusted for using the integral error, if the current
 *              error is outside of the integration zone, the integration error will
 *              decay, if the error is in the zone, the integration error will
 *              accumulate.
 *
 * @see setKp(), setKi(), setKd(), setKf(), setKz()
 *
 ******************************************************************************/
void BasicPid::setGains(double kf, double kp, double ki, double kd, double kz)
{
    m_kf = kf;
    m_kp = kp;
    m_ki = ki;
    m_kd = kd;
    m_kz = kz;
}

/******************************************************************************
 *
 * Set the maximum command limits. If this method is not called the defaults
 * will be -1.0 to 1.0. Setting these to something less will limit the maximum
 * velocity, how quickly positions are reached and how much force is available
 * to hold positions.
 *
 * @param   min_command the minimum value that will be returned as a command
 * @param   max_command the maximum value that will be returned as a command
 *
 ******************************************************************************/
void BasicPid::setCommandLimits(double min_command, double max_command)
{
    m_min_command = min_command;
    m_max_command = max_command;
}

/******************************************************************************
 *
 * Set this PID control to handle angles that wrap from -180 to 180. If true
 * a target of -179 and an actual of 179 will result in an error of 2 instead
 * of an error -358.
 *
 * @param is_angle  set to true if angls should wrap.
 *
 ******************************************************************************/
void BasicPid::setIsAngle(bool is_angle)
{
	m_is_angle = is_angle;
}

/******************************************************************************
 *
 * Using the current target and the current measured value, calculate the
 * motor command
 *
 * @return the percentage that the motor should be commanded.
 *
 ******************************************************************************/
double BasicPid::calculateCommand(double target, double actual)
{
    double err = (target - actual); // determine error

    if (m_is_angle == true)   // if an angle, the error between 170 and -170 is -20, not 340
    {
        int count = 0;
        while (std::abs(err) > 180.0)
        {
            err -= std::copysign(1.0, err) * 360.0;
            count++;
            if (count > 100)  // this would be an error of 18000 deg, major malfunction or lots of loops, prevents (unlikely) infinite loop
            {
                err = 0;
                break;
            }
        }
    }

    if (std::fabs(err) < m_kz) // accumulate error
	{
        m_error_prev += err; // integrate m_error when small (integrate threshold (m_kz) defines small)
	}
	else
	{
        m_error_prev *= 0.0; // decay error when large
	}

    m_error_delta = err - m_error;
    m_error = err;

    return RobotUtil::limit(m_min_command, m_max_command,
            (m_kf * target) + (m_kp * m_error) + (m_ki * m_error_prev) + (m_kd * m_error_delta));
}
