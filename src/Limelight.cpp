/*******************************************************************************
 *
 * File: Limelight.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/

#include "rfHardware/Limelight.h"


/******************************************************************************
 *
 ******************************************************************************/
Limelight::Limelight(void)
{
}


/******************************************************************************
 *
 ******************************************************************************/
Limelight::~Limelight()
{

}

/******************************************************************************
 *
 ******************************************************************************/
void Limelight::getData(double &target_visible,
                        double &targetOffsetAngle_Horizontal, 
                        double &targetOffsetAngle_Vertical,
                        double &targetArea,
                        double &targetSkew)
{
   target_visible = table->GetNumber("tv",0.0);
   targetOffsetAngle_Horizontal = table->GetNumber("tx",0.0);
   targetOffsetAngle_Vertical = table->GetNumber("ty",0.0);
   targetArea = table->GetNumber("ta",0.0);
   targetSkew = table->GetNumber("ts",0.0);
}

void Limelight::setPipeline(int pipeline)
{
    table->PutNumber("pipeline", pipeline);
}
