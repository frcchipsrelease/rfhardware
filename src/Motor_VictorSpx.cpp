/*******************************************************************************
 *
 * File: Motor_VictorSpx.cpp
 * 
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Motor_VictorSpx.h"

#include "gsutilities/Advisory.h"
#include "rfutilities/RobotUtil.h"

#include "ctre/phoenix/motorcontrol/SensorCollection.h"

#define CAN_TIMEOUT 0
//#define CAN_TIMEOUT kTimeout

/******************************************************************************
 *
 * Create an instance of a motor controller for the provided PWM Speed Controller
 *
 * WARNING: This class will take ownership of provided controller -- this class
 *          will delete the controller when finished using it if it is not
 *          a nullptr.
 *
 ******************************************************************************/
Motor_VictorSpx::Motor_VictorSpx(int device_id, int control_period_ms)
    : Motor()
    , m_controller(device_id)
{
    if (control_period_ms > 0)
    {
//        m_controller.setControlPeriod(control_period_ms);
    }

    m_forward_limit_pressed = false;
    m_reverse_limit_pressed = false;

    m_output_scale = 1.0;
    m_output_offset = 0.0;
}

/******************************************************************************
 *
 ******************************************************************************/
Motor_VictorSpx::~Motor_VictorSpx(void)
{
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setPercent(double percent)
{
    m_control_mode = ControlModeType::PERCENT;
    m_controller.Set(ControlMode::PercentOutput, percent);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getPercent()
{
    return m_controller.GetMotorOutputPercent();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setPosition(double position)
{
    Advisory::pwarning("Motor_VictorSpx::setPosition not implemented");
//    m_control_mode = ControlModeType::POSITION;
//    m_controller.Set(ControlMode::Position, (position-m_output_offset)/m_output_scale);

    //
    // should make sure sensors were set
//    if (((m_analog_input != nullptr) || (m_encoder_input != nullptr)) &&
//        (m_pid != nullptr))
//    {
//        m_control_mode = ControlModeType::POSITION;
//        m_target_position = position;
//    }
//    else
//    {
//        m_control_mode = ControlModeType::UNKNOWN_TYPE;
//    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getPosition()
{
    Advisory::pwarning("Motor_VictorSpx::getPosition not implemented");
    return 0.0;
//    return ( ((getRawPosition() * m_output_scale) + m_output_offset) );
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getRawPosition(void)
{
    Advisory::pwarning("Motor_VictorSpx::getRawPosition not implemented");
    return 0.0;
//    return m_controller.GetSelectedSensorPosition(0);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setVelocity(double velocity)
{
    Advisory::pwarning("Motor_VictorSpx::setVelocity not implemented");
//    m_control_mode = ControlModeType::VELOCITY;
//    m_controller.Set(ControlMode::Velocity, (velocity / m_output_scale) * 0.1 ); // @TODO: fix hard coded value

    //
    // should make sure sensors were set
//    if ((m_encoder_input != nullptr) && (m_pid != nullptr))
//    {
//        m_control_mode = ControlModeType::VELOCITY;
//        m_target_velocity = velocity;
//    }
//    else
//    {
//        m_control_mode = ControlModeType::UNKNOWN_TYPE;
//    }
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getVelocity(void)
{
    Advisory::pwarning("Motor_VictorSpx::getVelocity not implemented");
    return 0.0;
//    return (getRawVelocity() * m_output_scale * 10.0);  // @TODO: fix hard coded value
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getRawVelocity(void)
{
    Advisory::pwarning("Motor_VictorSpx::getRawVelocity not implemented");
    return 0.0;
//    return m_controller.GetSelectedSensorVelocity(0);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setMotorInvert(bool invert)
{
    m_controller.SetInverted(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_VictorSpx::getMotorInvert(void)
{
    return m_controller.GetInverted();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setSensorInvert(bool invert)
{
    Advisory::pwarning("Motor_VictorSpx::setFeesetSensorInvertdbackDevice not implemented");
//    m_controller.SetSensorPhase(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
// bool Motor_VictorSpx::getSensorInvert(void)
// {
//     return m_controller.GetSensorPhase();
// }

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setSensorScale(double scale, double offset)
{
    m_output_scale = scale;
    m_output_offset = offset;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setBrakeMode(bool brake)
{
    if (brake)
    {
        m_controller.SetNeutralMode(NeutralMode::Brake);
    }
    else
    {
        m_controller.SetNeutralMode(NeutralMode::Coast);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::doUpdate(void)
{
//    phoenix::motorcontrol::SensorCollection& sensor_data = m_controller.GetSensorCollection();
//    m_forward_limit_pressed = (sensor_data.IsFwdLimitSwitchClosed() > 0);
//    m_reverse_limit_pressed = (sensor_data.IsRevLimitSwitchClosed() > 0);
}

/******************************************************************************
 *
 ******************************************************************************/
Motor::ControlModeType Motor_VictorSpx::getControlMode()
{
    return m_control_mode;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setFeedbackDevice(FeedbackDeviceType sensor_type, bool wrap)
{
    Advisory::pwarning("Motor_VictorSpx::setFeedbackDevice not implemented");
}

/******************************************************************************
 *
 * This may be different then other motors.
 *
 * If this method is not called, the default values will be 1.0 and -1.0.
 *
 * @param fwd_nom   the maximum command to which the output will be set,
 *                  this will normally be positive -- between 0.0 and 1.0
 *
 * @param rev_nom   the minimum command to which the output will be set
 *                  this will normally be negative -- between -1.0 and 0.0
 *
 ******************************************************************************/
void Motor_VictorSpx::setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak)
{
    Advisory::pwarning("Motor_VictorSpx::setClosedLoopOutputLimits not implemented");
//    m_controller.ConfigPeakOutputForward(fwd_peak, CAN_TIMEOUT);
//    m_controller.ConfigPeakOutputReverse(rev_peak, CAN_TIMEOUT);
//    m_controller.ConfigNominalOutputForward(fwd_nom, CAN_TIMEOUT);
//    m_controller.ConfigNominalOutputReverse(rev_nom, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_VictorSpx::isUpperLimitPressed(void)
{
    return m_forward_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_VictorSpx::isLowerLimitPressed(void)
{
    return m_reverse_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
int Motor_VictorSpx::getDeviceID()
{
    return m_controller.GetDeviceID();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setAsFollower(Motor_VictorSpx& leader)
{
    Advisory::pwarning("Motor_VictorSpx::setAsFollower not implemented");
//    m_control_mode = ControlModeType::FOLLOWER;
//    m_controller.Follow(&leader);
//    m_controller.Set(ControlMode::Follower, leader);
}

/******************************************************************************
 *
 ******************************************************************************/
ctre::phoenix::motorcontrol::can::VictorSPX& Motor_VictorSpx::getController(void)
{
    return m_controller;
}

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) it will the talon will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 							the limit switch
 * 							NOTE: this could be updated to also allow for
 * 							the Talon to use external limit switches.
 *
 ******************************************************************************/
void Motor_VictorSpx::setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position, double position_value)
{
    Advisory::pwarning("Motor_VictorSpx::setUpperLimitSwitch not implemented");
}

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) it will the talon will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 							the limit switch
 * 							NOTE: this could be updated to also allow for
 * 							the Talon to use external limit switches.
 *
 ******************************************************************************/
void Motor_VictorSpx::setLowerLimitSwitch  (int8_t port, bool normally_open, bool reset_position, double position_value)
{
    Advisory::pwarning("Motor_VictorSpx::setLowerLimitSwitch not implemented");
}

/******************************************************************************
 *
 * NOTICE: This method takes ownership of the PID, this class will delete the
 *         PID when it is no longer needed.
 *
 ******************************************************************************/
void Motor_VictorSpx::setPid(BasicPid* pid)
{
    Advisory::pwarning("Motor_VictorSpx::setPid not implemented");
    // if (pid != nullptr)
    // {
    //     m_controller.Config_kF(0, pid->getKf(), CAN_TIMEOUT);
    //     m_controller.Config_kP(0, pid->getKp(), CAN_TIMEOUT);
    //     m_controller.Config_kI(0, pid->getKi(), CAN_TIMEOUT);
    //     m_controller.Config_kD(0, pid->getKd(), CAN_TIMEOUT);
    //     m_controller.Config_IntegralZone(0, (int)pid->getKz(), CAN_TIMEOUT);

    //     m_controller.ConfigPeakOutputReverse(-1.0, CAN_TIMEOUT);
    //     m_controller.ConfigPeakOutputForward(1.0, CAN_TIMEOUT);

    //     m_controller.ConfigNominalOutputReverse(pid->getLowerCommandLimit(), CAN_TIMEOUT);
    //     m_controller.ConfigNominalOutputForward(pid->getUpperCommandLimit(), CAN_TIMEOUT);

         delete pid;
    // }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setKp(double p)
{
    Advisory::pwarning("Motor_VictorSpx::setKp not implemented");
//    m_controller.Config_kP(0, p, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setKi(double i)
{
    Advisory::pwarning("Motor_VictorSpx::setKi not implemented");
//    m_controller.Config_kI(0, i, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setKd(double d)
{
    Advisory::pwarning("Motor_VictorSpx::setKd not implemented");
//    m_controller.Config_kD(0, d, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setKf(double f)
{
    Advisory::pwarning("Motor_VictorSpx::setKf not implemented");
//    m_controller.Config_kF(0, f, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_VictorSpx::setKz(double z)
{
     Advisory::pwarning("Motor_VictorSpx::setKz not implemented");
//   m_controller.Config_IntegralZone(0, (int)z, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getKp(void)
{
    Advisory::pwarning("Motor_VictorSpx::getKp not implemented");
    return 0.0;
 //   return m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_P, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getKi(void)
{
    Advisory::pwarning("Motor_VictorSpx::getKi not implemented");
    return 0.0;
//    return  m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_I, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getKd(void)
{
    Advisory::pwarning("Motor_VictorSpx::getKd not implemented");
    return 0.0;
 //   return  m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_D, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getKf(void)
{
    Advisory::pwarning("Motor_VictorSpx::getKf not implemented");
    return 0.0;
//    return  m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_F, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_VictorSpx::getKz(void)
{
    Advisory::pwarning("Motor_VictorSpx::getKz not implemented");
    return 0.0;
//    return  m_controller.ConfigGetParameter(ctre::phoenix::ParamEnum::eProfileParamSlot_IZone, 0, CAN_TIMEOUT);
}
