/*******************************************************************************
 *
 * File: Gyro_Spi.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *  This file is based on the WPILib ADXRS450 Gyro class with minor
 *  modifications to allow for more configuration and use of the ADXRS453 gyro
 *  on the Spartan board.
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Gyro_Spi.h"

#include "frc/DriverStation.h"
#include "frc/livewindow/LiveWindow.h"
#include "gsutilities/Advisory.h"
#include "frc/Timer.h"

static constexpr double kSamplePeriod = 0.001;
#ifdef WIN32
static constexpr double kCalibrationSampleTime = 5.0;
#else
static units::second_t kCalibrationSampleTime = 5.0_s;
#endif

static constexpr double kDegreePerSecondPerLSB = 0.0125;

static constexpr uint8_t kRateRegister = 0x00;
static constexpr uint8_t kTemRegister = 0x02;
static constexpr uint8_t kLoCSTRegister = 0x04;
static constexpr uint8_t kHiCSTRegister = 0x06;
static constexpr uint8_t kQuadRegister = 0x08;
static constexpr uint8_t kFaultRegister = 0x0A;
static constexpr uint8_t kPIDRegister = 0x0C;
static constexpr uint8_t kSNHighRegister = 0x0E;
static constexpr uint8_t kSNLowRegister = 0x10;

using namespace frc;

namespace rfh
{

/*******************************************************************************
 *
 * Gyro constructor on the specified SPI port.
 *
 * @param port The SPI port the gyro is attached to.
 *
 ******************************************************************************/
Gyro_Spi::Gyro_Spi(SPI::Port port, uint32_t spi_cmd, uint8_t xfer_size,
    uint32_t valid_mask, uint32_t valid_value, uint8_t data_shift, uint8_t data_size, bool is_signed,
    bool big_endian, bool invert)
    : Gyro()
    , m_spi(port)
{
    m_spi.SetClockRate(3000000);
    // m_spi.SetMSBFirst();   // Does not work and is deprecated in 2023
    
    // m_spi.SetSampleDataOnLeadingEdge();   // deprecated in 2023
    // m_spi.SetClockActiveHigh();           // deprecated in 2023
    m_spi.SetMode(frc::SPI::Mode::kMode3);   // replaces the above lines, we think
    
    m_spi.SetChipSelectActiveLow();

    m_invert = 1.0;

    m_is_ready = true;
    // Validate the part ID
    if ((readRegister(kPIDRegister) & 0xff00) != 0x5200)
    {
        Advisory::pinfo("could not find ADXRS450 gyro");
        m_is_ready = false;
        return;
    }

    static constexpr auto kSamplePeriod = 0.001_s;
    m_spi.InitAccumulator(kSamplePeriod, spi_cmd, xfer_size, valid_mask, valid_value,
        data_shift, data_size, is_signed, big_endian);

    //Calibrate();

    // add invert
    if(invert == true)
    {
    	m_invert = -1.0;
    }

//    HALReport(HALUsageReporting::kResourceType_ADXRS450, port);         // this is a defined type that is close
//    LiveWindow::GetInstance()->AddSensor("ADXRS450_Gyro", port, this);  // this is a known name that is close
}


/*******************************************************************************
 *
 ******************************************************************************/
bool Gyro_Spi::isReady() const
{
    return( m_is_ready);
}

/*******************************************************************************
 *
 * Initialize the gyro.
 * Calibrate the gyro by running for a number of samples and computing the
 * center value.
 * Then use the center value as the Accumulator center value for subsequent
 * measurements.
 * It's important to make sure that the robot is not moving while the centering
 * calculations are in progress, this is typically done when the robot is first
 * turned on while it's sitting at rest before the competition starts.
 *
 ******************************************************************************/
void Gyro_Spi::calibrate()
{
 #ifdef WIN32
    Wait(0.1);
#else
    Wait(0.1_s);
#endif

    m_spi.SetAccumulatorCenter(0);
    m_spi.ResetAccumulator();

    Wait (kCalibrationSampleTime);

    m_spi.SetAccumulatorCenter((int32_t) m_spi.GetAccumulatorAverage());
    m_spi.ResetAccumulator();
}

/*******************************************************************************
 *
 ******************************************************************************/
static bool CalcParity(uint32_t v)
{
    bool parity = false;
    while (v != 0)
    {
        parity = !parity;
        v = v & (v - 1);
    }
    return parity;
}

/*******************************************************************************
 *
 ******************************************************************************/
static inline uint32_t BytesToIntBE(uint8_t* buf)
{
    uint32_t result = ((uint32_t) buf[0]) << 24;
    result |= ((uint32_t) buf[1]) << 16;
    result |= ((uint32_t) buf[2]) << 8;
    result |= (uint32_t) buf[3];
    return result;
}

/*******************************************************************************
 *
 ******************************************************************************/
uint16_t Gyro_Spi::readRegister(uint8_t reg)
{
    uint32_t cmd = 0x80000000 | (((uint32_t) reg) << 17);
    if (!CalcParity(cmd))
        cmd |= 1u;

    // big endian
    uint8_t buf[4] =
    { (uint8_t)((cmd >> 24) & 0xff), (uint8_t)((cmd >> 16) & 0xff), (uint8_t)((cmd >> 8) & 0xff), (uint8_t)(cmd & 0xff) };

    m_spi.Write(buf, 4);
    m_spi.Read(false, buf, 4);
    if ((buf[0] & 0xe0) == 0)
        return 0;  // error, return 0
    return (uint16_t)((BytesToIntBE(buf) >> 5) & 0xffff);
}

/*******************************************************************************
 *
 * Reset the gyro.
 * Resets the gyro to a heading of zero. This can be used if there is
 * significant
 * drift in the gyro and it needs to be recalibrated after it has been running.
 *
 ******************************************************************************/
void Gyro_Spi::reset()
{
    m_spi.ResetAccumulator();
}

/*******************************************************************************
 *
 * Return the actual angle in degrees that the robot is currently facing.
 *
 * The angle is based on the current accumulator value corrected by the
 * oversampling rate, the
 * gyro type and the A/D calibration values.
 * The angle is continuous, that is it will continue from 360->361 degrees. This
 * allows algorithms that wouldn't
 * want to see a discontinuity in the gyro output as it sweeps from 360 to 0 on
 * the second time around.
 *
 * @return the current heading of the robot in degrees. This heading is based on
 * integration
 * of the returned rate from the gyro.
 *
 ******************************************************************************/
double Gyro_Spi::getAngle() const
{
    return (double) (m_invert * m_spi.GetAccumulatorValue() * kDegreePerSecondPerLSB * kSamplePeriod);
}

/*******************************************************************************
 *
 * Return the rate of rotation of the gyro
 *
 * The rate is based on the most recent reading of the gyro analog value
 *
 * @return the current rate in degrees per second
 *
 ******************************************************************************/
double Gyro_Spi::getRate() const
{
    return (double) (m_invert * m_spi.GetAccumulatorLastValue() * kDegreePerSecondPerLSB);
}

} // end namespace
