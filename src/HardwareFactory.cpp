/*******************************************************************************
 *
 * File HardwareFactory.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/HardwareFactory.h"

#include <math.h>

#include "gsutilities/Advisory.h"

#include "rfhardware/Motor_Pwm.h"
#include "rfhardware/Motor_TalonSrx.h"
#include "rfhardware/Motor_TalonFx.h"
#include "rfhardware/Motor_VictorSpx.h"
#include "rfhardware/Motor_SparkMax.h"

using namespace frc;

/*******************************************************************************
 *
 * Create an instance of a motor / motor controller from the given XML element.
 *
 *  <motor type="Talon" [device="port"] [control_period="10"] [invert="false"] [follow="0"] >
 *      [<pid [kf="0.001"] [kp="0.0"] [ki="0.0"] [kd="0.0"] [kz="0.0"] [ctrl_min="-1.0"] [ctrl_max="1.0"] />]
 *      [<encoder  [invert="false"] [scale="1.0"] />]
 *      [<pot p1_raw="0.0" p1_cal="0.0" p2_raw="1.0" p2_cal="1.0" />]
 *      [<limit_switch name="lower" [port="0"] [normally_open="false"] [reset="false"] [reset_value="0.0"]]
 *      [<limit_switch name="upper" [port="1"] [normally_open="false"] [reset="false"] [reset_value="0.0"]]
 *  </motor>
 * 
 * 
 * For type=TalonFxCan only
 *  <motor type="TalonFxCan"  ...
 *       <follower type="TalonFxCan|TalonSpxCan|VictorSrxCan" device_id="22"
 *             invert="false" brake_mode="false" />
 *  </motor>
 *
 ******************************************************************************/
Motor *HardwareFactory::createMotor(tinyxml2::XMLElement* xml)
{
    Motor* mc = nullptr;

    int device_id  = -1;
    xml->QueryIntAttribute("device_id", &device_id);
    if ((device_id < 0) || (device_id > 63))
    {
        xml->QueryIntAttribute("port", &device_id);
        if ((device_id < 0) || (device_id > 63))
        {
            device_id = 1;
        }
    }/*
    int control_period = -1;
    xml->QueryIntAttribute("control_period", &control_period);
    if ((control_period < 10) || (control_period > 255))
    {
        control_period = 10;   
    }
    */
    int status_period_general = -1;
    xml->QueryIntAttribute("status_period_general", &status_period_general);
    if ((status_period_general < 10) || (status_period_general > 255))
    {
        status_period_general = 10;   
    }
    int status_period_feedback = -1;
    xml->QueryIntAttribute("status_period_feedback", &status_period_feedback);
    if ((status_period_feedback < 20) || (status_period_feedback > 255))
    {
        status_period_feedback = 20;   
    }

    const char *type    = xml->Attribute("type");
    if (strcmp("TalonSrxCan", type) == 0)
    {
        Advisory::pinfo("    creating %s with id=%d", type, device_id);
        Motor_TalonSrx* mct = new Motor_TalonSrx(device_id);
        mc = dynamic_cast<Motor*>(mct);
        parseMotorCommon(mc, xml);
        parseMotorTalonSrx(mct, xml);
    }
    else if (strcmp("TalonFxCan", type) == 0)
    {
        std::string canBusName  = "rio";
        const char *canbus = xml->Attribute("canbus");
        if (canbus != nullptr && strcmp(canbus, "RIO") && strcmp(canbus, "rio"))
        {
            canBusName  = canbus;
            Advisory::pinfo("canbusname = %s!!!", canBusName.c_str());
        }
        Advisory::pinfo("    creating %s with id=%d on CAN Bus = %s", type, device_id, canBusName.c_str());
        Motor_TalonFx* mct = new Motor_TalonFx(device_id, canBusName, status_period_general, status_period_feedback);
        mc = dynamic_cast<Motor*>(mct);
        parseMotorCommon(mc, xml);
        parseMotorTalonFx(mct, xml);
    }
    else if (strcmp("TalonFxCanV6", type) == 0)
    {
        std::string canBusName  = "rio";
        const char *canbus = xml->Attribute("canbus");
        if (canbus != nullptr && strcmp(canbus, "RIO") && strcmp(canbus, "rio"))
        {
            canBusName  = canbus;
            Advisory::pinfo("canbusname = %s!!!", canBusName.c_str());
        }
        Advisory::pinfo("    creating %s with id=%d on CAN Bus = %s", type, device_id, canBusName.c_str());
        Motor_TalonFxV6 *mct = new Motor_TalonFxV6(device_id, canBusName, status_period_general, status_period_feedback);
        mc = dynamic_cast<Motor*>(mct);
        parseMotorCommon(mc, xml);
        parseMotorTalonFxV6(mct, xml);
    }
    else if (strcmp("VictorSpxCan", type) == 0)
    {
        Advisory::pinfo("    creating %s with id=%d", type, device_id);
        Motor_VictorSpx* mct = new Motor_VictorSpx(device_id);
        mc = dynamic_cast<Motor*>(mct);
        parseMotorCommon(mc, xml);
    }
    else if (strcmp("SparkMaxCan", type) == 0)
    {
    Advisory::pinfo("    creating %s with id=%d", type, device_id);
        Motor_SparkMax* mct = new Motor_SparkMax(device_id);
        mc = dynamic_cast<Motor*>(mct);
        parseMotorCommon(mc, xml);
        parseMotorSparkMax(mct, xml);
        mct->burnFlash();
    }
    else if (strcmp("Talon", type) == 0)
    {
        Advisory::pinfo("    creating PWM %s with id=%d", type, device_id);
        Motor_Pwm* mcp = new Motor_Pwm(new Talon(device_id));
        mc = dynamic_cast<Motor*>(mcp);
        parseMotorCommon(mc, xml);
        parseMotorPwm(mcp, xml);
    }
    else if (strcmp("Victor", type) == 0)
    {
        Advisory::pinfo("    creating PWM %s with id=%d", type, device_id);
        Motor_Pwm* mcp = new Motor_Pwm(new Victor(device_id));
        mc = dynamic_cast<Motor*>(mcp);
        parseMotorCommon(mc, xml);
        parseMotorPwm(mcp, xml);
    }
    else if (strcmp("VictorSp", type) == 0)
    {
        Advisory::pinfo("    creating PWM %s with id=%d", type, device_id);
        Motor_Pwm* mcp = new Motor_Pwm(new VictorSP(device_id));
        mc = dynamic_cast<Motor*>(mcp);
        parseMotorCommon(mc, xml);
        parseMotorPwm(mcp, xml);
    }
    else if (strcmp("Jaguar", type) == 0)
    {
        Advisory::pinfo("    creating PWM %s with id=%d", type, device_id);
        Motor_Pwm* mcp = new Motor_Pwm(new Jaguar(device_id));
        mc = dynamic_cast<Motor*>(mcp);
        parseMotorCommon(mc, xml);
        parseMotorPwm(mcp, xml);
    }
    else
    {
        Advisory::pinfo("    failed to create motor, unknown type \"%s\"", type);
    }

    return mc;
}

/*******************************************************************************
 *
 ******************************************************************************/
void HardwareFactory::parseMotorCommon(Motor *motor, tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement *elem;

    bool invert_motor = false;
    xml->QueryBoolAttribute("invert", &invert_motor);
    motor->setMotorInvert(invert_motor);

    bool invert_sensor = false;
    xml->QueryBoolAttribute("invert_sensor", &invert_sensor);
    motor->setSensorInvert(invert_sensor);

    bool brake_mode = false;
    xml->QueryBoolAttribute("brake_mode", &brake_mode);
    Advisory::pinfo(" **************   setting break mode %d", brake_mode);
    motor->setBrakeMode(brake_mode?Motor::BrakeMode::BRAKE:Motor::BrakeMode::COAST);

    //
    // PID
    //
    elem = xml->FirstChildElement("pid");
    if (elem != nullptr)
    {
        BasicPid *pid = createPid(elem);
        motor->setPid(pid);
    }

    //
    // Current Limits ?
    //

//    int fuse = -1;
//    xml->QueryIntAttribute("fuse", &fuse);
//    sc_pwm->SetFuse(fuse);
}

/*******************************************************************************
 *
 ******************************************************************************/
void HardwareFactory::parseMotorPwm(Motor_Pwm *motor, tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement *elem;

    //
    // Limit Switches
    //
    elem = xml->FirstChildElement("limit_switch");
    while (elem != nullptr)
    {
        const char *sw_name    = elem->Attribute("name");
        if (strcmp("lower", sw_name) == 0)
        {
            motor->setLowerLimitSwitch(createLimitSwitch(elem));
        }
        else if (strcmp("upper", sw_name) == 0)
        {
            motor->setUpperLimitSwitch(createLimitSwitch(elem));
        }
        else
        {
            Advisory::pinfo ("motor limit switch unknown name of \"%s\" found", sw_name);
        }

        elem = elem->NextSiblingElement("limit_switch");
    }

    //
    // Pot / Analog Input
    //
    elem = xml->FirstChildElement("analog_input");
    if (elem != nullptr)
    {
        motor->setAnalogInput(createScaledAnalogInput(elem));
    }

    //
    // Encoder
    //
    // elem = xml->FirstChildElement("encoder");
    // if (elem != nullptr)
    // {
    //     motor->setEncoder(createEncoder(elem));
    // }
}

/*******************************************************************************
 *
 ******************************************************************************/
void HardwareFactory::parseMotorTalonSrx(Motor_TalonSrx *motor, tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement *elem;

    //
    // Limit Switches
    //
    elem = xml->FirstChildElement("limit_switch");
    while (elem != nullptr)
    {
        int16_t port = 0;
        LimitSwitch::InputMode mode = LimitSwitch::INPUT_NORMALLY_OPEN;
        bool reset = false;
        double reset_value = 0.0;

        const char *sw_name    = elem->Attribute("name");
        if (strcmp("lower", sw_name) == 0)
        {
            parseLimitSwitch(elem, port, mode, reset, reset_value);
            motor->setLowerLimitSwitch(port, (mode==LimitSwitch::INPUT_NORMALLY_OPEN), reset, reset_value);
        }
        else if (strcmp("upper", sw_name) == 0)
        {
            parseLimitSwitch(elem, port, mode, reset, reset_value);
            motor->setUpperLimitSwitch(port, (mode==LimitSwitch::INPUT_NORMALLY_OPEN), reset, reset_value);
        }
        else
        {
            Advisory::pinfo ("motor limit switch unknown name of \"%s\" found", sw_name);
        }

        elem = elem->NextSiblingElement("limit_switch");
    }

    //
    // Pot / Analog Input
    //
    elem = xml->FirstChildElement("analog_input");
    if (elem != nullptr)
    {
        bool invert_sensor = false;
        elem->QueryBoolAttribute("invert", &invert_sensor);
        motor->setSensorInvert(invert_sensor);

        bool sensor_wrap = false;
        elem->QueryBoolAttribute("sensor_wrap", &sensor_wrap);
        motor->setFeedbackDevice(Motor_TalonSrx::INTERNAL_POTENTIOMETER, sensor_wrap);

        double scale = 1.0;
        double offset = 0.0;
        double min_raw = 0.05;
        double max_raw = 0.95;
        parseScaledAnalogInput(elem, scale, offset, min_raw, max_raw);
        motor->setSensorScale(scale, offset);

        Advisory::pinfo("  motor controller set to internal pot, sensor_wrap=%s, invert=%s, scale=%f offset=%f",
                sensor_wrap?"true":"false", invert_sensor?"true":"false", scale, offset);
    }

    //
    // Encoder
    //
    elem = xml->FirstChildElement("encoder");
    if (elem != nullptr)
    {
        motor->setFeedbackDevice(Motor_TalonSrx::INTERNAL_ENCODER, true);

        bool invert_sensor = false;
        elem->QueryBoolAttribute("invert", &invert_sensor);
        motor->setSensorInvert(invert_sensor);

        double scale = 1.0;
        elem->QueryDoubleAttribute("scale", &scale);
        motor->setSensorScale(scale);

        //@TODO: add advisory
    }

    //
    // Followers
    //
    elem = xml->FirstChildElement("follower");
    while (elem != nullptr)
    {
        int device_id  = -1;
        elem->QueryIntAttribute("device_id", &device_id);
        if ((device_id < 0) || (device_id > 63))
        {
            device_id = 1;
        }

        bool invert_motor = false;
        elem->QueryBoolAttribute("invert", &invert_motor);

        bool brake_mode = false;
        elem->QueryBoolAttribute("brake_mode", &brake_mode);

        const char *type    = elem->Attribute("type");
        if (strcmp("TalonSrxCan", type) == 0)
        {
            Advisory::pinfo("    creating %s follower with id=%d", type, device_id);
            ctre::phoenix::motorcontrol::can::TalonSRX* mcf = new TalonSRX(device_id);
            mcf->Follow(motor->getController());
            mcf->SetInverted(invert_motor);
            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else if (strcmp("TalonFxCan", type) == 0)
        {
            Advisory::pinfo("    creating %s follower with id=%d", type, device_id);
            ctre::phoenix::motorcontrol::can::TalonFX* mcf = new TalonFX(device_id);
            mcf->Follow(motor->getController());
            mcf->SetInverted(invert_motor);
            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else if (strcmp("VictorSpxCan", type) == 0)
        {
            Advisory::pinfo("    creating %s follower with id=%d", type, device_id);
            ctre::phoenix::motorcontrol::can::VictorSPX* mcf = new VictorSPX(device_id);
            mcf->Follow(motor->getController());
            mcf->SetInverted(invert_motor);
            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else
        {
            Advisory::pwarning("    could not create follower of type %s", type);
        }

        elem = elem->NextSiblingElement("follower");
    }
}

/*******************************************************************************
 *
 ******************************************************************************/
void HardwareFactory::parseMotorTalonFx(Motor_TalonFx *motor, tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement *elem;

    //
    // Current Limits
    //
    double current_limit = 27.0;
    double peak_current = 30.0;
    double peak_current_dur = 1.0;
    double stator_current_limit = 60.0;
    double stator_peak_current = 100.0;
    double stator_peak_current_dur = 1.0;
    float peak_forward_voltage = 12.0;
    float peak_reverse_voltage = -12.0;
    float nominal_forward_voltage = 0.0;
    float nominal_reverse_voltage = 0.0;

    xml->QueryDoubleAttribute("current_limit", &current_limit);
    xml->QueryDoubleAttribute("peak_current", &peak_current);
    xml->QueryDoubleAttribute("peak_current_dur", &peak_current_dur);
    if (current_limit != 0 || peak_current != 0 || peak_current_dur != 0)
    {
        motor->setCurrentLimit(true, current_limit, peak_current, peak_current_dur);
    }
    xml->QueryDoubleAttribute("stator_current_limit", &stator_current_limit);
    xml->QueryDoubleAttribute("stator_peak_current", &stator_peak_current);
    xml->QueryDoubleAttribute("stator_peak_current_dur", &stator_peak_current_dur);
    if (stator_current_limit != 0 || stator_peak_current != 0 || stator_peak_current_dur != 0)
    {
        motor->setStatorCurrentLimit(true, stator_current_limit, stator_peak_current, stator_peak_current_dur);
    }

    xml->QueryFloatAttribute("peak_forward_voltage", &peak_forward_voltage);
    xml->QueryFloatAttribute("peak_reverse_voltage", &peak_reverse_voltage);
    xml->QueryFloatAttribute("nominal_forward_voltage", &nominal_forward_voltage);
    xml->QueryFloatAttribute("nominal_reverse_voltage", &nominal_reverse_voltage);

    motor->setClosedLoopOutputLimits(nominal_forward_voltage, nominal_reverse_voltage, peak_forward_voltage,
                              peak_reverse_voltage);

    //
    // Limit Switches
    //
    elem = xml->FirstChildElement("limit_switch");
    while (elem != nullptr)
    {
        int16_t port = 0;
        LimitSwitch::InputMode mode = LimitSwitch::INPUT_NORMALLY_OPEN;
        bool reset = false;
        double reset_value = 0.0;

        const char *sw_name    = elem->Attribute("name");
        if (strcmp("lower", sw_name) == 0)
        {
            parseLimitSwitch(elem, port, mode, reset, reset_value);
            motor->setLowerLimitSwitch(port, (mode==LimitSwitch::INPUT_NORMALLY_OPEN), reset, reset_value);
        }
        else if (strcmp("upper", sw_name) == 0)
        {
            parseLimitSwitch(elem, port, mode, reset, reset_value);
            motor->setUpperLimitSwitch(port, (mode==LimitSwitch::INPUT_NORMALLY_OPEN), reset, reset_value);
        }
        else
        {
            Advisory::pinfo ("motor limit switch unknown name of \"%s\" found", sw_name);
        }

        elem = elem->NextSiblingElement("limit_switch");
    }
    
    //
    // Encoder
    //
    elem = xml->FirstChildElement("encoder");
    if (elem != nullptr)
    {
        motor->setFeedbackDevice(Motor_TalonFx::INTEGRATED_SENSOR, true);

        bool invert_sensor = false;
        elem->QueryBoolAttribute("invert", &invert_sensor);
        motor->setSensorInvert(invert_sensor);

        double scale = 1.0;
        elem->QueryDoubleAttribute("scale", &scale);
        motor->setSensorScale(scale);

        //@TODO: add advisory
      Advisory::pinfo ("Encoder %d %f created", invert_sensor, scale);
    }

    //
    // Followers
    //
    elem = xml->FirstChildElement("follower");
    while (elem != nullptr)
    {
        int device_id  = -1;
        elem->QueryIntAttribute("device_id", &device_id);
        if ((device_id < 0) || (device_id > 63))
        {
            device_id = 1;
        }

        bool invert_motor = false;
        elem->QueryBoolAttribute("invert", &invert_motor);

        bool brake_mode = false;
        elem->QueryBoolAttribute("brake_mode", &brake_mode);

        const char *type    = elem->Attribute("type");
        if (strcmp("TalonSrxCan", type) == 0)
        {
            Advisory::pinfo("    creating %s follower with id=%d", type, device_id);
            ctre::phoenix::motorcontrol::can::TalonSRX* mcf = new TalonSRX(device_id);
            mcf->Follow(motor->getController());
            mcf->SetInverted(invert_motor);
            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else if (strcmp("TalonFxCan", type) == 0)
        {
            Advisory::pinfo("    creating %s follower with id=%d", type, device_id);
            ctre::phoenix::motorcontrol::can::TalonFX* mcf = new TalonFX(device_id);
            mcf->Follow(motor->getController());
            mcf->SetInverted(invert_motor);
            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else if (strcmp("VictorSpxCan", type) == 0)
        {
            Advisory::pinfo("    creating %s follower with id=%d", type, device_id);
            ctre::phoenix::motorcontrol::can::VictorSPX* mcf = new VictorSPX(device_id);
            mcf->Follow(motor->getController());
            mcf->SetInverted(invert_motor);
            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else
        {
            Advisory::pwarning("    could not create follower of type %s", type);
        }

        elem = elem->NextSiblingElement("follower");
    }
}

/*******************************************************************************
 *
 ******************************************************************************/
void HardwareFactory::parseMotorTalonFxV6(Motor_TalonFxV6 *motor, tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement *elem;

    //
    // Current Limits
    //
    double current_limit = 27.0;
    double peak_current = 30.0;
    double peak_current_dur = 1.0;
    double stator_current_limit = 60.0;
//    double stator_peak_current = 100.0;
    float peak_forward_voltage = 12.0;
    float peak_reverse_voltage = -12.0;
    float nominal_forward_voltage = 0.0;
    float nominal_reverse_voltage = 0.0;

    xml->QueryDoubleAttribute("current_limit", &current_limit);
    xml->QueryDoubleAttribute("peak_current", &peak_current);
    xml->QueryDoubleAttribute("peak_current_dur", &peak_current_dur);
    Advisory::pinfo("   HardwareFactory::setCurrentLimit amps=%f, peak=%f, duration=%f", current_limit, peak_current, peak_current_dur);
    if (current_limit != 0 || peak_current != 0 || peak_current_dur != 0)
    {
        motor->setCurrentLimit(true, current_limit, peak_current, peak_current_dur);
    }
    xml->QueryDoubleAttribute("stator_current_limit", &stator_current_limit);
    if (stator_current_limit != 0)
    {
        motor->setStatorCurrentLimit(true, stator_current_limit);
    }

    xml->QueryFloatAttribute("peak_forward_voltage", &peak_forward_voltage);
    xml->QueryFloatAttribute("peak_reverse_voltage", &peak_reverse_voltage);
    xml->QueryFloatAttribute("nominal_forward_voltage", &nominal_forward_voltage);
    xml->QueryFloatAttribute("nominal_reverse_voltage", &nominal_reverse_voltage);

    motor->setClosedLoopOutputLimits(nominal_forward_voltage, nominal_reverse_voltage, peak_forward_voltage,
                              peak_reverse_voltage);

    //
    // Limit Switches
    //
    elem = xml->FirstChildElement("limit_switch");
    while (elem != nullptr)
    {
//        int16_t port = 0;
//        LimitSwitch::InputMode mode = LimitSwitch::INPUT_NORMALLY_OPEN;
//        bool reset = false;
//        double reset_value = 0.0;
//				<limit_switch name="lower" normally_open="true" reset="false" reset_value="false" />

        const char *sw_name    = elem->Attribute("name");
        if (strcmp("lower", sw_name) == 0)
        {
            motor->setLowerLimitSwitch(createLimitSwitch(elem));
        }
        else if (strcmp("upper", sw_name) == 0)
        {
            motor->setUpperLimitSwitch(createLimitSwitch(elem));
        }
        else
        {
            Advisory::pinfo ("motor limit switch unknown name of \"%s\" found", sw_name);
        }

        elem = elem->NextSiblingElement("limit_switch");
    }
    
    //
    // Encoder
    //
    elem = xml->FirstChildElement("encoder");
    if (elem != nullptr)
    {
        motor->setFeedbackDevice(Motor_TalonFxV6::INTEGRATED_SENSOR, true);

        bool invert_sensor = false;
        elem->QueryBoolAttribute("invert", &invert_sensor);
        motor->setSensorInvert(invert_sensor);

        double scale = 1.0;
        elem->QueryDoubleAttribute("scale", &scale);
        motor->setSensorScale(scale);

        //@TODO: add advisory
      Advisory::pinfo ("Encoder %d %f created", invert_sensor, scale);
    }

    //
    // Followers
    //
    elem = xml->FirstChildElement("follower");
    while (elem != nullptr)
    {
        int device_id  = -1;
        elem->QueryIntAttribute("device_id", &device_id);
        if ((device_id < 0) || (device_id > 63))
        {
            device_id = 1;
        }

        bool invert_motor = false;
        elem->QueryBoolAttribute("invert", &invert_motor);

        bool brake_mode = false;
        elem->QueryBoolAttribute("brake_mode", &brake_mode);
        const char *canbus = xml->Attribute("canbus");

        const char *type    = elem->Attribute("type");
        if (strcmp("TalonSrxCan", type) == 0)
        {
            Advisory::pinfo("    Cannot create follower %s with id=%d", type, device_id);
            /* Advisory::pinfo("    creating %s follower with id=%d", type, device_id);
            ctre::phoenix::motorcontrol::can::TalonSRX* mcf = new TalonSRX(device_id);
            mcf->Follow(motor->getController());
            mcf->SetInverted(invert_motor);
            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast); */
        }
        else if (strcmp("TalonFxCanV6", type) == 0)
        {
            Advisory::pinfo("    creating %s follower with id=%d following id=%d", type, device_id, motor->getDeviceID());
            ctre::phoenix6::hardware::TalonFX* mcf = new ctre::phoenix6::hardware::TalonFX(device_id, canbus);
            mcf->SetControl(ctre::phoenix6::controls::Follower{motor->getDeviceID(), false});
            mcf->SetInverted(invert_motor);
            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else if (strcmp("VictorSpxCan", type) == 0)
        {
            Advisory::pinfo("    Cannot create follower %s with id=%d", type, device_id);
            /* Advisory::pinfo("    creating %s follower with id=%d", type, device_id);
            ctre::phoenix::motorcontrol::can::VictorSPX* mcf = new VictorSPX(device_id);
            mcf->Follow(motor->getContr                                                              oller());
            mcf->SetInverted(invert_motor);
            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
            */
        }
        else
        {
            Advisory::pwarning("    could not create follower of type %s", type);
        }

        elem = elem->NextSiblingElement("follower");
    }
}

void HardwareFactory::parseMotorSparkMax(Motor_SparkMax *motor, tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement *elem;

    // Current Limits are set to not burn up the motor
    // From Rev document 2/2024
    // https://www.revrobotics.com/neo-550-brushless-motor-locked-rotor-testing/
    // Limit            Motor destruction
    // 20 A             N/A no failure before 227 seconds
    // 40 A             Fail after ~27 seconds
    // 60 A             Fail after ~5.5 seconds
    // 80 A             Fail after ~2.0 seconds
    unsigned int current_limit = 35;
    xml->QueryUnsignedAttribute("current_limit", &current_limit);
    if (current_limit != 0)
    {
        motor->setCurrentLimit(current_limit);
    }

    //
    // Limit Switches
    //
    elem = xml->FirstChildElement("limit_switch");
    while (elem != nullptr)
    {
        int16_t port = 0;
        LimitSwitch::InputMode mode = LimitSwitch::INPUT_NORMALLY_OPEN;
        bool reset = false;
        bool enabled = true;
        double reset_value = 0.0;

        const char *sw_name    = elem->Attribute("name");
        if (strcmp("lower", sw_name) == 0)
        {
            parseLimitSwitch(elem, port, mode, reset, reset_value);
            Advisory::pinfo ("motor limit switch name \"%s\" created", sw_name);
            motor->setLowerLimitSwitch((mode==LimitSwitch::INPUT_NORMALLY_OPEN), enabled, reset);
        }
        else if (strcmp("upper", sw_name) == 0)
        {
            parseLimitSwitch(elem, port, mode, reset, reset_value);
            Advisory::pinfo ("motor limit switch name \"%s\" created", sw_name);
            motor->setUpperLimitSwitch((mode==LimitSwitch::INPUT_NORMALLY_OPEN), enabled, reset);
        }
        else
        {
            Advisory::pinfo ("motor limit switch unknown name of \"%s\" found", sw_name);
        }

        elem = elem->NextSiblingElement("limit_switch");
    }
    
    //
    // Encoder
    //
    elem = xml->FirstChildElement("encoder");
    if (elem != nullptr)
    {
        motor->setFeedbackDevice(Motor_SparkMax::INTERNAL_ENCODER, true);

        bool invert_sensor = false;
        elem->QueryBoolAttribute("invert", &invert_sensor);
        motor->setSensorInvert(invert_sensor);

        double scale = 1.0;
        elem->QueryDoubleAttribute("scale", &scale);
        motor->setSensorScale(scale);

        //@TODO: add advisory
      Advisory::pinfo ("Encoder %d %f created", invert_sensor, scale);
}

    //
    // Followers
    //
    elem = xml->FirstChildElement("follower");
    while (elem != nullptr)
    {
        int device_id  = -1;
        elem->QueryIntAttribute("device_id", &device_id);
        if ((device_id < 0) || (device_id > 63))
        {
            device_id = 1;
        }

        bool invert_motor = false;
        elem->QueryBoolAttribute("invert", &invert_motor);

        bool brake_mode = false;
        elem->QueryBoolAttribute("brake_mode", &brake_mode);

        const char *type    = elem->Attribute("type");
        if (strcmp("TalonSrxCan", type) == 0)
        {
            Advisory::pinfo("  NOT IMPLEMENTED  creating %s follower with id=%d", type, device_id);
//            ctre::phoenix::motorcontrol::can::TalonSRX* mcf = new TalonSRX(device_id);
//            mcf->Follow(motor->getController());
//            mcf->SetInverted(invert_motor);
//            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else if (strcmp("TalonFxCan", type) == 0)
        {
            Advisory::pinfo("    NOT IMPLEMENTED  creating %s follower with id=%d", type, device_id);
//            ctre::phoenix::motorcontrol::can::TalonFX* mcf = new TalonFX(device_id);
 //           mcf->Follow(motor->getController());
//            mcf->SetInverted(invert_motor);
//            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else if (strcmp("VictorSpxCan", type) == 0)
        {
            Advisory::pinfo("    NOT IMPLEMENTED  creating %s follower with id=%d", type, device_id);
//            ctre::phoenix::motorcontrol::can::VictorSPX* mcf = new VictorSPX(device_id);
//            mcf->Follow(motor->getController());
//            mcf->SetInverted(invert_motor);
//            mcf->SetNeutralMode(brake_mode?NeutralMode::Brake:NeutralMode::Coast);
        }
        else
        {
            Advisory::pwarning("    could not create follower of type %s", type);
        }

        elem = elem->NextSiblingElement("follower");
    }
}
#if 0 // @TODO: this method should be deleted after createMotor is finished
RSpeedController *HardwareFactory::createRSpeedController(tinyxml2::XMLElement* xml)
{
	RSpeedController *sc = nullptr;

    int device  = 1;
    xml->QueryIntAttribute("port", &device);
	if ((device < 0) || (device > 63))
	{
		device = 1;
	}

    int ctrl_period=10;
    xml->QueryIntAttribute("control_period", &ctrl_period);

    const char *type    = xml->Attribute("type");
    if ((strcmp("CanTalon", type) == 0)  || (strcmp("TalonSrxCan", type) == 0 || (strcmp("TalonFxCan", type) == 0)))
	{
	    sc = new RSpeedControllerCan(device, ctrl_period);

	    bool invert_motor = false;
	    bool invert_sensor = false;
		bool brake_mode = false;

	    int current_limit = 0;
	    int peak_current = 0;
	    int peak_current_dur = 0;

        float peak_forward_voltage = 12.0;
        float peak_reverse_voltage = -12.0;
        float nominal_forward_voltage = 0.0;
        float nominal_reverse_voltage = 0.0;

	    double scale=1.0;
	    if(nullptr != sc)
	    {
	        xml->QueryBoolAttribute("invert", &invert_motor);
	        sc->InvertMotor(invert_motor);

	        xml->QueryBoolAttribute("brake_mode", &brake_mode);
			sc->SetBrakeMode(brake_mode);

	        xml->QueryIntAttribute("current_limit", &current_limit);
	        xml->QueryIntAttribute("peak_current", &peak_current);
	        xml->QueryIntAttribute("peak_current_dur", &peak_current_dur);

	        sc->SetCurrentLimit(current_limit, peak_current, peak_current_dur);

	        tinyxml2::XMLElement *elem;
	        if (current_limit != 0 || peak_current != 0 || peak_current_dur != 0)
	        {
	        	sc->SetCurrentLimitEnabled(true);
	        }

	        xml->QueryFloatAttribute("peak_forward_voltage", &peak_forward_voltage);
	        xml->QueryFloatAttribute("peak_reverse_voltage", &peak_reverse_voltage);
	        xml->QueryFloatAttribute("nominal_forward_voltage", &nominal_forward_voltage);
	        xml->QueryFloatAttribute("nominal_reverse_voltage", &nominal_reverse_voltage);

	        sc->SetClosedLoopOutputLimits(nominal_forward_voltage, nominal_reverse_voltage,
	            peak_forward_voltage, peak_reverse_voltage);

	        elem = xml->FirstChildElement("encoder");
	        if (elem != nullptr)
	        {
	            sc->SetFeedbackDevice(RSpeedController :: INTERNAL_ENCODER);

	            elem->QueryDoubleAttribute("scale", &scale);
	            sc->SensorOutputPerCount(scale);
	            bool is_drive=false;

	            elem->QueryBoolAttribute("drive", &is_drive);

	            elem->QueryBoolAttribute("invert", &invert_sensor);
                sc->InvertSensor(invert_sensor, is_drive);
	        }
			else
			{
				elem = xml->FirstChildElement("pot");
				if (elem != nullptr)
				{
					bool sensor_wrap = false;
					elem->QueryBoolAttribute("sensor_wrap", &sensor_wrap);

					sc->SetFeedbackDevice(RSpeedController :: INTERNAL_POTENTIOMETER, (sensor_wrap?1:0));

					elem->QueryBoolAttribute("invert", &invert_sensor);
					sc->InvertSensor(invert_sensor);

					float raw1 	= 0.0;
					float raw2  = 5.0;
					float calc1 = 0.0;
					float calc2 = 5.0;

					float scale = 1.0;
					float offset = 0.0;

					elem->QueryFloatAttribute("p1_raw", &raw1);
					elem->QueryFloatAttribute("p2_raw", &raw2);
					elem->QueryFloatAttribute("p1_cal", &calc1);
					elem->QueryFloatAttribute("p2_cal", &calc2);

					scale	= (calc2 - calc1)/(raw2 - raw1);
					offset 	= calc1-raw1*scale;
					sc->SensorOutputPerCount(scale, offset);

					Advisory::pinfo("  motor controller set to internal pot, sensor_wrap=%s, invert=%s, p1(%f,%f) p2(%f,%f), scale=%f offset=%f",
							sensor_wrap?"true":"false", invert_sensor?"true":"false", raw1, calc1, raw2, calc2, scale, offset);
				}
			}

	        bool has_upper_limit = false;
	        bool has_lower_limit = false;
			bool upper_normally_open=false;
			bool lower_normally_open=false;
			bool upper_reset_zero=false;
			bool lower_reset_zero=false;

	        elem = xml->FirstChildElement("digital_input");
			while (elem != nullptr)
			{
			    const char *sw_name    = elem->Attribute("name");
			    if (strcmp("upper_limit", sw_name) == 0)
				{
			    	elem->QueryBoolAttribute("normally_open", &upper_normally_open);
			    	elem->QueryBoolAttribute("reset_zero", &upper_reset_zero);
					sc->SetForwardLimitSwitch(upper_normally_open, true, upper_reset_zero);

			    	has_upper_limit = true;
				}
			    else if (strcmp("lower_limit", sw_name) == 0)
			    {
			    	elem->QueryBoolAttribute("normally_open", &lower_normally_open);
			    	elem->QueryBoolAttribute("reset_zero", &lower_reset_zero);
					sc->SetReverseLimitSwitch(lower_normally_open, true, lower_reset_zero);

			    	has_lower_limit = true;
			    }
			    else
			    {
			    	Advisory::pinfo ("motor limit switch unknown name of %s found", sw_name);
			    }

				elem = elem->NextSiblingElement("digital_input");
			}

			// if the limits were set in a previous configuration,
			// we should disable them now
            if (! has_upper_limit)
            {
                sc->SetForwardLimitSwitch(false, false, false);
            }

            if (! has_lower_limit)
            {
                sc->SetReverseLimitSwitch(false, false, false);
            }

            double kf = 0.001;
            double kp = 0.0;
            double ki = 0.0;
            double kd = 0.0;
            double iz = 0.0;
            double cruise_velocity = 0.0;
            double acceleration = 0.0;

            elem = xml->FirstChildElement("pid");
	        if (elem != nullptr)
	        {
				elem->QueryDoubleAttribute("kf", &kf);
				elem->QueryDoubleAttribute("kp", &kp);
				elem->QueryDoubleAttribute("ki", &ki);
				elem->QueryDoubleAttribute("kd", &kd);
				elem->QueryDoubleAttribute("iz", &iz);
				elem->QueryDoubleAttribute("cruise_velocity", &cruise_velocity);
				elem->QueryDoubleAttribute("acceleration", &acceleration);

	            sc->SetF(kf);
	            sc->SetP(kp);
	            sc->SetI(ki);
	            sc->SetD(kd);

	            if (iz != 0.0)
	            {
	            	sc->SetIZone(iz);
	            }

	            if (cruise_velocity != 0.0)
	            {
	            	sc->SetCruiseVelocity(cruise_velocity);
	            }

	            if (acceleration != 0.0)
	            {
	            	sc->SetAcceleration(acceleration);
	            }
	        }

	        Advisory::pinfo("    creating CAN Talon Speed Controller on device %d, invert_m=%s, invert_s=%s scale=%.3f, kf=%f, kp=%f, ki=%f, kd=%f, cruise_velocity=%f, acceleration=%f",
	        	device, invert_motor?"true":"false",invert_sensor?"true":"false", scale, kf, kp, ki, kd, cruise_velocity, acceleration);
	    }
	    else
	    {
	        Advisory::pinfo("    failed to create speed controller of type \"%s\"", type);
	    }
	}
	else if (strcmp("VictorSpxCan", type) == 0)
	{
	    sc = new RSpeedControllerVictorSpxCan(device, ctrl_period);

	    if(nullptr != sc)
	    {
	        Advisory::pinfo("    creating %s Speed Controller", type);

    	    bool invert_motor = false;
			bool brake_mode = false;

	        xml->QueryBoolAttribute("invert", &invert_motor);
	        sc->InvertMotor(invert_motor);

	        xml->QueryBoolAttribute("brake_mode", &brake_mode);
			sc->SetBrakeMode(brake_mode);
	    }
	    else
	    {
	        Advisory::pinfo("    failed to create speed controller of type \"%s\"", type);
	    }
	}
	else if (strcmp("SparkMaxCan", type) == 0)
	{

	    sc = new RSpeedControllerSparkMaxCan(device, ctrl_period);

	    if(nullptr != sc)
	    {
	        tinyxml2::XMLElement *elem;
			Advisory::pinfo("    creating %s Speed Controller", type);

			 elem = xml->FirstChildElement("encoder");
			 if (elem != nullptr)
	        {
				//sc->SetFeedbackDevice(RSpeedControllerSparkMaxCan :: );
				double scale = 1.0;
	            elem->QueryDoubleAttribute("scale", &scale);
	            sc->SensorOutputPerCount(scale);
	            bool is_drive=false;

	            // elem->QueryBoolAttribute("drive", &is_drive);

	            // elem->QueryBoolAttribute("invert", &invert_sensor);
                // sc->InvertSensor(invert_sensor, is_drive);
	        }

            double kf = 0.001;
            double kp = 0.0;
            double ki = 0.0;
            double kd = 0.0;
            double iz = 0.0;
            double cruise_velocity = 0.0;
            double acceleration = 0.0;
			
            elem = xml->FirstChildElement("pid");
	        if (elem != nullptr)
	        {
				elem->QueryDoubleAttribute("kf", &kf);
				elem->QueryDoubleAttribute("kp", &kp);
				elem->QueryDoubleAttribute("ki", &ki);
				elem->QueryDoubleAttribute("kd", &kd);
				elem->QueryDoubleAttribute("iz", &iz);
				elem->QueryDoubleAttribute("cruise_velocity", &cruise_velocity);
				elem->QueryDoubleAttribute("acceleration", &acceleration);

	            sc->SetF(kf);
	            sc->SetP(kp);
	            sc->SetI(ki);
	            sc->SetD(kd);

	            if (iz != 0.0)
	            {
	            	sc->SetIZone(iz);
	            }

	            if (cruise_velocity != 0.0)
	            {
	            	sc->SetCruiseVelocity(cruise_velocity);
	            }

	            if (acceleration != 0.0)
	            {
	            	sc->SetAcceleration(acceleration);
	            }
	        }


    	    bool invert_motor = false;
			bool brake_mode = false;

	        xml->QueryBoolAttribute("invert", &invert_motor);
	        sc->InvertMotor(invert_motor);

	        xml->QueryBoolAttribute("brake_mode", &brake_mode);
			sc->SetBrakeMode(brake_mode);
	    }
	    else
	    {
	        Advisory::pinfo("    failed to create speed controller of type \"%s\"", type);
	    }
	}
    else
    {
    	RSpeedControllerPwm * sc_pwm = nullptr;
        if (strcmp("Talon", type) == 0)
        {
        	sc_pwm = new RSpeedControllerPwm(new Talon(device), ctrl_period);
        }
        else if (strcmp("Victor", type) == 0)
        {
        	sc_pwm = new RSpeedControllerPwm(new Victor(device), ctrl_period);
        }
        else if (strcmp("VictorSP", type) == 0)
        {
        	sc_pwm = new RSpeedControllerPwm(new VictorSP(device), ctrl_period);
        }
        else if (strcmp("Jaguar", type) == 0)
        {
        	sc_pwm = new RSpeedControllerPwm(new Jaguar(device), ctrl_period);
        }

        sc = sc_pwm;

        if(nullptr != sc)
        {
            bool invert_motor = false;
            int fuse = -1;

            xml->QueryBoolAttribute("invert", &invert_motor);
            sc->InvertMotor(invert_motor);

            xml->QueryIntAttribute("fuse", &fuse);
            sc_pwm->SetFuse(fuse);

            Advisory::pinfo("    creating Speed Controller on port %d, invert_m=%s", device, invert_motor?"true":"false");
        }
        else
        {
            Advisory::pinfo("    failed to create speed controller of type \"%s\"", type);
        }
    }

	return sc;
}
#endif

/*******************************************************************************
 *
 * Create an instance of a solenoid from the given XML element.
 * 
 * The XML element must have the format of
 *  
 *  <solenoid [module="module"] [port="port"] [type="type"] [...] >
 *  
 *  Where:	module	the module to which this solenoid is
 *  				connected, can be a vaule between 0 and 63, if a module is not
 *  				specified, the default value of 0 will be used
 *  				
 *  		port	the port to which this solenoid is conncected,
 *  				can be a value from 0 to 7, if not specified, the
 *  				default value of 0 is used.
 *  	
 *  	    type    Either REV or CTRE for the brand of PCM
 * 
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="shoot"
 *  				
 ******************************************************************************/
Solenoid *HardwareFactory::createSolenoid(tinyxml2::XMLElement* xml)
{
    int module = xml->IntAttribute("module");
    int port = xml->IntAttribute("port");

    if ((module < 0) || (module > 63))
	{
		Advisory::pinfo("    using default \"module\" of 0");
		module = 0;
	}
	
    if ((port < 0) || (port > 8))
    
	{
        Advisory::pinfo("    using default \"port\" of 0");
        port = 0;
	}

   // 2022 change to support both CTRE and REV PMCs/solenoids
    const char *type = xml->Attribute("type");
    frc::PneumaticsModuleType pm_type;
    if( type == nullptr || strcmp(type, "REV") == 0 || strcmp(type, "rev") == 0)
    {
        pm_type = frc::PneumaticsModuleType::REVPH;
    }
    else // DEFAULT
    {
        pm_type = frc::PneumaticsModuleType::CTREPCM;
    }

	if (module > 0)
	{
		Advisory::pinfo("    creating solenoid on module %d, port %d", module, port);
		return new Solenoid(module, pm_type, port);
	}
	else
	{
		Advisory::pinfo("    creating solenoid on port %d", port);
		return new Solenoid(pm_type, port);
	}
}

/*******************************************************************************
 *
 * Create an instance of a double solenoid from the given XML element.
 *
 * The XML element must have the format of
 *
 *  <double_solenoid [module="module"] [port_a="port"] [port_b="port"] [type="type"] [...] >
 *
 *  Where:	module	the module to which both sides of the solenoid are
 *  				connected, can be a vaule of 0 to 63, if a module is not
 *  				specified, the default value of 0 will be used
 *
 *  		port_a	the port to which the first solenoid is conncected
 *                  if not specified the default value of 0 will be used
 *
 *  		port_b	the port to which the second solenoid is connected
 *                  if not specified the default value of 1 will be used
 *
 *  	    type    Either REV or CTRE for the brand of PCM
 *
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="shoot"
 *
 ******************************************************************************/
DoubleSolenoid *HardwareFactory::createDoubleSolenoid(tinyxml2::XMLElement* xml)
{
	int module = 0;
	int port_a = 0;
	int port_b = 1;

	xml->QueryIntAttribute("module", &module);
	xml->QueryIntAttribute("port_a", &port_a);
	xml->QueryIntAttribute("port_b", &port_b);

	if ((module < 0) || (module > 63))
	{
		Advisory::pinfo("    using default \"module\" of 0");
		module = 0;
	}

	if ((port_a < 0) || (port_a > 7))
	{
		Advisory::pinfo("    using default \"port_a\" of 0");
		port_a = 0;
	}

	if ((port_b < 0) || (port_b > 7))
	{
		Advisory::pinfo("    using default \"port_b\" of 1");
		port_b = 1;
	}

    // 2022 change to support both CTRE and REV PMCs/solenoids
    const char *type = xml->Attribute("type");
    frc::PneumaticsModuleType pm_type;
    if( type == nullptr || strcmp(type, "REV") == 0 || strcmp(type, "rev") == 0)
    {
        pm_type = frc::PneumaticsModuleType::REVPH;
    }
    else // DEFAULT
    {
        pm_type = frc::PneumaticsModuleType::CTREPCM;
    }

    if (module > 0)
    {
        Advisory::pinfo("    creating double solenoid on module %d, ports %d, %d", module, port_a, port_b);
        return new DoubleSolenoid(module, pm_type, port_a, port_b);
    }
    else
    {
        Advisory::pinfo("    creating double solenoid on ports %d, %d", port_a, port_b);
        return new DoubleSolenoid(pm_type, port_a, port_b);
    }
}

/*******************************************************************************
 *
 * Create a encoder
 * 
 ******************************************************************************/
rfh::Encoder* HardwareFactory::createEncoder(tinyxml2::XMLElement* xml)
{
    const char *type = xml->Attribute("type");  // could be SPI or analog

    if (strcmp(type, "cancoder") == 0) 
    {
        return(createCancoder(xml));
    }
    // else if (strcmp(type, "optical") == 0)
    // {
    //     return(createOpticaalEncoder(xml));
    // }
    else
    {
        Advisory::pinfo("unsupported encoder type %s", type);
    }

    return nullptr;
}


/*******************************************************************************
 *
 * Create a encoder
 * 
 * The XML element must have the format of
 *  
 *  <encoder [module="1"] [port_a="1"] [port_b="2"] [invert="false"] />
 *  
 *  Where:	module	the module (or card) to which this speed controller is 
 *  				connected, can be a vaule of 1 or 2, if a module is not 
 *  				specified, the default value of 1 will be used (FRC_CRIO Only)
 *  				
 *  		port_a	one of the digital input ports to which this encoder is 
 *                  conncected, can be a value from 1 to 14, if not specified, 
 *                  the default value of 1 is used.
 *  				
 *  		port_b	one of the digital input ports to which this encoder is 
 *                  conncected, can be a value from 1 to 14, if not specified, 
 *                  the default value of 2 is used.
 * 
 *   		invert	if true, the inputs will be reversed in software to
 *                  change the direction the encoder counts, default is false
 *  				
 ******************************************************************************/
Encoder *HardwareFactory::createOpticalEncoder(tinyxml2::XMLElement* xml)
{
	Encoder *encoder;
	
	int port_a	= xml->IntAttribute("port_a");
	if ((port_a < 0) || (port_a > 20))
	{
		Advisory::pinfo("    using default \"port_a\" of 1");
		port_a = 1;
	}
	
	int port_b  = xml->IntAttribute("port_b");
	if ((port_b < 0) || (port_b > 20))
	{
		Advisory::pinfo("    using default \"port_b\" of 2");
		port_b = 2;
	}
	
	bool invert = xml->BoolAttribute("invert");

	float scale = xml->FloatAttribute("scale");
	if (scale == 0.0)
	{
		Advisory::pinfo("    using default \"scale\" of 0.01");
		scale = 0.01;	
	}
	
	Advisory::pinfo("    creating encoder on port_a %d, port_b %d, escale=%f, invert %s",
			port_a, port_b, scale, invert?"true":"false");

	encoder = new Encoder(port_a, port_b, invert);
	
	encoder->SetDistancePerPulse(scale);

	return encoder;
}

/*******************************************************************************
 *
 * Create a cancoder
 * 
 * The XML element must have the format of
 * TODO write readme
 * TODO move all initialization values from XML to local variables in rfh::cancoder.
 * Then, during an initialization routine, pass the parsed values into the Phoenix::CANcoder,
 * block with timeouts, and read error status back. THAT WAY, errors are detected and logged
 * a priori.
 * FIXME finish function
 ******************************************************************************/
rfh::Cancoder * HardwareFactory::createCancoder(tinyxml2::XMLElement *xml)
{
    rfh::Cancoder *cc = nullptr;

    int can_id = -1;
 	xml->QueryIntAttribute("device_id", &can_id);
    if ( can_id < 0 )
	{
	    xml->QueryIntAttribute("port", &can_id);
        if ( can_id < 0 )
        {
            can_id = 1;
        }
	}

    const char *can_bus = xml->Attribute("canbus");
    if (can_bus == nullptr)
    {
        can_bus = "rio";
    }
    Advisory::pinfo("    cancoder on canbus %s, device_id %d", can_bus, can_id);
    cc = new rfh::Cancoder(can_id, can_bus);

    cc->setCountsPerRotation(1.0);
    
    float scale = 360.0;
    xml->QueryFloatAttribute("scale", &scale);
    Advisory::pinfo("    cancoder scale: %f", scale);
    cc->setScale(scale);
    
    float offset = 0.0;
    xml->QueryFloatAttribute("offset", &offset);
    Advisory::pinfo("    cancoder offset: %f ", offset);
    cc->setOffset(offset);
    
    bool invert = false;
    xml->QueryBoolAttribute("invert", &invert);
    cc->setInvertDirection(invert);
    Advisory::pinfo("    cancoder invert = %s", invert?"true":"false");

    return(cc);
}

/*******************************************************************************
 *
 * Create a RAbsPosSensot
 *
 * The XML element must have the format of
 *
 *  <aps [port="port"] [raw1="0.0"] [calc1="0.0"] [raw2="5.0"] [calc2="5.0"]
 *       [wraps="0"] [raw_range="5.0"] [...] / >
 *
 *  Where:	port	the port to which this speed controller is conncected,
 *  				can be a value from 1 to 10, if not specified, the
 *  				default value of 1 is used.
 *
 *			p1_raw	the raw value at a given point, defaults to 0.0
 *
 *			p1_cal	what the calculated value should be at the raw1 point,
 *					defaults to 0.0
 *
 *			p2_raw	the raw value at a second point, defaults to 5.0
 *
 *			p2_cal	what the calculated value should be at the raw2 point,
 *					defaults to 5.0
 *
 *			wraps	the number of wraps of the sensor between raw1 and raw2,
 *					defaults to 0
 *
 *			raw_range	the range of the raw value, defaults to 5.0
 *
 ******************************************************************************/
AbsPosSensor *HardwareFactory::createAbsPosSensor(tinyxml2::XMLElement* xml)
{
    AbsPosSensor *aps;

	int port	= xml->IntAttribute("port");
	if ((port < 0) || (port > 20))
	{
		// don't default to 1, 1 must be used by gyros
		Advisory::pinfo("    using default \"port\" of 2");
		port = 2;
	}

	Advisory::pinfo("    creating APS on port %d", port);
    aps = new AbsPosSensor((uint32_t)port);

	if (aps != NULL)
	{
		float raw1 	= 0.0;
		float raw2  = 5.0;
		float calc1 = 0.0;
		float calc2 = 5.0;
		float wraps = 0.0;

		float scale = 1.0;
		float offset = 0.0;
		float raw_range = aps->getRawRange();

		xml->QueryFloatAttribute("p1_raw", &raw1);
		xml->QueryFloatAttribute("p2_raw", &raw2);
		xml->QueryFloatAttribute("p1_cal", &calc1);
		xml->QueryFloatAttribute("p2_cal", &calc2);
		xml->QueryFloatAttribute("wraps", &wraps);
		xml->QueryFloatAttribute("raw_range", &raw_range);

		scale	= (calc2 - calc1)/((raw2 - raw1) + (wraps * raw_range));
		offset 	= calc1-raw1*scale;

		aps->setScale(scale);
		aps->setOffset(offset);
		aps->setRawRange(raw_range);
	}

	return aps;
}

/*******************************************************************************
 *
 * Create a gyro class, either "spi" or "analog"
 *
 ******************************************************************************/
rfh::Gyro* HardwareFactory::createGyro(tinyxml2::XMLElement* xml)
{
    const char *type = xml->Attribute("type");  // could be SPI or analog

    if(strcmp(type, "analog") == 0)  // create analog gyro
    {
        return(createGyroAnalog(xml));
    }
    else if(strcmp(type, "spi") == 0)  // create SPI gyro
    {
        Advisory::pinfo("  creating GyroSpi");
        return(createGyroSpi(xml));
    }
    else if(strcmp(type, "NavX") == 0)  // create NavX gyro
    {
        Advisory::pinfo("  creating GyroNavX");
        return(createGyroNavX(xml));
    }
    else if(strcmp(type, "Pigeon") == 0)  // create Pigeon gyro
    {
        Advisory::pinfo("  creating Pigeon");
        return(createGyroPigeon(xml));
    }
    else if(strcmp(type, "ADXRS450") == 0)  // create SPI gyro
    {
        Advisory::pinfo("  creating GyroADXRS450");
        return(createGyroADXRS450(xml));
    }
    else
    {
        Advisory::pinfo("unsupported gyro type %s", type);
    }

    return(nullptr);
}

/*******************************************************************************
 *
 * Create an analog gyro
 * 
 * The XML element must have the format of
 *  
 *  <gyro [module="1"] [port="1"] [sensitivity="0.007"] />
 *  
 *  Where:	module	the module (or card) to which this speed controller is 
 *  				connected, can be a vaule of 1 or 2, if a module is not 
 *  				specified, the default value of 1 will be used
 *  				
 *  		port	the port to which this speed controller is conncected,
 *  				can be a value from 1 to 10, if not specified, the
 *  				default value of 1 is used.
 *  				
 *  		sensitivity	the sensitivity that will be applied to the raw gyro value
 *  				
 ******************************************************************************/
rfh::Gyro_Analog *HardwareFactory::createGyroAnalog(tinyxml2::XMLElement* xml)
{
    rfh::Gyro_Analog *gyro;
		
	int port	= xml->IntAttribute("port");
    if ((port < 0) || (port > 2))
	{
        Advisory::pinfo("    using default \"port\" of 0");
        port = 0;
	}

	Advisory::pinfo("    creating gyro on port %d", port);
    gyro = new rfh::Gyro_Analog(port);
	
	if (gyro != NULL)
	{
		float sensitivity = xml->FloatAttribute("sensitivity");
		if (sensitivity == 0.0)
		{
			Advisory::pinfo("    using default \"sensitivity\" of 0.007");
			gyro->setSensitivity(0.007);
		}
		else
		{
			gyro->setSensitivity(sensitivity);
		}
	}

	return gyro;
}

/*******************************************************************************
 *
 * create an spi gyro
 *
 ******************************************************************************/
rfh::Gyro_Spi* HardwareFactory::createGyroSpi(tinyxml2::XMLElement* xml)
{
    frc::SPI::Port port=frc::SPI::kMXP;
    int spi_cmd = 0x20000000u;
    int xfer_size = 4;
    int valid_mask = 0x0c00000eu; // ADXRS450 is 0x0c000000u
    int valid_value = 0x04000000u;
    int data_shift = 10u;
    int data_size = 16u;
    bool is_signed = true;
    bool big_endian = true;
    bool invert = false;

    const char *port_name = xml->Attribute("port");
         if (strcmp(port_name, "MXP") == 0) port = SPI::kMXP;
    else if (strcmp(port_name, "CS0") == 0) port = SPI::kOnboardCS0;
    else if (strcmp(port_name, "CS1") == 0) port = SPI::kOnboardCS1;
    else if (strcmp(port_name, "CS2") == 0) port = SPI::kOnboardCS2;
    else if (strcmp(port_name, "CS3") == 0) port = SPI::kOnboardCS3;

    xml->QueryIntAttribute("spi_cmd", &spi_cmd);
    xml->QueryIntAttribute("xfer_size", &xfer_size);
    xml->QueryIntAttribute("valid_mask", &valid_mask);
    xml->QueryIntAttribute("valid_value", &valid_value);
    xml->QueryIntAttribute("data_shift", &data_shift);
    xml->QueryIntAttribute("data_size", &data_size);

    xml->QueryBoolAttribute("is_signed", &is_signed);
    xml->QueryBoolAttribute("big_endian", &big_endian);
    xml->QueryBoolAttribute("invert", &invert);

    rfh::Gyro_Spi *rg = new rfh::Gyro_Spi(port, (uint32_t)spi_cmd,
        (uint8_t)xfer_size, (uint32_t)valid_mask, (uint32_t)valid_value,
        (uint8_t)data_shift, (uint8_t)data_size, is_signed, big_endian, invert);

    Advisory::pinfo("  created GyroSpi");
    return(rg);
}

/*******************************************************************************
 *
 * create a navx gyro
 *
 ******************************************************************************/
rfh::Gyro_NavX* HardwareFactory::createGyroNavX(tinyxml2::XMLElement* xml)
{
    bool is_micro = false;
    xml->QueryBoolAttribute("is_micro", &is_micro);

    const char *port_name = xml->Attribute("port");    
    if (is_micro)
    {
        frc::I2C::Port port = frc::I2C::kOnboard;

        if (strcmp(port_name, "Onboard") == 0) port = frc::I2C::kOnboard;

        rfh::Gyro_NavX *rg = new rfh::Gyro_NavX(port, is_micro);
        Advisory::pinfo("  created GyroNavX Micro");
        return(rg);
    }
    else
    {
        frc::SPI::Port port = frc::SPI::kMXP;

        if (strcmp(port_name, "MXP") == 0) port = frc::SPI::kMXP;
        else if (strcmp(port_name, "CS0") == 0) port = frc::SPI::kOnboardCS0;
        else if (strcmp(port_name, "CS1") == 0) port = frc::SPI::kOnboardCS1;
        else if (strcmp(port_name, "CS2") == 0) port = frc::SPI::kOnboardCS2;
        else if (strcmp(port_name, "CS3") == 0) port = frc::SPI::kOnboardCS3;

        rfh::Gyro_NavX *rg = new rfh::Gyro_NavX(port);   
        Advisory::pinfo("  created GyroNavX Regular");
        return(rg); 
    }
}



/*******************************************************************************
 *
 * create a pigeon gyro
 *
 ******************************************************************************/
rfh::Gyro_Pigeon* HardwareFactory::createGyroPigeon(tinyxml2::XMLElement* xml)
{
    int device_id = -1;

    xml->QueryIntAttribute("device_id", &device_id);
    if ((device_id < 0) || (device_id > 63))
    {
        xml->QueryIntAttribute("port", &device_id);
        if ((device_id < 0) || (device_id > 63))
        {
            device_id = 1;
        }
    }
    
    std::string canBusName  = "rio";
    const char *canbus = xml->Attribute("canbus");
    if (canbus != nullptr && strcmp(canbus, "RIO") && strcmp(canbus, "rio"))
    {
        canBusName  = canbus;
        Advisory::pinfo("canbusname = %s!!!", canBusName.c_str());
    }

    rfh::Gyro_Pigeon *rg = new rfh::Gyro_Pigeon(device_id, canBusName);

    Advisory::pinfo("  created GyroPigeon");
    return(rg);
}
/*******************************************************************************
 *
 * create an ADXRS450 gyro
 *
 ******************************************************************************/
rfh::Gyro_ADXRS450* HardwareFactory::createGyroADXRS450(tinyxml2::XMLElement* xml)
{
    frc::SPI::Port port=frc::SPI::kOnboardCS0;
    bool invert = false;

    const char *port_name = xml->Attribute("port");
    if      (strcmp(port_name, "MXP") == 0) port = SPI::kMXP;
    else if (strcmp(port_name, "CS0") == 0) port = SPI::kOnboardCS0;
    else if (strcmp(port_name, "CS1") == 0) port = SPI::kOnboardCS1;
    else if (strcmp(port_name, "CS2") == 0) port = SPI::kOnboardCS2;
    else if (strcmp(port_name, "CS3") == 0) port = SPI::kOnboardCS3;

    xml->QueryBoolAttribute("invert", &invert);

    rfh::Gyro_ADXRS450 *rg = new rfh::Gyro_ADXRS450(port, invert);

    Advisory::pinfo("  created GyroADXRS450");
    return(rg);
}

/*******************************************************************************
 *
 * Create a RpmCounter
 * 
 * The XML element must have the format of
 *  
 *  <counter [module="1"] [port="3"] [count_per_rev="8"] 
 *  	[filter_coeff="0.9"] [...] >
 *  
 *  Where:	module	the module (or card) to which this speed controller is 
 *  				connected, can be a vaule of 1 or 2, if a module is not 
 *  				specified, the default value of 1 will be used
 *  				
 *  		port	the port to which this speed controller is conncected,
 *  				can be a value from 1 to 10, if not specified, the
 *  				default value of 1 is used.
 *  				
 *  		count_per_rev	how many times the counter will increment is 
 *  						a single rotation of the output shaft
 *  						
 *  		filter_coeff	the coefficient that will be used to smooth
 *  						the output
 *  						
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="shoot"
 *  				
 ******************************************************************************/
RpmCounter *HardwareFactory::createRpmCounter(tinyxml2::XMLElement* xml)
{
    RpmCounter *counter;
	
	int port	= xml->IntAttribute("port");
	if ((port < 0) || (port > 20))
	{
		// don't default to 1, 1 must be used by gyros
		Advisory::pinfo("    using default \"port\" of 2");
		port = 2;
	}
	
	Advisory::pinfo("    creating counter on port %d", port);
    counter = new RpmCounter((uint32_t)port);
	
	if (counter != NULL)
	{
		int cnt_per_rev		= xml->IntAttribute("count_per_rev");
		float fltr_coeff	= xml->FloatAttribute("filter_coeff");

		if (cnt_per_rev < 1)
		{
			cnt_per_rev = 1;
			Advisory::pinfo("    using default \"count_per_rev\" of 1");
		}
		counter->setCountPerRev(cnt_per_rev);
		
		if (fltr_coeff == 0.0)
		{
			fltr_coeff = 0.5;
			Advisory::pinfo("    using default \"filter_coeff\" of %f", fltr_coeff);
		}
		counter->setFilterCoeff(fltr_coeff);
	}

	return counter;
}

/*******************************************************************************
 *
 * Create an instance of a Relay from the given XML element.
 * 
 * The XML element must have the format of
 *  
 *  <Relay [module="module"] [port="port"] [...] >
 *  
 *  Where:	module	the module (or card) to which this Relay is 
 *  				connected, can be a vaule of 1 or 2, if a module is not 
 *  				specified, the default value of 1 will be used (FRC_CRIO Only)
 *  		port	the port to which this Relay is conncected,
 *  				can be a value from 1 to 10, if not specified, the
 *  				default value of 1 is used.
 *  	
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="shoot"
 *  				
 ******************************************************************************/
Relay *HardwareFactory::createRelay(tinyxml2::XMLElement* xml)
{
	int port			= xml->IntAttribute("port");
	if ((port < 0) || (port > 20))
	{
		Advisory::pinfo("    using default \"port\" of 1");
		port = 1;
	}

	Advisory::pinfo("    creating relay on port %d", port);
	return new Relay(port);
}

/*******************************************************************************
 *
 * Create an instance of a Servo from the given XML element.
 * 
 * The XML element must have the format of
 *  
 *  <Servo [module="module"] [port="port"] [...] >
 *  
 *  Where:	module	the module (or card) to which this Servo is 
 *  				connected, can be a vaule of 1 or 2, if a module is not 
 *  				specified, the default value of 1 will be used (FRC_CRIO only)
 *  				
 *  		port	the port to which this Servo is conncected,
 *  				can be a value from 1 to 10, if not specified, the
 *  				default value of 1 is used.
 *  	
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="shoot"
 *  				
 ******************************************************************************/
Servo *HardwareFactory::createServo(tinyxml2::XMLElement* xml)
{
	int port			= xml->IntAttribute("port");
	if ((port < 0) || (port > 20))
	{
		Advisory::pinfo("    using default \"port\" of 1");
		port = 1;
	}

	Advisory::pinfo("    creating servo on port %d", port);
	return new Servo(port);
}


/*******************************************************************************
 *
 * Create an instance of a Limit Switch from the given XML element.
 *
 * @param	xml	the xml that defines the digital input.
 * 
 * The XML element must have the format of
 *  
 *  <digital_input [port="port"] [normally_open="true"] [...] >
 *  
 *  Where:	port	the port to which this Limit Switch is connected,
 *  				can be a value from 0 to ??, if not specified, the
 *  				default value of 0 is used.
 *  				
 *  		normally_open	true if the connected switch is normally open
 *  						false if it is normally closed
 *  	
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="upper_limit"
 *  				
 *  @return	a pointer to a digital input object, the calling class is
 *  		responsible for deleting the returned object.
 *
 ******************************************************************************/
LimitSwitch *HardwareFactory::createLimitSwitch(tinyxml2::XMLElement* xml)
{
    int16_t port = 0;
    LimitSwitch::InputMode mode = LimitSwitch::INPUT_NORMALLY_OPEN;
    bool reset = false;
    double reset_value = 0.0;

    parseLimitSwitch(xml, port, mode, reset, reset_value);

	if (port < 0)
	{
		Advisory::pinfo("    using default \"port\"");
        port = 0;
	}

    Advisory::pinfo("    creating limit switch: port=%d, mode=%s, reset=%d, reset_value=%8.6f",
       port, (mode==LimitSwitch::INPUT_NORMALLY_OPEN ? "normally open" : "normally closed"),
       reset, reset_value);

    return new LimitSwitch(port, mode, reset, reset_value);
}

/*******************************************************************************
 *
 ******************************************************************************/
void HardwareFactory::parseLimitSwitch(tinyxml2::XMLElement* xml, int16_t& port,
    LimitSwitch::InputMode& mode, bool& reset, double& reset_value)
{
    bool normally_open = (mode == LimitSwitch::INPUT_NORMALLY_OPEN);
    int temp_port = port;

    xml->QueryIntAttribute("port", &temp_port);
    xml->QueryBoolAttribute("normally_open", &normally_open);
    xml->QueryBoolAttribute("reset", &reset);
    xml->QueryDoubleAttribute("reset_value", &reset_value);

    port = temp_port;
    mode = normally_open?LimitSwitch::INPUT_NORMALLY_OPEN:LimitSwitch::INPUT_NORMALLY_CLOSED;
}

/*******************************************************************************
 *
 ******************************************************************************/
DigitalInput *HardwareFactory::createDigitalInput(tinyxml2::XMLElement* xml)
{
	int port = 1;
	xml->QueryIntAttribute("port", &port);

	if (port < 0)
	{
		Advisory::pinfo("    using default \"port\"");
		port = 1;
	}

	Advisory::pinfo("    creating digital input on port %d", port);

	DigitalInput *rdo = new DigitalInput(port);
	if (nullptr == rdo)
	{
		Advisory::pinfo("ERROR: failed to create digital input");
	}

	return rdo;
}

/*******************************************************************************
 *
 ******************************************************************************/
DigitalOutput *HardwareFactory::createDigitalOutput(tinyxml2::XMLElement* xml)
{
	int port = 1;
	xml->QueryIntAttribute("port", &port);

	if (port < 0)
	{
		Advisory::pinfo("    using default \"port\"");
		port = 1;
	}

	Advisory::pinfo("    creating digital output on port %d", port);

	DigitalOutput *rdo = new DigitalOutput(port);
	if (nullptr == rdo)
	{
		Advisory::pinfo("ERROR: failed to create digital output");
	}

	return rdo;
}

/*******************************************************************************
 *
 * Create an instance of a Scaled Analog Input from the given XML element.
 *
 * @param	xml	the xml that defines the scaled analog input.
 *
 * The XML element must have the format of
 *
 *  <scaled_analog [port="port"]
 *  	[p1_raw="0.0"] [p1_cal="0.0"] [p2_raw="1.0"] [p2_cal="1.0"]
 *      [min_raw="min_raw"] [max_raw="max_raw"] [...] >
 *
 *  Where:	port	the port to which this analog device is conncected,
 *  				can be a value from 0 to 9, if not specified, the
 *  				default value of 0 is used.
 *
 *          p1_raw  The raw value at an easily identifiable point (p1)
 *          p1_cal  the calibrated value at an easily identifiable point (p1)
 *          p2_raw  The raw value at an easily identifiable point (p2)
 *          p2_cal  the calibrated value at an easily identifiable point (p2)
 *
 *                  The points p1 and p2 will be used to calculate the
 *                  slope and offset for this analog input
 *
 *  		min_raw	the minimum raw value that will be considered as
 *  				valid, if the raw value is less than this, isReady()
 *  				will return false.  This is to help detect failed
 *  				sensors.
 *
 *  		max_raw	the maximum raw value that will be considered as
 *  				valid, if the raw value is greater than this, isReady()
 *  				will return false.  This is to help detect failed
 *  				sensors.
 *
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="shoot"
 *
 *  @return	a pointer to a scaled analog input object, the calling class is
 *  		responsible for deleting the returned object.
 *
 ******************************************************************************/
ScaledAnalogInput *HardwareFactory::createScaledAnalogInput(tinyxml2::XMLElement* xml)
{
    ScaledAnalogInput *sa;

	int port	= 0;

	xml->QueryIntAttribute("port", &port);

	Advisory::pinfo("    creating Scaled Analog on port %d", port);
	sa = new ScaledAnalogInput(port);

	if (sa != NULL)
	{
        double scale = 1.0;
        double offset = 0.0;
        double min_raw = 0.05;
        double max_raw = 0.95;

        parseScaledAnalogInput(xml, scale, offset, min_raw, max_raw);

		sa->setScale(scale);
		sa->setOffset(offset);

		sa->setMinimumRawValue(min_raw);
		sa->setMaximumRawValue(max_raw);
	}

	return sa;
}

/*******************************************************************************
 *
 ******************************************************************************/
void HardwareFactory::parseScaledAnalogInput(tinyxml2::XMLElement* xml,
    double& scale, double& offset, double& min_raw, double& max_raw)
{
    double p1_raw = 0.0;
    double p2_raw = 1.0;
    double p1_cal = 0.0;
    double p2_cal = 1.0;

    xml->QueryDoubleAttribute("min_raw", &min_raw);
    xml->QueryDoubleAttribute("max_raw", &max_raw);
    xml->QueryDoubleAttribute("p1_raw", &p1_raw);
    xml->QueryDoubleAttribute("p2_raw", &p2_raw);
    xml->QueryDoubleAttribute("p1_cal", &p1_cal);
    xml->QueryDoubleAttribute("p2_cal", &p2_cal);

    scale	= (p2_cal - p1_cal)/(p2_raw - p1_raw);
    offset 	= p1_cal-p1_raw*scale;
}

/*******************************************************************************
 *
 ******************************************************************************/
AnalogIrSensor *HardwareFactory::createAnalogIrSensor(tinyxml2::XMLElement* xml)
{
	int channel = xml->IntAttribute("channel");
	int threshold = DEFAULT_THRESHOLD_VALUE;
	int cycles = DEFAULT_LATCH_CYLES;
    AnalogIrSensor::ThresholdType threshold_type = AnalogIrSensor::kLessThan;
	const char *type 	= xml->Attribute("threshold_type");

    AnalogIrSensor *ir = nullptr;

	if(channel >= 0)
	{
        ir = new AnalogIrSensor(channel);
	}

	xml->QueryIntAttribute("latch_cycles", &cycles);
	xml->QueryIntAttribute("threshold", &threshold);

	if(nullptr != type)
	{
		if(0 == strcmp(type, "greater_than"))
		{
            threshold_type = AnalogIrSensor::kGreaterThan;
		}
	}

	if(nullptr != ir)
	{
		ir->setLatchCycles(cycles);
		ir->setThreshold(threshold);
		ir->setThresholdType(threshold_type);
	}
	return(ir);
}


/*******************************************************************************
 *
 ******************************************************************************/
Limelight *HardwareFactory::createLimelight(tinyxml2::XMLElement* xml)
{

 Limelight *light = new Limelight();
 return light;
}


/*******************************************************************************
 *
 ******************************************************************************/
 LightsStrip *HardwareFactory::createLightsStrip(tinyxml2::XMLElement* xml)
 {
    int port = 0;
    int length = 30;
    xml->QueryIntAttribute("length", &length);
    xml->QueryIntAttribute("port", &port);
    LightsStrip *lights = new LightsStrip(length, port);
    Advisory::pinfo("lights strip created on port %d with length %d", port, length);
    return lights;
 }

// ##############################################################################
// ##############################################################################
// ##############################################################################
// ##############################################################################
// ##############################################################################
// ##############################################################################
// ##############################################################################

/*******************************************************************************
 *
 * Create a washout filter for a command (WashoutCommand
 *
 * The XML element must have the format of
 *
    <washout name="drive_washout" coefficient="coeff", decay_percent="decay%" />
 *
 *  Where:	coefficient  decay coefficient (dependent on rate)
 *  		decay_percent is the amount to decay as percent of full command
 *
 ******************************************************************************/
#if 0 //@TODO: FIX_THIS
WashoutCommand *HardwareFactory::createWashout(tinyxml2::XMLElement* xml)
{
    WashoutCommand *wash;

    Advisory::pinfo("    creating washout commander");
    wash = new WashoutCommand();

    if (wash != NULL)
    {
        float coeff		= xml->FloatAttribute("coefficient");
        float percent_decay		= xml->FloatAttribute("decay_percent");

        wash->setCoefficient(coeff);
        wash->setDecayPercent(percent_decay);

        Advisory::pinfo("      WASHOUT coeff=%f %% decay=%f",coeff, percent_decay);
    }

    return wash;
}
#endif

/*******************************************************************************
 *
 * Create a trapezoid velocity control of position (SimpleTrapCntl)
 *
 * The XML element must have the format of
 *
 *  <pid [kp="kp"] [ki="ki"] [kd="kd"]
 *  	 [vel_min="targ_min"] [vel_max="targ_max"] [vel_thp="targ_thp"]
 *  	 [accel="accel_val"] [cntl_min="cntl_min"] [cntl_max="cntl_max"]
 *  	 [...] >
 *
 *  Where:	kp		proportional constant
 *  		ki		integral constant
 *  		kd		derivative constant
 *
 *  		vel_min	minimum allowed value for the velocity
 *  		vel_max	maximum allowed value for the velocity
 *  		vel_thp	threashold percentage change
 *
 * 			accel	the slope of the velocity curve
 *
 *  		control_min	the minimum control value that should be generated
 *  		control_max	the maximum control value that should be generated
 *
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="shoot"
 *
 ******************************************************************************/
SimpleTrapCntl *HardwareFactory::createTrapCntl(tinyxml2::XMLElement* xml)
{
    SimpleTrapCntl *trap;

    Advisory::pinfo("    creating simple Trapezoid Velocity Control");
    trap = new SimpleTrapCntl();

    if (trap != NULL)
    {
        float kf 			= 0.0;
        float kp 			= 0.0;
        float ki 			= 0.0;
        float kd 			= 0.0;
        float vel_min 		= -1.0;
        float vel_max 		= 1.0;
        float vel_thp 		= 0.2;
        float accel			= 1.0;
        float control_min 	= -1.0;
        float control_max 	= 1.0;
        bool is_angle = false;

        xml->QueryFloatAttribute("kf", &kf);
        xml->QueryFloatAttribute("kp", &kp);
        xml->QueryFloatAttribute("ki", &ki);
        xml->QueryFloatAttribute("kd", &kd);
        xml->QueryFloatAttribute("vel_min", &vel_min);
        xml->QueryFloatAttribute("vel_max", &vel_max);
        xml->QueryFloatAttribute("vel_thp", &vel_thp);
        xml->QueryFloatAttribute("accel", &accel);
        xml->QueryFloatAttribute("control_min", &control_min);
        xml->QueryFloatAttribute("control_max", &control_max);
        xml->QueryBoolAttribute("is_angle", &is_angle);

        trap->setControlConstants(kf, kp, ki, kd);
        trap->setVelocityLimits(vel_min, vel_max, vel_thp);
        // trap->setVelocityLimits(vel_min, vel_max, 0.20);
        trap->setAccelerationLimits(accel);
        trap->setControlValueLimits(control_min, control_max);
        trap->setIsAngle(is_angle);

        Advisory::pinfo("      TRAP Kf=%f, Kp=%f, Ki=%f, Kd=%f, tmn=%f, tmx=%f, acc= %f, thp=%f, cmn=%f, cmx=%f, angle=%s",
                kf, kp, ki, kd, vel_min, vel_max, accel, vel_thp, control_min, control_max, is_angle?"true":"false");
    }

    return trap;
}

/*******************************************************************************
 *
 * Create a trapezoid velocity control of position (SimpleTrapCntl)
 *
 * The XML element must have the format of
 *
 *  <pid [kp="kp"] [ki="ki"] [kd="kd"]
 *  	 [vel_min="targ_min"] [vel_max="targ_max"] [vel_thp="targ_thp"]
 *  	 [accel="accel_val"] [cntl_min="cntl_min"] [cntl_max="cntl_max"]
 *  	 [...] >
 *
 *  Where:	kp		proportional constant
 *  		ki		integral constant
 *  		kd		dirivitive constant
 *
 *  		vel_min	minimum allowed value for the velocity
 *  		vel_max	maximum allowed value for the velocity
 *  		vel_thp	threashold perentage change
 *
 * 			accel	the slope of the velocity curve
 *
 *  		cntl_min	the minimum control value that should be generated
 *  		cntl_max	the maximum control value that should be generated
 *
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="shoot"
 *
 ******************************************************************************/
#if 0 //@TODO: FIX_THIS
TrapezoidalProfile *HardwareFactory::createTrapezoid(tinyxml2::XMLElement* xml)
{
    TrapezoidalProfile *trap;

    Advisory::pinfo("    creating trapezoidal profile");
    trap = new TrapezoidalProfile();

    if (trap != NULL)
    {
        float max_vel		= xml->FloatAttribute("max_vel");
        float percent_accel		= xml->FloatAttribute("percent_accel");
        float delta_time		= xml->FloatAttribute("delta_time");

        trap->setMaxVelocity(max_vel);
        trap->setPercentAcc(percent_accel);
        trap->setDeltaTime(delta_time);

        Advisory::pinfo("      TRAP max_v=%f %% accel=%f dt=%f",max_vel, percent_accel, delta_time);
    }

    return trap;
}
#endif

/*******************************************************************************
 *
 * Create a pid (BasicPID)
 *
 * The XML element must have the format of
 *
 *  <pid [kp="kp"] [ki="ki"] [kd="kd"]
 *  	 [targ_min="targ_min"] [targ_max="targ_max"] [targ_thp="targ_thp"]
 *  	 [cntl_min="cntl_min"] [cntl_max="cntl_max"]
 *  	 [...] >
 *
 *  Where:	kp		proportional constant
 *  		ki		integral constant
 *  		kd		derivative constant
 *
 *  		targ_min	minimum allowed value for the target
 *  		targ_max	maximum allowed value for the target
 *  		targ_thp	threashold percentage change
 *
 *  		cntl_min	the minimum control value that should be generated
 *  		cntl_max	the maximum control value that should be generated
 *
 *  		...		other options may be required based on use, a common
 *  				option would be something like name="shoot"
 *
 ******************************************************************************/
BasicPid *HardwareFactory::createPid(tinyxml2::XMLElement* xml)
{
    BasicPid *pid = new BasicPid();

    Advisory::pinfo("    creating BasicPid");

    if (pid != NULL)
    {
        double kf = 0.001;
        double kp = 0.0;
        double ki = 0.0;
        double kd = 0.0;
        double kz = 100000.0;

        xml->QueryDoubleAttribute("kf", &kf);
        xml->QueryDoubleAttribute("kp", &kp);
        xml->QueryDoubleAttribute("ki", &ki);
        xml->QueryDoubleAttribute("kd", &kd);
        xml->QueryDoubleAttribute("kz", &kz);

        pid->setGains(kf, kp, ki, kd, kz);

        double cntl_min	= -1.0;
        double cntl_max	= 1.0;

        xml->QueryDoubleAttribute("cntl_min", &cntl_min);
        xml->QueryDoubleAttribute("cntl_max", &cntl_max);

        pid->setCommandLimits(cntl_min, cntl_max);

        bool is_angle = false;

        xml->QueryBoolAttribute("is_angle", &is_angle);

        pid->setIsAngle(is_angle);

        Advisory::pinfo("      PID Kf=%f, Kp=%f, Ki=%f, Kd=%f, Kz=%f, cntl_min=%f, cntl_max=%f, is_angle=%s",
                kf, kp, ki, kd, kz, cntl_min, cntl_max, is_angle?"true":"false"	);
    }

    return pid;
}
