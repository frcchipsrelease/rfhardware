/*******************************************************************************
 *
 * File: Gyro_Pigeon.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 * 
 * References:
 * CTRE Pigeon 2.0:
 * - Communication via CAN.            
 * - Docs https://v5.docs.ctr-electronics.com/en/stable/ch11a_BringUpPigeon2.html.
 * - API https://api.ctr-electronics.com/phoenix/release/cpp/classctre_1_1phoenix_1_1sensors_1_1_w_p_i___pigeon2.html
 * 
 ******************************************************************************/
#include "rfhardware/Gyro_Pigeon.h"

#include "gsutilities/Advisory.h"

//#include "frc/DriverStation.h"
//#include "frc/livewindow/LiveWindow.h"
//#include "frc/Timer.h"

static constexpr double kSamplePeriod = 0.001;
#ifdef WIN32
static constexpr double kCalibrationSampleTime = 5.0;
#else
static units::second_t kCalibrationSampleTime = 5.0_s;
#endif

static constexpr double kDegreePerSecondPerLSB = 0.0125;

static constexpr uint8_t kRateRegister = 0x00;
static constexpr uint8_t kTemRegister = 0x02;
static constexpr uint8_t kLoCSTRegister = 0x04;
static constexpr uint8_t kHiCSTRegister = 0x06;
static constexpr uint8_t kQuadRegister = 0x08;
static constexpr uint8_t kFaultRegister = 0x0A;
static constexpr uint8_t kPIDRegister = 0x0C;
static constexpr uint8_t kSNHighRegister = 0x0E;
static constexpr uint8_t kSNLowRegister = 0x10;

using namespace frc;

namespace rfh
{

/*******************************************************************************
 *
 * Gyro constructor with the specified device ID.
 *
 * @param device_id The device ID of the Pigeon.
 *
 ******************************************************************************/
Gyro_Pigeon::Gyro_Pigeon(int device_id, std::string canbus)
    : Gyro()
{
#if (PHOENIX_VERSION == 6)
    pigeon = new ctre::phoenix6::hardware::Pigeon2(device_id, canbus);
#else
    pigeon = new WPI_Pigeon2(device_id, canbus);
#endif
    
    m_is_ready = true;
}


/*******************************************************************************
 *
 ******************************************************************************/
bool Gyro_Pigeon::isReady() const
{
    return( m_is_ready);
}

/*******************************************************************************
 *
 * Initialize the gyro.
 * Calibrate the gyro by running for a number of samples and computing the
 * center value.
 * Then use the center value as the Accumulator center value for subsequent
 * measurements.
 * It's important to make sure that the robot is not moving while the centering
 * calculations are in progress, this is typically done when the robot is first
 * turned on while it's sitting at rest before the competition starts.
 *
 ******************************************************************************/
void Gyro_Pigeon::calibrate()
{

}

/*******************************************************************************
 *
 ******************************************************************************/
// static bool CalcParity(uint32_t v)
// {
//     bool parity = false;

//     return parity;
// }


/*******************************************************************************
 *
 * Reset the gyro.
 * Resets the gyro to a heading of zero. This can be used if there is
 * significant
 * drift in the gyro and it needs to be recalibrated after it has been running.
 *
 ******************************************************************************/
void Gyro_Pigeon::reset()
{
       pigeon->Reset();
}

/*******************************************************************************
 *
 * Return the actual angle in degrees that the robot is currently facing.
 *
 * The angle is based on the current accumulator value corrected by the
 * oversampling rate, the
 * gyro type and the A/D calibration values.
 * The angle is continuous, that is it will continue from 360->361 degrees. This
 * allows algorithms that wouldn't
 * want to see a discontinuity in the gyro output as it sweeps from 360 to 0 on
 * the second time around.
 *
 * @return the current heading of the robot in degrees. This heading is based on
 * integration
 * of the returned rate from the gyro.
 *
 ******************************************************************************/
double Gyro_Pigeon::getAngle() const
{
    return pigeon->GetAngle(); // cw +
}

/*******************************************************************************
 *
 * Return the actual angle in degrees that the robot is currently facing as a
 * Rotation2d object.
 *
 * The angle is based on the current accumulator value corrected by the
 * oversampling rate, the
 * gyro type and the A/D calibration values.
 * The angle is continuous, that is it will continue from 360->361 degrees. This
 * allows algorithms that wouldn't
 * want to see a discontinuity in the gyro output as it sweeps from 360 to 0 on
 * the second time around.
 *
 * @return the current heading of the robot in degrees as a Rotation2d object.
 * This heading is based on integration
 * of the returned rate from the gyro.
 *
 ******************************************************************************/
frc::Rotation2d Gyro_Pigeon::getRotation2d() const
{
    return pigeon->GetRotation2d(); // ccw +
}

/*******************************************************************************
 *
 * Return the rate of rotation of the gyro
 *
 * The rate is based on the most recent reading of the gyro analog value
 *
 * @return the current rate in degrees per second
 *
 ******************************************************************************/
double Gyro_Pigeon::getRate() const
{
    return pigeon->GetRate();
}

/*******************************************************************************
 *
 ******************************************************************************/
double Gyro_Pigeon::getPitch() const
{
#if (PHOENIX_VERSION == 6)
    return pigeon->GetPitch().GetValue().value();
#else
    return static_cast<double>(pigeon->GetPitch());
#endif
}

/*******************************************************************************
 *
 ******************************************************************************/
double Gyro_Pigeon::getRoll() const
{
#if (PHOENIX_VERSION == 6)
    return pigeon->GetRoll().GetValue().value();
#else
    return static_cast<double>(pigeon->GetRoll());
#endif
}

/*******************************************************************************
 *
 ******************************************************************************/
double Gyro_Pigeon::getYaw() const
{
#if (PHOENIX_VERSION == 6)
    return pigeon->GetYaw().GetValue().value();
#else
    return static_cast<double>(pigeon->GetYaw());
#endif
}

/*******************************************************************************
 *
 ******************************************************************************/
double Gyro_Pigeon::getCompassHeading() const
{
#if (PHOENIX_VERSION == 6)
    return pigeon->GetYaw().GetValue().value();
#else
    return static_cast<double>(pigeon->GetCompassHeading());
#endif
}
 
/*******************************************************************************
 *
 ******************************************************************************/
bool Gyro_Pigeon::isMoving() const
{
    //return pigeon->IsMoving(); // jly pigeon doesn't have this function
    return false;
}
 
/*******************************************************************************
 *
 ******************************************************************************/
bool Gyro_Pigeon::isRotating() const
{
    return pigeon->GetRate() > 0.5f;
}

/*******************************************************************************
 *
 ******************************************************************************/
bool Gyro_Pigeon::isConnected() const
{
    //return pigeon->IsConnected(); // jly pigeon doesn't have this function
    return true;
}

} // end namespace