/*******************************************************************************
 *
 * File: Motor.cpp
 * 
 * This file contains the default Motor definition, this is a pure virtual
 * class that provides a default implementation for most of the methods.
 *
 * Written by:
 *  FRC Team 324, Chips
 * 	Clear Creek Independent School District FIRST Robotics
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Motor.h"

/******************************************************************************
 *
 ******************************************************************************/
Motor::Motor(void) {}
Motor::~Motor(void) {}

void Motor::setPosition(double pos) {}
double Motor::getPosition() { return 0.0; }
double Motor::getRawPosition(void) { return 0.0; }
void Motor::resetPosition(double position) {}

void Motor::setVelocity(double velocity)  {}
double Motor::getVelocity(void) { return 0.0; }
double Motor::getRawVelocity(void) { return 0.0; }

void Motor::doUpdate(void) {}

void Motor::setBrakeMode(BrakeMode brake_mode)  {}
Motor::BrakeMode Motor::getBrakeMode(void) { return BrakeMode::COAST; }

void Motor::setMotorInvert(bool invert)  {}
bool Motor::getMotorInvert(void) { return false; }

void Motor::setSensorInvert(bool invert) {}
bool Motor::getSensorInvert(void)  { return false; }
void Motor::setSensorScale(double scale, double offset) {}

void Motor::setPid(BasicPid* pid) {}

void Motor::setControlMode(ControlModeType type, int deviceID)  {}
Motor::ControlModeType Motor::getControlMode() { return ControlModeType::PERCENT; }

void Motor::setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak) {}

void Motor::setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position, double position_value) {}
void Motor::setLowerLimitSwitch(int8_t port, bool normally_open, bool reset_position, double position_value) {}

bool Motor::isUpperLimitPressed(void) { return false; }
bool Motor::isLowerLimitPressed(void) { return false; }

int Motor::getDeviceID() { return -1; }

void Motor::setCurrentLimit(uint32_t amps, uint32_t peak, uint32_t duration )  {}
void Motor::setCurrentLimitEnabled(bool enabled) {}

void Motor::setKp(double p)  {}
void Motor::setKi(double i)  {}
void Motor::setKd(double d)  {}
void Motor::setKf(double f)  {}
void Motor::setKz(double i_zone)  {}

double Motor::getKp(void) { return 0.0; }
double Motor::getKi(void) { return 0.0; }
double Motor::getKd(void) { return 0.0; }
double Motor::getKf(void) { return 0.0; }
double Motor::getKz(void) { return 0.0; }
