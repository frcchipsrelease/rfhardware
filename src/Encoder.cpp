/*******************************************************************************
 *
 * File: Encoder.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Encoder.h"

#include <math.h>

namespace rfh
{

/*******************************************************************************
 *
 * @brief create an instance of a generic encoder.
 * 
 * This class should be used as a base class for other classes that provide
 * a hardware specific implementation of any position and/or rate sensing
 * device.
 * 
 ******************************************************************************/
Encoder::Encoder(uint32_t capabilities_arg)
    : capabilities(capabilities_arg)
{
    scale = 1.0;
    offset = 0.0;
    counts_per_rotation = 1.0;
    invert_direction = false;
}

/*******************************************************************************
 * 
 * @return true if the encoder hardware is capable of the specified 
 *              argument
 * 
 * @param   capability  the capability that is being checked
 * 
 * @return true if the specified capability was included in the subclasses
 *              list of capabilities.
 *  
 ******************************************************************************/
bool Encoder::isCapableOf(rfh::Encoder::Capability capability_arg)
{
    return ((capabilities & capability_arg) == capability_arg);
}

/*******************************************************************************
 *
 * @brief   set the scale for the user units to be returned by this encoder
 * 
 *  This allows the user to create an encoder that returns the desired units.
 *  for example, if the user wishes to work in degrees, they can set the
 *  scale to 360.0 so one rotation will return 360 degrees.
 * 
 * @param   scale   the scale that should be used to convert the encoder
 *                  rotations to the user units
 * 
 ******************************************************************************/
void Encoder::setScale(float scale_arg)
{
    // just incase the offset was set first, we need to adjust the stored
    // offset for the given scale
    offset = (offset * scale) / scale_arg;

    scale = scale_arg;
}

/*******************************************************************************
 *
 * @return the scale to be used by this encoder
 * 
 ******************************************************************************/
float Encoder::getScale(void)
{
    return scale;
}

/*******************************************************************************
 *
 * @brief set the offset associated with this encoder
 * 
 * This allows the user to set some "home" position for the encoder. For 
 * example, the user may want to set half of a rotation to be the offset so
 * a single rotation sensor returns from -0.5 5o 0.5.
 * 
 * @param   offset the home positions offset in user units, if the scale is
 *                 set for degrees, an offset of -180 degrees means all 
 *                 values will be adjusted by subtracting half of a rotation
 * 
 ******************************************************************************/
void Encoder::setOffset(float offset_arg)
{
    offset = offset_arg / scale;
}

/*******************************************************************************
 *
 * @return the offset to be used by this encoder in user units
 * 
 ******************************************************************************/
float Encoder::getOffset(void)
{
    return offset * scale;
}

/*******************************************************************************
 *
 * @brief set the offset such that the current position reads the specified
 *        value
 * 
 * This allows the user to set some "home" position for the encoder based 
 * on the current location. This is useful for homeing based on a 
 * limit switch that is at a known location being pressed.
 * 
 * @param   desired_position    what the current position should be reading
 *                              in user units
 * 
 ******************************************************************************/
void Encoder::setOffsetToPosition(float desired_position)
{
    setOffset(0.0);
    setOffset(getPosition() - desired_position);
}

/*******************************************************************************
 *
 * @brief   Set the hardware counts to rotations scale
 * 
 * This will be used to convert the raw hardware values to rotations so 
 * the rest of the code is based on rotations
 * 
 * @param   counts_per_rotation     the number of counts the encoder detects
 *                                  during a single rotation, a lot of encoders
 *                                  will be a power of 2 such as 1024 or 4096,
 *                                  a three turn potentiometer will normally
 *                                  be the system voltage * 1.0/3.0
 * 
 ******************************************************************************/
void Encoder::setCountsPerRotation(float counts_per_rotation_arg)
{
    counts_per_rotation = counts_per_rotation_arg;
}

/*******************************************************************************
 *
 * @return the hardwares count_per_rotation
 * 
 ******************************************************************************/
float Encoder::getCountsPerRotation(void)
{
    return counts_per_rotation;
}

/*******************************************************************************
 *
 * @brief   set the sensor direction
 * 
 * After the sensor is mounted, move it in the direction that should be 
 * positive, if the value gets smaller, change what you are passing to this
 * function.
 * 
 * @param   clockwise   true if the movement of the sensor should invert the
 *                      hardware direction.
 * 
 ******************************************************************************/
void Encoder::setInvertDirection(bool invert)
{
    invert_direction = invert;
}

/*******************************************************************************
 *
 * @return the sensor direction
 * 
 ******************************************************************************/
bool Encoder::getInvertDirection(void)
{
    return invert_direction;
}

/*******************************************************************************
 *
 * @brief   return the raw sensor position value
 * 
 * This method should be overriden by any sensor implementation that provides
 * position to return the raw sensor count. If the sensor does not provide
 * position, this default impementation will always return 0.0
 * 
 * @return  this defaule implementation will always return 0.0
 * 
 ******************************************************************************/
float Encoder::getRawPosition(void)
{
    return 0.0;
}

/*******************************************************************************
 *
 * @return  the current sensor position in rotations adjusted by the sensor 
 *          offset in rotations
 *  
 ******************************************************************************/
float Encoder::getRotationPosition(void)
{
    return (getRawPosition() / counts_per_rotation) - offset;
}

/*******************************************************************************
 *
 * @return the current position of the sensor in user units
 * 
 ******************************************************************************/
float Encoder::getPosition(void)
{
    return getRotationPosition() * scale;
}

/*******************************************************************************
 *
 * @brief   return the absolute raw sensor position value
 * 
 * This method should be overriden by any sensor implementation that provides
 * absolute position to return the raw sensor count between 0 and counts per
 * revolution. 
 * 
 * If the sensor does not provide position, this default impementation will 
 * always return 0.0
 * 
 * @return  this defaule implementation will always return 0.0
 * 
 ******************************************************************************/
float Encoder::getAbsoluteRawPosition(void)
{
    return 0.0;
}

/*******************************************************************************
 *
 * @return  the current sensor position in absolute rotations adjusted by the 
 *          sensor offset then normalized to a value between 0.0 and 1.0 rotations
 *  
 ******************************************************************************/
float Encoder::getAbsoluteRotationPosition(void)
{
    return fmod(fmod((getAbsoluteRawPosition() / counts_per_rotation) - offset, 1.0) + 1.0, 1.0);
}

/*******************************************************************************
 *
 * @return the current absloute position of the sensor in user units
 * 
 ******************************************************************************/
float Encoder::getAbsolutePosition(void)
{
    return getAbsoluteRotationPosition() * scale;
}

/*******************************************************************************
 *
 * @return  the current sensor position in absolute rotations adjusted by the 
 *          sensor offset then normalized to a value between -0.5 and 0.5 
 *          rotations (so the center of the range is zero)
 *  
 ******************************************************************************/
float Encoder::getAbsoluteRotationCenteredPosition(void)
{
    float r = getAbsoluteRotationPosition();
    if (r > 0.5)
    {
        r -= 1.0;
    }
    return r;
}

/*******************************************************************************
 *
 * @return the current absloute position (with zero at the center of the range)
 *         of the sensor in user units
 * 
 ******************************************************************************/
float Encoder::getAbsoluteCenteredPosition(void)
{
    return getAbsoluteRotationCenteredPosition() * scale;
}

/*******************************************************************************
 *
 * @brief   return the raw sensor position value
 * 
 * This method should be overriden by any sensor implementation that provides
 * change rate of the position in counts per second. If the sensor does not 
 * provide position, this default impementation will always return 0.0
 * 
 * @return  this defaule implementation will always return 0.0
 * 
 ******************************************************************************/
float Encoder::getRawRate(void)
{
    return 0.0;
}

/*******************************************************************************
 *
 * @return  the rate at which the sensor is changing in rotations per second
 * 
 ******************************************************************************/
float Encoder::getRotationRate(void)
{
    return getRawRate() / counts_per_rotation;
}

/*******************************************************************************
 *
 * @return  the rate at which the sensor is changing in user units
 * 
 ******************************************************************************/
float Encoder::getRate(void)
{
    return getRotationRate() * scale;
}

}
