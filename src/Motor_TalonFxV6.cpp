/*******************************************************************************
 *
 * File: Motor_TalonFxV6.cpp
 * 
 * Written by:
 *  FRC Team 324, Chips
 * 	Clear Creek Independent School District FIRST Robotics
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Motor_TalonFxV6.h"

#include "gsutilities/Advisory.h"
#include "rfutilities/RobotUtil.h"

#include "ctre/phoenix/motorcontrol/SensorCollection.h"

//using namespace gsu;
using namespace ctre::phoenix6;

#define CAN_TIMEOUT 0
//#define CAN_TIMEOUT kTimeout

const double Motor_TalonFxV6::m_volt_max = 12.0;

/******************************************************************************
 *
 * Create an instance of a motor controller for the provided PWM Speed Controller
 *
 * WARNING: This class will take ownership of provided controller -- this class
 *          will delete the controller when finished using it if it is not
 *          a nullptr.
 *
 ******************************************************************************/
Motor_TalonFxV6::Motor_TalonFxV6(int device_id, std::string canbus, int status_period_general_ms, int status_period_feedback_ms)
    : Motor()
    , m_controller(device_id, canbus)
{
    Advisory::pinfo("Motor_TalonFxV6::Motor_TalonFxV6");
    
    // The Kraken does not have directly connected limit switches
    forward_limit_switch = nullptr;
    reverse_limit_switch = nullptr;

    /* set default motor controller parameters */
    m_controller.GetConfigurator().Apply(configs::TalonFXConfiguration());

    m_controller.GetConfigurator().SetPosition(0_tr); // Set the Integrated Encoder to a raw position of 0
/*
    if (control_period_ms > 0)
    {
        m_controller.SetControlFramePeriod(ControlFrameEnhanced::Control_1_General, control_period_ms);
    }
    else
    {
        m_controller.SetControlFramePeriod(ControlFrameEnhanced::Control_1_General, 10);
    }
*/
    if (status_period_general_ms > 0)
    {
        //BaseStatusSignal.setUpdateFrequencyForAll(100, cancoder.getPosition(), cancoder.getFault_Undervoltage(), cancoder.getSupplyVoltage());

        //m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_1_General, status_period_general_ms, CAN_TIMEOUT);
    }
    else
    {
       // m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_1_General, 10, CAN_TIMEOUT);
    }

    if (status_period_feedback_ms > 0)
    {
        //  cancoder.GetPosition().SetUpdateFrequency(100_Hz);

        //m_controller.GetPosition().SetUpdateFrequency(status_period_feedback_ms);
    }
    else
    {
        //m_controller.SetStatusFramePeriod(StatusFrameEnhanced::Status_2_Feedback0, 20, CAN_TIMEOUT);
    }
    
    forward_limit_pressed = false;
    reverse_limit_pressed = false;

    m_output_scale = 1.0;
    m_output_offset = 0.0;
    m_velocity_timescale = 1.0; // 1 s = 10 * (100ms) where 100ms is TalonFx unitary time
}

/******************************************************************************
 *
 ******************************************************************************/
Motor_TalonFxV6::~Motor_TalonFxV6(void)
{
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setPercent(double percent)
{
    m_controller.SetControl(dutyCycleControl.WithOutput(percent));
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getPercent()
{
    return dutyCycleControl.Output();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setPosition(double position)
{
    m_control_mode = ControlModeType::POSITION;
    units::angle::turn_t val = (position - m_output_offset)/m_output_scale * 1_tr;
    m_controller.SetControl(positionControl.WithPosition(val));
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getPosition()
{
    return ((positionControl.Position() * m_output_scale) + m_output_offset);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getRawPosition(void)
{
    return m_controller.GetPosition().GetValueAsDouble();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::resetPosition(double value)
{
    m_controller.GetConfigurator().SetPosition(value * 1_tr); 
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setVelocity(double velocity)
{
    m_control_mode = ControlModeType::VELOCITY;
 //   units::angular_velocity::turns_per_second_t val = (velocity * 1_tps/ m_output_scale / m_velocity_timescale);
    units::angular_velocity::turns_per_second_t val = (velocity * 1_tps);

    velocityControl.Slot = 0;
    m_controller.SetControl(velocityControl.WithVelocity(val)); 
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getVelocity(void)
{
    return m_controller.GetVelocity().GetValueAsDouble();
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getRawVelocity(void)
{
    return m_controller.GetVelocity().GetValueAsDouble();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setMotorInvert(bool invert)
{
    m_controller.SetInverted(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_TalonFxV6::getMotorInvert(void)
{
    return m_controller.GetInverted();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setSensorInvert(bool invert)
{
    //m_controller.SetSensorPhase(invert);
}

/******************************************************************************
 *
 ******************************************************************************/
// bool Motor_TalonFxV6::getSensorInvert(void)
// {
//     return m_controller.GetSensorPhase();
// }

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setSensorScale(double scale, double offset)
{
    m_output_scale = scale;
    m_output_offset = offset;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setBrakeMode(bool brake)
{
    configs::MotorOutputConfigs motor_outputs{};
    m_controller.GetConfigurator().Refresh(motor_outputs);
    motor_outputs.DutyCycleNeutralDeadband = 0.0;
        Advisory::pinfo(" ---------------  in set Brake Mode");

   if (brake)
    {
        Advisory::pinfo("   set Brake Mode to Brake");
        motor_outputs.NeutralMode = signals::NeutralModeValue::Brake;
    }
    else
    {
        Advisory::pinfo("   set Brake Mode to Coast");
        motor_outputs.NeutralMode = signals::NeutralModeValue::Coast;
    }
    m_controller.GetConfigurator().Apply(motor_outputs);
}

/******************************************************************************
 *
 * Configure the supply current limits for the motor
 * 
 * @param   enabled     If true current limiting will be enabled
 * 
 * @param   amps        The amount of supply current allowed.
 * 
 * @param   peak        Delay supply current limiting until current 
 *                      exceeds this threshold for longer than duration. 
 * 
 * @param   duration     Allows unlimited current for a period of time 
 *                       before current limiting occurs.
 * 
 ******************************************************************************/
void  Motor_TalonFxV6::setCurrentLimit(bool enabled, double amps, double peak, double duration )
{
    Advisory::pinfo("   Motor_TalonFxV6::setCurrentLimit enabled=%d, amps=%f, peak=%f, duration=%f", enabled, amps, peak, duration);
    ctre::phoenix6::configs::CurrentLimitsConfigs currentLimits{};
    m_controller.GetConfigurator().Refresh(currentLimits);
    currentLimits.SupplyCurrentLimit = amps;
    currentLimits.SupplyCurrentThreshold = peak;
    currentLimits.SupplyTimeThreshold = duration;
    currentLimits.SupplyCurrentLimitEnable = enabled;
    m_controller.GetConfigurator().Apply(currentLimits);
}

/******************************************************************************
 *
 * Configure the stator current limits for the motor
 * 
 * @param   enabled     If true current limiting will be enabled
 * 
 * @param   amps        The limit of the output current 
 * 
 ******************************************************************************/
void  Motor_TalonFxV6::setStatorCurrentLimit(bool enabled, double amps)
{
    Advisory::pinfo("   Motor_TalonFxV6::setStatorCurrentLimit enabled=%d, amps=%f", enabled, amps);
    ctre::phoenix6::configs::CurrentLimitsConfigs currentLimits{};
    m_controller.GetConfigurator().Refresh(currentLimits);
    currentLimits.StatorCurrentLimit = amps;
    currentLimits.StatorCurrentLimitEnable = enabled;
    m_controller.GetConfigurator().Apply(currentLimits);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::doUpdate(void)
{
    LimitSwitch::InputMode mode = LimitSwitch::INPUT_NORMALLY_OPEN;
	if (reverse_limit_switch != nullptr) {
        reverse_limit_pressed = reverse_limit_switch->isPressed();
    }
    if (forward_limit_switch != nullptr) {
        forward_limit_pressed = forward_limit_switch->isPressed();
    }
}

/******************************************************************************
 *
 ******************************************************************************/
Motor::ControlModeType Motor_TalonFxV6::getControlMode()
{
    return m_control_mode;
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setFeedbackDevice(FeedbackDeviceType sensor_type, bool wrap)
{/*
    switch(sensor_type)
    {
        case INTEGRATED_SENSOR:
        {
            m_controller.ConfigSelectedFeedbackSensor(FeedbackDevice::IntegratedSensor, 0, CAN_TIMEOUT);
        } break;

        default:
        {
            Advisory::pwarning("    unable to set sensor to specified type -- TalonFx only support INTEGRATED_SENSOR");
        } break;
    }*/
}

/******************************************************************************
 *
 * This may be different then other motors.
 *
 * If this method is not called, the default values will be 1.0 and -1.0.
 *
 * @param fwd_nom   the maximum command to which the output will be set,
 *                  this will normally be positive -- between 0.0 and 1.0
 *
 * @param rev_nom   the minimum command to which the output will be set
 *                  this will normally be negative -- between -1.0 and 0.0
 *
 ******************************************************************************/
void Motor_TalonFxV6::setClosedLoopOutputLimits(float fwd_nom, float rev_nom, float fwd_peak, float rev_peak)
{
 /*   m_controller.ConfigPeakOutputForward(fwd_peak, CAN_TIMEOUT);
    m_controller.ConfigPeakOutputReverse(rev_peak, CAN_TIMEOUT);
    m_controller.ConfigNominalOutputForward(fwd_nom, CAN_TIMEOUT);
    m_controller.ConfigNominalOutputReverse(rev_nom, CAN_TIMEOUT);*/
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_TalonFxV6::isUpperLimitPressed(void)
{
    return forward_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
bool Motor_TalonFxV6::isLowerLimitPressed(void)
{
    return reverse_limit_pressed;
}

/******************************************************************************
 *
 ******************************************************************************/
int Motor_TalonFxV6::getDeviceID()
{
    return m_controller.GetDeviceID();
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setAsFollower(Motor_TalonFxV6& leader)
{
    m_control_mode = ControlModeType::FOLLOWER;

    Advisory::pwarning("Not Implemented -- Motor_TalonFxV6::setAsFollower");
//    m_controller.Follow(&leader);
//    m_controller.Set(ControlMode::Follower, leader);
}

/******************************************************************************
 *
 ******************************************************************************/
ctre::phoenix6::hardware::TalonFX& Motor_TalonFxV6::getController(void)
{
    return m_controller;
}


/******************************************************************************
 *
 * Configuring the External Lower Limit Switch
 *
 * @param limit_switch 	external lower limit switch created in hardware factory
 * 
 *******************************************************************************/
void Motor_TalonFxV6::setLowerLimitSwitch(LimitSwitch* limit_switch)
{
    reverse_limit_switch = limit_switch;
    if (reverse_limit_switch == nullptr)
    {
        reverse_limit_pressed = false;
        return;
    }
}

/******************************************************************************
 *
 * Configuring the External Upper Limit Switch
 *
 * @param limit_switch 	external upper limit switch created in hardware factory
 * 
 *******************************************************************************/
void Motor_TalonFxV6::setUpperLimitSwitch(LimitSwitch* limit_switch)
{
    forward_limit_switch = limit_switch;
    if (forward_limit_switch == nullptr)
    {
        forward_limit_pressed = false;
        return;
    }
}

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) it will the talon will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 							the limit switch
 * 							NOTE: this could be updated to also allow for
 * 							the Talon to use external limit switches.
 *
 ******************************************************************************/
void Motor_TalonFxV6::setUpperLimitSwitch(int8_t port, bool normally_open, bool reset_position, double position_value)
{
    Advisory::pinfo("        Motor_TalonFxV6::setUpperLimitSwitch(%d,%d,%d,%f)", port, normally_open,reset_position, position_value);
 /*   if (port >= 0)
    {
        m_controller.ConfigForwardLimitSwitchSource(
                LimitSwitchSource_FeedbackConnector,
                normally_open?LimitSwitchNormal_NormallyOpen:LimitSwitchNormal_NormallyClosed,
                CAN_TIMEOUT);

        m_controller.ConfigClearPositionOnLimitF(reset_position);
    }
    else
    {
        m_controller.ConfigForwardLimitSwitchSource(
                LimitSwitchSource_Deactivated,
                LimitSwitchNormal_Disabled,
                CAN_TIMEOUT);
    }*/
}

/******************************************************************************
 *
 * Configuring the Can Limit Switch
 *
 * @param normally_open 	true if the switch is wired to be normally open,
 *                          false if it is normally closed
 *
 * @param enabled			if true (the default) it will the talon will be
 * 							configured to have a limit switch wired to the
 * 							speed controller, if false it will turn off
 * 							the limit switch
 * 							NOTE: this could be updated to also allow for
 * 							the Talon to use external limit switches.
 *
 ******************************************************************************/
void Motor_TalonFxV6::setLowerLimitSwitch  (int8_t port, bool normally_open, bool reset_position, double position_value)
{
    Advisory::pinfo("        Motor_TalonFxV6::setLowerLimitSwitch(%d,%d,%d,%f)", port, normally_open,reset_position, position_value);
 /*   if (port >= 0)
    {
        m_controller.ConfigReverseLimitSwitchSource(
                LimitSwitchSource_FeedbackConnector,
                normally_open?LimitSwitchNormal_NormallyOpen:LimitSwitchNormal_NormallyClosed,
                CAN_TIMEOUT);

        m_controller.ConfigClearPositionOnLimitR(reset_position);
    }
    else
    {
        m_controller.ConfigReverseLimitSwitchSource(
                LimitSwitchSource_Deactivated,
                LimitSwitchNormal_Disabled,
                CAN_TIMEOUT);
    }*/
}

/******************************************************************************
 *
 * NOTICE: This method takes ownership of the PID, this class will delete the
 *         PID when it is no longer needed.
 *
 ******************************************************************************/
void Motor_TalonFxV6::setPid(BasicPid* pid)
{ 
    if (pid != nullptr)
    {
        configs::Slot0Configs slot0Configs{};
        m_controller.GetConfigurator().Refresh(slot0Configs);

        slot0Configs.kP = pid->getKp();
        slot0Configs.kD = pid->getKd();
        slot0Configs.kI = pid->getKi();
        slot0Configs.kV = pid->getKf();

        //configs.Slot0.kF = pid->getKf(); there are multiple FF 
        m_controller.GetConfigurator().Apply(slot0Configs, 50_ms);
        delete pid;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setKp(double p)
{
    configs::Slot0Configs slot0Configs{};
    m_controller.GetConfigurator().Refresh(slot0Configs);
    slot0Configs.kP = p;
    m_controller.GetConfigurator().Apply(slot0Configs);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setKi(double i)
{        
    configs::Slot0Configs slot0Configs{};
    m_controller.GetConfigurator().Refresh(slot0Configs);
    slot0Configs.kI = i;
    m_controller.GetConfigurator().Apply(slot0Configs);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setKd(double d)
{
    configs::Slot0Configs slot0Configs{};
    m_controller.GetConfigurator().Refresh(slot0Configs);
    slot0Configs.kD = d;
    m_controller.GetConfigurator().Apply(slot0Configs);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setKf(double f)
{
    //m_controller.Config_kF(0, f, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
void Motor_TalonFxV6::setKz(double z)
{
    //m_controller.Config_IntegralZone(0, (int)z, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getKp(void)
{
    configs::TalonFXConfiguration configs{};
    m_controller.GetConfigurator().Refresh(configs.Slot0);
    return configs.Slot0.kP;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getKi(void)
{
    configs::TalonFXConfiguration configs{};
    m_controller.GetConfigurator().Refresh(configs.Slot0);
    return configs.Slot0.kI;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getKd(void)
{
    configs::TalonFXConfiguration configs{};
    m_controller.GetConfigurator().Refresh(configs.Slot0);
    return configs.Slot0.kD;
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getKf(void)
{
    return  0.0;//m_controller.ConfigGetParameter(ParamEnum::eProfileParamSlot_F, 0, CAN_TIMEOUT);
}

/******************************************************************************
 *
 ******************************************************************************/
double Motor_TalonFxV6::getKz(void)
{
    return  0.0;//m_controller.ConfigGetParameter(ctre::phoenix::ParamEnum::eProfileParamSlot_IZone, 0, CAN_TIMEOUT);
}
