/*******************************************************************************
 *
 * File: AnalogIrSensor.cpp -- Analog IR Sensor
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/AnalogIrSensor.h"

using namespace frc;

/******************************************************************************
 *
 ******************************************************************************/
AnalogIrSensor::AnalogIrSensor(int channel) : AnalogInput(channel)
{
	m_threshold = DEFAULT_THRESHOLD_VALUE;
	m_threshold_type = AnalogIrSensor::kLessThan;
	m_default_value = DEFAULT_IR_VALUE;
	m_latch_cycles = DEFAULT_LATCH_CYLES;
	m_latch_count = 0;
	m_value = 0;
}


/******************************************************************************
 *
 ******************************************************************************/
AnalogIrSensor::~AnalogIrSensor()
{

}

/******************************************************************************
 *
 ******************************************************************************/
void AnalogIrSensor::setThreshold(int threshold)
{
	m_threshold = threshold;
}

/******************************************************************************
 *
 ******************************************************************************/
void AnalogIrSensor::setLatchCycles(int cycles)
{
	m_latch_cycles = cycles;
}

/******************************************************************************
 *
 ******************************************************************************/
void AnalogIrSensor::setThresholdType(AnalogIrSensor::ThresholdType type)
{
	m_threshold_type = type;
}

/******************************************************************************
 *
 ******************************************************************************/
bool AnalogIrSensor::onLine()
{
	bool ret = false;

	if(m_latch_count > 0)
	{
		ret = true;
	}
	return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
int AnalogIrSensor::update()
{
	int value = GetValue();

	if(m_threshold_type == AnalogIrSensor::kGreaterThan && value > m_threshold)
	{
		m_latch_count = m_latch_cycles;
	}
	else if(m_threshold_type == AnalogIrSensor::kGreaterThan && m_latch_count > 0)
	{
		m_latch_count--;
	}
	else if(m_threshold_type == AnalogIrSensor::kLessThan && value < m_threshold)
	{
		m_latch_count = m_latch_cycles;
	}
	else if(m_threshold_type == AnalogIrSensor::kLessThan && m_latch_count > 0)
	{
		m_latch_count--;
	}
	return value;
}
