/*******************************************************************************
 *
 * File: Cancoder.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfhardware/Cancoder.h"

using namespace ctre::phoenix6;

namespace rfh
{

/*******************************************************************************
 *
 *  @brief   Create an instance of a cancoder
 * 
 * The default configuration for a Cancoder is to have a range that is from 
 * 0 to 360 degrees (or 0.0 to 1.0 revolutions) with clockwise
 * rotation increasing the value and no offset.
 * 
 * @param   port    the Cancoder ID
 * 
 * @param   canbus  the name of the canbus to which this Cancoder is connected
 * 
 *******************************************************************************/
Cancoder::Cancoder(int can_id, std::string can_bus)
    : Encoder
      (
        Encoder::Capability::ABSOLUTE | 
        Encoder::Capability::POSITION |
        Encoder::Capability::RATES
      )
    , m_cancoder(can_id, can_bus)
{
    configs::CANcoderConfiguration canCoderConfig{};
    canCoderConfig.MagnetSensor.AbsoluteSensorRange = signals::AbsoluteSensorRangeValue::Unsigned_0To1;
    canCoderConfig.MagnetSensor.SensorDirection = signals::SensorDirectionValue::Clockwise_Positive;
    canCoderConfig.MagnetSensor.MagnetOffset = 0.0;
    m_cancoder.GetConfigurator().Apply(canCoderConfig);
}

/*******************************************************************************
 *
 * @brief   set sensor direction
 * 
 * @param   invert   if false the sensor value will increase when turned 
 *                      clockwise, else the value will increase when turned
 *                      counterclockwise 
 * 
 *******************************************************************************/
void Cancoder::setInvertDirection(bool invert)
{
    configs::CANcoderConfiguration canCoderConfig{};
    m_cancoder.GetConfigurator().Refresh(canCoderConfig);
    if (invert)
    {
        canCoderConfig.MagnetSensor.SensorDirection = signals::SensorDirectionValue::CounterClockwise_Positive;
    }
    else
    {
        canCoderConfig.MagnetSensor.SensorDirection = signals::SensorDirectionValue::Clockwise_Positive;
    }
    invert_direction = invert;
    m_cancoder.GetConfigurator().Apply(canCoderConfig);
}

/*******************************************************************************
 *
 * @return   the sensor direction invert flag
 * 
 *******************************************************************************/
bool Cancoder::getInvertDirection(void)
{
    configs::CANcoderConfiguration canCoderConfig{};
    m_cancoder.GetConfigurator().Refresh(canCoderConfig);
    return (canCoderConfig.MagnetSensor.SensorDirection == signals::SensorDirectionValue::CounterClockwise_Positive);
}

/*******************************************************************************
 *
 * @return the current sensor position in degrees 
 * 
 *******************************************************************************/
float Cancoder::getRawPosition() 
{
    return (float)m_cancoder.GetPosition().GetValue().value();
}

/*******************************************************************************
 *
 * @return the absolute position in degrees per second 
 * 
 *******************************************************************************/
float Cancoder::getAbsoluteRawPosition() 
{
    return (float)m_cancoder.GetAbsolutePosition().GetValue().value();
}

/*******************************************************************************
 *
 * @return the current sensor position in degrees per second 
 * 
 *******************************************************************************/
float Cancoder::getRawRate() 
{
    return (float)m_cancoder.GetVelocity().GetValue().value();
}

}
